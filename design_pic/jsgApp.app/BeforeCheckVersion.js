require("jsb.js");
require("jsb_pg.js");
require("LoadingRes.js");
require("CBootLoader.js");
require("Qidea_SQLite.js");
require("Qidea_DownLoader.js");
var rootName="Documents/JSG";
var remoteServerSite="http://ios.jsanguo.com";
var remoteMid="/rescenter/v1/";
function BeforeCheckVersion()
{
    var me=this;
    me.httpLoader=new QideaCC.loader();
    Pg.showGL();
    me.loadLoadingScene();
    me.getUnPackInfo();
};
BeforeCheckVersion.prototype.loadLoadingScene=function(){
    var me = this;
    var loadingNode = null;
    try{
        loadingNode = cc.BuilderReader.load("image/interface/BootLoader.ccbi");
    }catch(e){
        //me.moveFile(me.getPackagePath("image/interface/BootLoader.ccbi"),me.getLocalPath("image/interface/BootLoader.ccbi"));
        cc.log("loadBootloader Failed");
        return null;
        //loadingNode=cc.BuilderReader.load(me.getPackagePath("image/interface/BootLoader.ccbi"));
    }
    
    var runningScene = cc.Director.getInstance().getRunningScene();
    if (runningScene === null)
        cc.Director.getInstance().runWithScene(loadingNode);
    else
        cc.Director.getInstance().replaceScene(loadingNode);
    
    return loadingNode;
};
BeforeCheckVersion.prototype.readFile=function(fileName){
    return cc.FileUtils.getInstance().getFileData(fileName,"rb",0);
};
BeforeCheckVersion.prototype.getRemoteUrl=function(url){
    return remoteServerSite+remoteMid+url;
};
BeforeCheckVersion.prototype.getLocalPath=function(url){
    return "../../"+rootName+"/"+url;
};
BeforeCheckVersion.prototype.getUnPackInfo=function()
{
    var me=this;
    var firstJSON=me.readFile(me.getLocalPath("first.json"));
    if(firstJSON==null)
    {
        me.httpLoader.LoadFormRomote(("http://ios.jsanguo.com/rescenter/v2.2/flag.json"), 5, 0, function (loader) {
                                     var tips = "你的网络不给力啊(Code:" + loader.getCode() + ")请检查网络";
                                     QideaCC.showAlert("网络出错异常，检查网络然后重试",
                                                       tips,
                                                       function (indx) {
                                                       if (indx == 0) {
                                                       QideaCC.IOSExit();//打开网络设置
                                                       }
                                                       },
                                                       "退出",
                                                       "");
                                     }, function (loader) {
                                     try{
                                     var unpackInfo = JSON.parse(loader.getData());
                                     var isPack=unpackInfo["unpack"];
                                     IsUnPack=isPack;
                                     QideaCC.setUnPack(isPack);
                                     QideaCC.runJSscript("BootLoader.js");
                                     }
                                     catch(e)
                                     {
                                      QideaCC.setUnPack(false);
                                      QideaCC.runJSscript("BootLoader.js");
                                     }
                                     }, function (loader) {
                                     });

    }else
    {
        QideaCC.setUnPack(true);
        QideaCC.runJSscript("BootLoader.js");
    }
    
};
qBeforeCheckVersion= new BeforeCheckVersion();