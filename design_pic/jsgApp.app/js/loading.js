function GameLoading() {

};
GameLoading.isInPhoneGap=function(){
        return (cordova || PhoneGap || phonegap)
            && /^file:\/{3}[^\/]/i.test(window.location.href)
            && /ios|iphone|ipod|ipad|android/i.test(navigator.userAgent);
};
GameLoading.ux2px=function(ux){
    return 1.0*document.body.offsetWidth/320*ux;
};
GameLoading.uy2px=function(uy){
    return 1.0*document.height/480*uy;
};
GameLoading.resizeUI=function(){
    GameLoading.$loadingLineOuter.css({
        left:GameLoading.ux2px(25),
        top:GameLoading.uy2px(302),
        width:GameLoading.ux2px(271),
        height:GameLoading.uy2px(7.5)
    });
    GameLoading.$loadingLineValue.css({
        marginTop:GameLoading.uy2px(10),
        fontSize:GameLoading.uy2px(14)
    });
};

GameLoading.init=function(){
    $(function(){
        GameLoading.$loadingRoot=$("#GameLogin_Loading");
        GameLoading.$loadingLineOuter=GameLoading.$loadingRoot.find(".GameLogin_loadingLineOuter");
        GameLoading.$loadingLine=GameLoading.$loadingRoot.find(".GameLogin_loadingLine");
        GameLoading.$loadingLineValue=GameLoading.$loadingRoot.find(".GameLogin_loadingLineValue");
        GameLoading.$Value=GameLoading.$loadingLineValue.find("label");
        GameLoading.resizeUI();
        $(window).resize(function(){
            GameLoading.resizeUI();
        });
    });
};

GameLoading.showLoadStep = function (step) {
    var stepLength = 100 / bootLoader.config.totalStep;
    GameLoading.update(step * stepLength);
    clearInterval(GameLoading.timer);
    GameLoading.timer = setInterval(function()
    {
        GameLoading.update(GameLoading.currPercent+0.5);
    },2000);
};

GameLoading.show = function () {
    var $globalLoadingResources = $("#GameLogin_Loading");
    $globalLoadingResources.show();
    GameLoading.update();
};

//config:{percent:number,value:string}
GameLoading.update = function (percent,text) {
    GameLoading.currPercent = percent;
    if(GameLoading.currPercent>100){
        GameLoading.currPercent=100;
    }
    if(typeof GameLoading.$loadingLine!="undefined"){
        GameLoading.$loadingLine.animate({
            "width":GameLoading.currPercent +"%"
        }, "slow");
        GameLoading.$Value.html(text?text:"初始化...").animate({
            opacity:"toggle"
        }, "slow");
    }

};

GameLoading.hidden = function () {
    $("#GameLogin_Loading").hide();
    clearInterval(GameLoading.timer);
};
GameLoading.show = function () {
    $("#GameLogin_Loading").show();
};


GameLoading.remove = function () {
    $("#GameLogin_Loading").remove();
    clearInterval(GameLoading.timer);
};
GameLoading.saveLoginData=function(data){
    var checkoutCanSendLoginDataTimer=setInterval(function(){
        if(typeof Td!="undefined" && typeof Td._loginSuccess=="function"){
            clearInterval(checkoutCanSendLoginDataTimer);
            Td._loginSuccess(data);
        }
    },1000);
};