/*
**  Flags For DBFile Open Operations
**
** These bit values are intended for use in the
** 3rd parameter to the [sqlite3_open_v2()] interface and
** in the 4th parameter to the [sqlite3_vfs.xOpen] method.
*/
var Qidea_Open_FLag=Qidea_Open_FLag ||{};
 Qidea_Open_FLag.Qidea_SQLITE_OPEN_READONLY         =0x00000001 ; /* Ok for sqlite3_open_v2() */
 Qidea_Open_FLag.Qidea_SQLITE_OPEN_READWRITE        =0x00000002 ;/* Ok for sqlite3_open_v2() */
 Qidea_Open_FLag.Qidea_SQLITE_OPEN_CREATE           =0x00000004 ;/* Ok for sqlite3_open_v2() */
 Qidea_Open_FLag.Qidea_SQLITE_OPEN_DELETEONCLOSE    =0x00000008 ; /* VFS only */
 Qidea_Open_FLag.Qidea_SQLITE_OPEN_EXCLUSIVE        =0x00000010 ; /* VFS only */
 Qidea_Open_FLag.Qidea_SQLITE_OPEN_AUTOPROXY        =0x00000020 ;  /* VFS only */
 Qidea_Open_FLag.Qidea_SQLITE_OPEN_URI              =0x00000040 ; /* Ok for sqlite3_open_v2() */
 Qidea_Open_FLag.Qidea_SQLITE_OPEN_MAIN_DB          =0x00000100 ; /* VFS only */
 Qidea_Open_FLag.Qidea_SQLITE_OPEN_TEMP_DB          =0x00000200 ; /* VFS only */
 Qidea_Open_FLag.Qidea_SQLITE_OPEN_TRANSIENT_DB     =0x00000400 ; /* VFS only */
 Qidea_Open_FLag.Qidea_SQLITE_OPEN_MAIN_JOURNAL     =0x00000800 ;/* VFS only */
 Qidea_Open_FLag.Qidea_SQLITE_OPEN_TEMP_JOURNAL     =0x00001000 ; /* VFS only */
 Qidea_Open_FLag.Qidea_SQLITE_OPEN_SUBJOURNAL       =0x00002000 ; /* VFS only */
 Qidea_Open_FLag.Qidea_SQLITE_OPEN_MASTER_JOURNAL   =0x00004000 ; /* VFS only */
 Qidea_Open_FLag.Qidea_SQLITE_OPEN_NOMUTEX          =0x00008000 ; /* Ok for sqlite3_open_v2() */
 Qidea_Open_FLag.Qidea_SQLITE_OPEN_FULLMUTEX        =0x00010000 ; /* Ok for sqlite3_open_v2() */
 Qidea_Open_FLag.Qidea_SQLITE_OPEN_SHAREDCACHE      =0x00020000 ; /* Ok for sqlite3_open_v2() */
 Qidea_Open_FLag.Qidea_SQLITE_OPEN_PRIVATECACHE     =0x00040000 ; /* Ok for sqlite3_open_v2() */
 Qidea_Open_FLag.Qidea_SQLITE_OPEN_WAL              =0x00080000 ; /* VFS only */
 /*********** ErrDefine*****************/
 var Qidea_Sqlite_Err=Qidea_Sqlite_Err||{};
 Qidea_Sqlite_Err.SQLITE_OK           =0;
Qidea_Sqlite_Err.SQLITE_ERROR        =1;   /* SQL error or missing database */
Qidea_Sqlite_Err.SQLITE_INTERNAL     =2;   /* Internal logic error in SQLite */
Qidea_Sqlite_Err.SQLITE_PERM         =3;   /* Access permission denied */
Qidea_Sqlite_Err.SQLITE_ABORT        =4;   /* Callback routine requested an abort */
Qidea_Sqlite_Err.SQLITE_BUSY         =5;   /* The database file is locked */
Qidea_Sqlite_Err.SQLITE_LOCKED       =6;   /* A table in the database is locked */
Qidea_Sqlite_Err.SQLITE_NOMEM        =7;   /* A malloc() failed */
Qidea_Sqlite_Err.SQLITE_READONLY     =8;   /* Attempt to write a readonly database */
Qidea_Sqlite_Err.SQLITE_INTERRUPT    =9;   /* Operation terminated by sqlite3_interrupt()*/
Qidea_Sqlite_Err.SQLITE_IOERR        =10;   /* Some kind of disk I/O error occurred */
Qidea_Sqlite_Err.SQLITE_CORRUPT      =11;   /* The database disk image is malformed */
Qidea_Sqlite_Err.SQLITE_NOTFOUND     =12;   /* Unknown opcode in sqlite3_file_control() */
Qidea_Sqlite_Err.SQLITE_FULL         =13;   /* Insertion failed because database is full */
Qidea_Sqlite_Err.SQLITE_CANTOPEN     =14;   /* Unable to open the database file */
Qidea_Sqlite_Err.SQLITE_PROTOCOL     =15;   /* Database lock protocol error */
Qidea_Sqlite_Err.SQLITE_EMPTY      = 16 ;  /* Database is empty */
Qidea_Sqlite_Err.SQLITE_SCHEMA      =17 ;  /* The database schema changed */
Qidea_Sqlite_Err.SQLITE_TOOBIG      =18 ;  /* String or BLOB exceeds size limit */
Qidea_Sqlite_Err.SQLITE_CONSTRAINT  =19 ;  /* Abort due to constraint violation */
Qidea_Sqlite_Err.SQLITE_MISMATCH    =20 ;  /* Data type mismatch */
Qidea_Sqlite_Err.SQLITE_MISUSE      =21 ;  /* Library used incorrectly */
Qidea_Sqlite_Err.SQLITE_NOLFS       =22 ;  /* Uses OS features not supported on host */
Qidea_Sqlite_Err.SQLITE_AUTH        =23 ;  /* Authorization denied */
Qidea_Sqlite_Err.SQLITE_FORMAT      =24 ;  /* Auxiliary database format error */
Qidea_Sqlite_Err.SQLITE_RANGE       =25 ;  /* 2nd parameter to sqlite3_bind out of range */
Qidea_Sqlite_Err.SQLITE_NOTADB      =26 ;  /* File opened that is not a database file */
Qidea_Sqlite_Err.SQLITE_ROW         =100;  /* sqlite3_step() has another row ready */
Qidea_Sqlite_Err.SQLITE_DONE        =101;  /* sqlite3_step() has finished executing */
