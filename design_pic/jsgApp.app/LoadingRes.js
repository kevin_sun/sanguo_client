function LoadingRes(){

};
LoadingRes.prototype.onDidLoadFromCCB=function(){
    cc.log("LoadingRes onDidLOadFromCCB");
    var me = this;
    me.loadingLine.setAnchorPoint({x:0,y:0.5});
    me.updateProgress(0,"正在加载资源");
    me.btnOK.setCallback(me.btnOKClick,me);
    me.hideDialogMsg();
};


LoadingRes.prototype.updateProgress=function(percent,msg){
    var me = this;
    if(percent>100){
        percent=100;
    }
    me.loadingLine.setPreferredSize({width:520.0*percent/100+26,height:20});
    me.msg.setString(msg);
};
LoadingRes.prototype.updateMsg=function(msg){
    var me = this;
    me.msg.setString(msg);
};
LoadingRes.prototype.updateMsg2=function(msg){
    var me = this;
    me.msg2.setString(msg);
};

LoadingRes.prototype.clearMsgSchedule=function(){
    var me = this;
    cc.Director.getInstance().getScheduler().unscheduleAllCallbacksForTarget(me.msg);
}
LoadingRes.prototype.updatePrecent=function(percent,time){
    var me = this;
    var curPercent =0;
    cc.Director.getInstance().getScheduler().scheduleCallbackForTarget(me.msg,function(){
        cc.log("update precent:"+curPercent);
        curPercent+=1;
        if(curPercent>=percent){
            curPercent=percent;
            cc.Director.getInstance().getScheduler().unscheduleAllCallbacksForTarget(me.msg);
        }
        me.loadingLine.setPreferredSize({width:520.0*curPercent/100+26,height:20});

    },time/percent,cc.REPEAT_FOREVER ,0, false);
    cc.log("------updatePercent-------");

};

LoadingRes.prototype.showDialogMsg=function(){
    var me = this;
    me.DialogMsg.setVisible(true);
};

LoadingRes.prototype.hideDialogMsg=function(){
    var me = this;
    me.DialogMsg.setVisible(false);
};

LoadingRes.prototype.btnOKClick=function(item){
    cc.log("----------btnOKClick---------------");
    this.hideDialogMsg(false);
    var loader = QideaCC.Downloader.getInstance();
    loader.DownLoaderBegin();
}