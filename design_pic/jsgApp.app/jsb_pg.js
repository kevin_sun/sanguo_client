/**
 从cocos端向pg端通讯的接口
 如何使用：
 cocos2d端 js中，require这个文件，然后使用Pg.go()就会跳转到pg中。
 */

var Pg=Pg||{};
if(_pg){
    cc.log("find _pg");
//    _pg.init();
//    var ret = _pg.eval("var v = 1;v");
//    cc.log("=== after eval, return as " + ret);
}

//go back to phonegap view
Pg.init = function(){
    _pg.init();
};

Pg.go = function(){
    _pg.goto();
};

Pg.destroy = function(){
    _pg.destroy();
}
Pg.showPg = function(){
    _pg.showPg();
}
Pg.hidePg = function(){
    _pg.hidePg();
}
Pg.showGL = function(){
    _pg.showGL();
}
Pg.hideGL = function(){
    _pg.hideGL();
}
Pg.eval = function(js){
    return _pg.eval(js);
}
Pg.setWWWFolderName=function(path){
    _pg.setWWWFolderName(path);
}

Pg.loadURL=function(url){
    _pg.reload(url);
}

/*
if(!cc.BuilderReader){
    cc.BuilderReader = cc.Reader;
}
*/
