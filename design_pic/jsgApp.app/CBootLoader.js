/*****************************************
 Qidea-Cocos native+html 兼容API   Begin
 ****************************************/
var QCC={};
/**
 * 查询节点是否可见
 * @param node
 * @return {Boolean}
 */
QCC.isNodeVisible=function(node){
    var isVisible=true;
    if(node.isVisible()==false){
        return false;
    }else{
        var curNode = node.getParent();
        while(curNode!=null){
            if(curNode.isVisible()==false){
                return false;
            }
            curNode = curNode.getParent();
        }
    }
    return isVisible;
}
/**
 * 判断node是否包含touch点
 * @param node
 * @param touch
 * @return {Boolean}
 */
QCC.rectContainsTouchPoint=function (node,touch) {
    if(sys.platform=="browser"){
        //for browser
        if(cc.rectContainsPoint(node.getBoundingBoxToWorld(),touch.getLocation()) && QCC.isNodeVisible(node)){
            return true;
        }else{
            return false;
        }
    }else{
        //for native
        var  s =node.getContentSize();
        var rect= cc.rect(0, 0, s.width, s.height);

        cc.log("{"+rect.x+","+rect.y+"}  {"+rect.width+","+rect.height+"}");
        //查询子节点的BoundingBox
        var children = node.getChildren();
        if (children){
            for (var i = 0; i < children.length; i++) {
                var child = children[i];
                if (child && child.isVisible()) {
                    var childSize = child.getContentSize();
                    var childPoint = node.convertToNodeSpace(child.getPosition());
                    var childRect = cc.rect(childPoint.x, childPoint.y, childSize.width, childSize.height);
                    if (childRect) {
                        rect = cc.rectUnion(rect, childRect);
                    }
                }
            }
        }

        cc.log(node.getAnchorPoint().x+","+node.getAnchorPoint().y);
        cc.log("{"+rect.x+","+rect.y+"}  {"+rect.width+","+rect.height+"}");
        cc.log("{"+node.convertTouchToNodeSpaceAR(touch).x+","+node.convertTouchToNodeSpaceAR(touch).y+"} ");
        if(cc.rectContainsPoint(rect,node.convertTouchToNodeSpace(touch)) && QCC.isNodeVisible(node) ){
            cc.log("clicked");
            return true;
        }else{
            cc.log("not clicked");
            return false;
        }
    }
}
/**
 * 代理对象添加事件
 * @param delegate  代理对象
 * @param priority  优先级  0+
 * @param swallowsTouches 是否运行多点触碰
 */
QCC.addTargetedDelegate=function(delegate, priority, swallowsTouches){
    if(sys.platform=="browser"){
        //for browser
        cc.Director.getInstance().getTouchDispatcher().addTargetedDelegate(delegate,priority,swallowsTouches);
    }else{
        //for native
        cc.registerTargettedDelegate(priority,swallowsTouches,delegate);
    }
};

QCC.addClick=function(node,callback){
    node.onTouchBegan=function(touch,event){
        return QCC.rectContainsTouchPoint(node,touch);
    }
    node.onTouchEnded =function(touch,event){
        callback();
    }
  //  QCC.addTargetedDelegate(node,0,true);
};

/*****************************************
 Qidea-Cocos native+html 兼容API   End
 ****************************************/



function CBootLoader(){

};
CBootLoader.prototype.onDidLoadFromCCB=function(){
    cc.log("CBootLoader onDidLOadFromCCB");
    var me = this;
    me.loadingLine.setAnchorPoint({x:0,y:0.5});
    me.updateProgress(0,"");
    //me.btnOK.setCallback(me.btnOKClick,me);
    me.hideAll();
    var color=me.msg2.getColor();
    me.msg2outLine=QideaCC.QideaLabelFX.create(me.msg2.getString(),me.msg2.getFontName(),me.msg2.getFontSize(),me.msg2.getDimensions(),
    me.msg2.getHorizontalAlignment(),me.msg2.getVerticalAlignment(),cc.size(3,3),3,cc.c4b(0,0,0,183),cc.c4b(253,229,86,255));
    me.msg2outLine.setScale(me.msg2.getScale());
    me.msg2outLine.setPosition(me.msg2.getPosition());
    var parent =me.msg2.getParent();
    if(parent!=null)
    {
        parent.addChild(me.msg2outLine,0,0);
        parent.removeChild(me.msg2);
    }


    //点击选区按钮上添加事件
    //me.bindClickTxtChooseServer();
    //me.bindClickChooseServer();

};
CBootLoader.prototype.bindClickChooseServer=function(){
    var me = this;
    QCC.addClick(me[CBootLoader.COMP_TYPE.ChooseServer],function(){
        me.show(CBootLoader.COMP_TYPE.EnterGame);
    });
};
CBootLoader.prototype.bindClickTxtChooseServer=function(){
    var me = this;
    QCC.addClick(me.txtChooseServer,function(){
        me.show(CBootLoader.COMP_TYPE.ChooseServer);
    });
};

CBootLoader.prototype.updateProgress=function(percent,msg){
    var me = this;
    if(percent>100){
        percent=100;
    }
    me.loadingLine.setPreferredSize({width:520.0*percent/100+26,height:20});
    me.msg.setString(msg);

};
CBootLoader.prototype.updateMsg=function(msg){
    var me = this;
    me.msg.setString(msg);
};
CBootLoader.prototype.updateMsg2=function(msg){
    var me = this;
    if(me.msg2outLine!=null)
    {
        me.msg2outLine.setString(msg);
    }

};
CBootLoader.prototype.setCheckVersionMsg=function(msg){
   var me=this;
  me.checkVersionLog.setString(msg);
};
CBootLoader.prototype.clearMsgSchedule=function(){
    var me = this;
    cc.Director.getInstance().getScheduler().unscheduleAllCallbacksForTarget(me.msg);
};
CBootLoader.prototype.updatePrecent=function(percent,time){
    var me = this;
    var curPercent =0;
    cc.Director.getInstance().getScheduler().scheduleCallbackForTarget(me.msg,function(){
        cc.log("update precent:"+curPercent);
        curPercent+=1;
        if(curPercent>=percent){
            curPercent=percent;
            cc.Director.getInstance().getScheduler().unscheduleAllCallbacksForTarget(me.msg);
        }
        me.loadingLine.setPreferredSize({width:520.0*curPercent/100+26,height:20});

    },time/percent,cc.REPEAT_FOREVER ,0, false);
    cc.log("------updatePercent-------");

};

CBootLoader.COMP_TYPE={
    DialogMsg:"dialogMsg",
    Process:"loadingProcess",
    EnterGame:"loadingEnter",
    CheckVersion:"loadingCheckVersion",
    ChooseServer:"loadingChooseServer"

}

CBootLoader.prototype.hideAll=function(){
    var me = this;
    cc.log("CBootLoader->hideAll");
    me.hide(CBootLoader.COMP_TYPE.DialogMsg);
    me.hide(CBootLoader.COMP_TYPE.Process);
    me.hide(CBootLoader.COMP_TYPE.EnterGame);
    me.hide(CBootLoader.COMP_TYPE.CheckVersion);
    me.hide(CBootLoader.COMP_TYPE.ChooseServer);
};
CBootLoader.prototype.hide=function(compType){
    var me = this;
    me[compType].setVisible(false);
}
CBootLoader.prototype.show=function(compType){
    var me = this;
    me.hideAll();
    cc.log( "show"+compType );
    me[compType].setVisible(true);
};
CBootLoader.prototype.runLoading=function(){
    var me = this;

};

CBootLoader.prototype.btnOKClick=function(item){
    cc.log("----------btnOKClick---------------");
    this.hide(CBootLoader.COMP_TYPE.DialogMsg);
    var loader = QideaCC.Downloader.getInstance();
    this.show(CBootLoader.COMP_TYPE.Process);
    loader.DownLoaderBegin();

};

CBootLoader.prototype.onEnterGame=function(){
    cc.log("----------onEnterGame---------------");
    qBootLoader.startGame();
    qBootLoader.EnterGame();

};
