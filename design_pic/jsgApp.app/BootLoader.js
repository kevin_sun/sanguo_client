/**
 * Bootloader
 * User: qidea
 * Date: 4/10/13
 * Time: 4:11 下午
 * To change this template use File | Settings | File Templates.
 */
require("jsb.js");
require("jsb_pg.js");
require("LoadingRes.js");
require("CBootLoader.js");
require("Qidea_SQLite.js");
require("Qidea_DownLoader.js");
cc.log(QideaCC.getAppDir());
var appPath = QideaCC.getAppDir();
var appPathTmp = appPath.substr(0, appPath.lastIndexOf("/"));
var appPackName = appPathTmp.substr(appPathTmp.lastIndexOf("/") + 1);
cc.log(appPackName);
var rootName = "Documents/JSG";
var remoteServerSite = "http://ios.jsanguo.com";
var remoteMid = "/rescenter/v1/";
var IsInGame = false;
var IsUnpack = false;
function BootLoader() {
    var me = this;
    //me.updateTimeDuring = 1000*60*30;   //后台停留时间超过5分钟 更新
    me.updateTimeDuring=1000*60*120;
    me.updateTimeDuringSmall = 1000*60*5;
    me.httpLoader=new QideaCC.loader();
    IsUnpack=QideaCC.IsUnPack();
    cc.log("unpack"+IsUnpack);
    //注册重启后台切入前台时，自动更新
    QideaCC.addObserver(me, function () {
        //检测在后端停留的时间长度
        require("CBootLoader.js");
        cc.log("QideaCC_Event.EVNET_COME_TO_FOREGROUND----------------");
        cc.Director.getInstance().startAnimation();
        cc.AudioEngine.getInstance().resumeAllEffects();
        if (!IsUnpack) {
            if (IsInGame)//如果已经正常进入游戏
            {
                me.timeEnd = Date.now();
                if ((me.timeEnd - me.timeStart) > me.updateTimeDuring) {
		    QideaCC.setIdleTimerDisabled(true);
                    IsInGame = false;
                    Pg.hidePg();
                    Pg.showGL();
                    me.loadingNode = me.loadLoadingScene();
                    cc.log("120min 后强制检查版本");
                    cc.log("重新检测更新");
                    me.startGame();
                } else if ((me.timeEnd - me.timeStart) > me.updateTimeDuringSmall) {
                    cc.log("30min 到  120min中 游戏内检查版本");
                    Pg.eval("Loading.hiddenGlobalLoading();database.getKingInfo(null);");
                    // me.loadingNode = me.loadLoadingScene();
                    // me.startGame();
                }else
                {
                    cc.log("30min 内直接进游戏");
                }

            } else {
                cc.log("重新开始加载流程");
                me.loadingNode = me.loadLoadingScene();
                me.startGame();
            }
        } else {
            if (IsInGame)//如果已经正常进入游戏
            {
                me.timeEnd = Date.now();
                if ((me.timeEnd - me.timeStart) > me.updateTimeDuring) {
                    me.timeStart = me.timeEnd;
                    //重新检测更新
                    IsInGame = false;
                    Pg.hidePg();
                    Pg.showGL();
                    cc.log("120min 后强制检查版本");
                    cc.log("重新检测更新");
                    Pg.eval("window.location.href='blank.html';");
                    me.checkPackageFirstRun();
                } else if ((me.timeEnd - me.timeStart) > me.updateTimeDuringSmall) {
		    QideaCC.setIdleTimerDisabled(true);
                    me.timeStart = me.timeEnd;
                    cc.log("30min 到  120min中 游戏内检查版本");
                    me.checkVerionInGame();
                } else {
                    //30min 中内 直接进入游戏
                    cc.log("30min 内直接进游戏");
                    cc.Director.getInstance().startAnimation();
                    cc.AudioEngine.getInstance().resumeAllEffects();
                    //调用客户端音乐检查函数
                    me.checkMusic();
                }
            } else {
		QideaCC.setIdleTimerDisabled(true);
                me.checkMusic();
                cc.log("重新开始加载流程");
                me.checkPackageFirstRun();
            }
        }

    }, Qidea_Event.EVNET_COME_TO_FOREGROUND);

    QideaCC.addObserver(me, function () {
        cc.log("QideaCC_Event.EVENT_COME_TO_BACKGROUND-----------------");
        me.timeStart = Date.now();
        cc.Director.getInstance().stopAnimation();
        cc.AudioEngine.getInstance().pauseMusic();
        cc.AudioEngine.getInstance().pauseAllEffects();
        QideaCC.Downloader.getInstance().QuitDownLoader();
        me.httpLoader.clear();
    }, Qidea_Event.EVENT_COME_TO_BACKGROUND);
    cc.log("QideaCC_Event.EVENT_COME_TO_BACKGROUND:" + Qidea_Event.EVENT_COME_TO_BACKGROUND);
    //首次安装，解包ipa目录资源到Document中
    if (!IsUnpack) {
        IsInGame = false;
        Pg.hidePg();
        Pg.showGL();
        me.loadingNode = me.loadLoadingScene();
        me.startGame();
    } else {
        me.checkPackageFirstRun();
    }
}
;

/**
 * sprintf
 */
var sprintf = (function () {
    function get_type(variable) {
        return Object.prototype.toString.call(variable).slice(8, -1).toLowerCase();
    }

    function str_repeat(input, multiplier) {
        for (var output = []; multiplier > 0; output[--multiplier] = input) {/* do nothing */
        }
        return output.join('');
    }

    var str_format = function () {
        if (!str_format.cache.hasOwnProperty(arguments[0])) {
            str_format.cache[arguments[0]] = str_format.parse(arguments[0]);
        }
        return str_format.format.call(null, str_format.cache[arguments[0]], arguments);
    };

    str_format.format = function (parse_tree, argv) {
        var cursor = 1, tree_length = parse_tree.length, node_type = '', arg, output = [], i, k, match, pad, pad_character, pad_length;
        for (i = 0; i < tree_length; i++) {
            node_type = get_type(parse_tree[i]);
            if (node_type === 'string') {
                output.push(parse_tree[i]);
            }
            else if (node_type === 'array') {
                match = parse_tree[i]; // convenience purposes only
                if (match[2]) { // keyword argument
                    arg = argv[cursor];
                    for (k = 0; k < match[2].length; k++) {
                        if (!arg.hasOwnProperty(match[2][k])) {
                            throw(sprintf('[sprintf] property "%s" does not exist', match[2][k]));
                        }
                        arg = arg[match[2][k]];
                    }
                }
                else if (match[1]) { // positional argument (explicit)
                    arg = argv[match[1]];
                }
                else { // positional argument (implicit)
                    arg = argv[cursor++];
                }

                if (/[^s]/.test(match[8]) && (get_type(arg) != 'number')) {
                    throw(sprintf('[sprintf] expecting number but found %s', get_type(arg)));
                }
                switch (match[8]) {
                    case 'b':
                        arg = arg.toString(2);
                        break;
                    case 'c':
                        arg = String.fromCharCode(arg);
                        break;
                    case 'd':
                        arg = parseInt(arg, 10);
                        break;
                    case 'e':
                        arg = match[7] ? arg.toExponential(match[7]) : arg.toExponential();
                        break;
                    case 'f':
                        arg = match[7] ? parseFloat(arg).toFixed(match[7]) : parseFloat(arg);
                        break;
                    case 'o':
                        arg = arg.toString(8);
                        break;
                    case 's':
                        arg = ((arg = String(arg)) && match[7] ? arg.substring(0, match[7]) : arg);
                        break;
                    case 'u':
                        arg = Math.abs(arg);
                        break;
                    case 'x':
                        arg = arg.toString(16);
                        break;
                    case 'X':
                        arg = arg.toString(16).toUpperCase();
                        break;
                }
                arg = (/[def]/.test(match[8]) && match[3] && arg >= 0 ? '+' + arg : arg);
                pad_character = match[4] ? match[4] == '0' ? '0' : match[4].charAt(1) : ' ';
                pad_length = match[6] - String(arg).length;
                pad = match[6] ? str_repeat(pad_character, pad_length) : '';
                output.push(match[5] ? arg + pad : pad + arg);
            }
        }
        return output.join('');
    };

    str_format.cache = {};

    str_format.parse = function (fmt) {
        var _fmt = fmt, match = [], parse_tree = [], arg_names = 0;
        while (_fmt) {
            if ((match = /^[^\x25]+/.exec(_fmt)) !== null) {
                parse_tree.push(match[0]);
            }
            else if ((match = /^\x25{2}/.exec(_fmt)) !== null) {
                parse_tree.push('%');
            }
            else if ((match = /^\x25(?:([1-9]\d*)\$|\(([^\)]+)\))?(\+)?(0|'[^$])?(-)?(\d+)?(?:\.(\d+))?([b-fosuxX])/.exec(_fmt)) !== null) {
                if (match[2]) {
                    arg_names |= 1;
                    var field_list = [], replacement_field = match[2], field_match = [];
                    if ((field_match = /^([a-z_][a-z_\d]*)/i.exec(replacement_field)) !== null) {
                        field_list.push(field_match[1]);
                        while ((replacement_field = replacement_field.substring(field_match[0].length)) !== '') {
                            if ((field_match = /^\.([a-z_][a-z_\d]*)/i.exec(replacement_field)) !== null) {
                                field_list.push(field_match[1]);
                            }
                            else if ((field_match = /^\[(\d+)\]/.exec(replacement_field)) !== null) {
                                field_list.push(field_match[1]);
                            }
                            else {
                                throw('[sprintf] huh?');
                            }
                        }
                    }
                    else {
                        throw('[sprintf] huh?');
                    }
                    match[2] = field_list;
                }
                else {
                    arg_names |= 2;
                }
                if (arg_names === 3) {
                    throw('[sprintf] mixing positional and named placeholders is not (yet) supported');
                }
                parse_tree.push(match);
            }
            else {
                throw('[sprintf] huh?');
            }
            _fmt = _fmt.substring(match[0].length);
        }
        return parse_tree;
    };

    return str_format;
})();

var vsprintf = function (fmt, argv) {
    argv.unshift(fmt);
    return sprintf.apply(null, argv);
};

//end sprintf


BootLoader.prototype.tickBegin = function () {
    this.tickTime = Date.now();
};

BootLoader.prototype.tickEnd = function (msg) {
    this.tickTime2 = Date.now();
    cc.log(msg + " spend:" + (this.tickTime2 - this.tickTime) + " ms.");
};

BootLoader.prototype.checkPackageFirstRun = function () {
    var me = this;
    Pg.showGL();
    me.loadingNode = me.loadLoadingScene();
    var loader = QideaCC.Downloader.getInstance();
    //检查是否为新包
    var newVersion = me.readFile(me.getPackagePath("ipa.version.json"));
    var oldVersion = me.readFile(me.getLocalPath("ipa.version.json"));
    if (oldVersion == null || JSON.parse(newVersion).version != JSON.parse(oldVersion).version) {
        QideaCC.removeFile(QideaCC.getDocumentsDir() + "/JSG/" + "first.json");
        cc.log("有新包需要安装...");
    }

    cc.log("checkPackageFirstRun");
    //检测 Document 中是否存在  文件 first.json
    var firstJSON = me.readFile(me.getLocalPath("first.json"));
    if (firstJSON == null) {
        cc.log("is first setup");
        //检测是否要下载新app.json
        var appJSON = me.readFile(me.getPackagePath("app.json"));
        if (appJSON != null) {
            appJSON = JSON.parse(appJSON);
        } else {
            appJSON = {};
        }
        //var fileIndex=0;
        // var fileCount=0;
        //
        var filesize = 0;
        loader.clear();
        for (var file in appJSON) {
            //  fileIndex++;
            //   cc.log(fileIndex+"file:"+file);
            filesize += file["size"];
            loader.moveResource(me.getPackagePath(file), me.getLocalPath(file), false);
        }
        var sysFreeSize = QideaCC.IOSavailableMemory(); //计算设备空间
        cc.log("设备可用空间" + sysFreeSize);
        if (sysFreeSize != -1 && sysFreeSize < filesize) {
            QideaCC.showAlert("空间不足异常",
                "可用储存空间不足,无法启动游戏.你可用在设置中管理储存空间",
                function (indx) {
                    if (indx == 0) {

                        QideaCC.IOSOpenUrl("prefs:root=General");//弹出储存

                    }
                },
                "设置",
                "");
            return;
        }
        //开启进度条显示
        me.loadingNode.controller.show(CBootLoader.COMP_TYPE.Process);
        me.loadingNode.controller.updateProgress(0, "正在初始化游戏内容");
        loader.setDelegate(this, function (mgr) {
            me.loadingNode.controller.updateMsg("初始化内容失败:[Code:" + loader.getErrcode() + "]");
            cc.log("初始化内容失败:" + loader.getErrString());
            cc.log(mgr.getCurFileName());
        }, function (mgr) {
            //初始化成功
            firstJSON = {status:"done"};
            cc.log("move Done");
            var firstJSONData = JSON.stringify(firstJSON);
            me.writeFile(me.getLocalPath("first.json"), firstJSONData);

            me.writeFile(me.getLocalPath("ipa.version.json"), newVersion);


            //启动BootLoader
            me.startBootLoader();
        }, function (mgr) {
            cc.log(mgr.getDownLoadStatus());
            var downLoaderStatus = JSON.parse(mgr.getDownLoadStatus());
            //cc.log(downLoaderStatus.name);
            switch (downLoaderStatus.status) {
                case "move":
                case "delete":
                    var percent = (downLoaderStatus.curIndex / downLoaderStatus.count) * 100;
                    me.loadingNode.controller.updateProgress(percent, sprintf("正在初始化游戏内容(%3d/%3d)", downLoaderStatus.curIndex, downLoaderStatus.count));
                    me.loadingNode.controller.updateMsg2(percent.toFixed(0) + "%");
                    break;
            }

        }, 0);
        loader.DownLoaderBegin();

    } else {

        //启动BootLoader
        cc.log("is not first setup");
        me.startBootLoader();
    }
};
BootLoader.prototype.EnterGame = function () {
    IsInGame = true;
    QideaCC.setIdleTimerDisabled(false);
};
//游戏内检查是否有新的资源下载
BootLoader.prototype.checkVerionInGame = function () {
    var me = this;
    Pg.eval("Loading.showGlobalLoading();");
    var localAppCheckFile = me.readFile(me.getLocalPath("app_check.json"));
    if (localAppCheckFile != null) {
        cc.log("localAppCheckFile:" + localAppCheckFile);
        localAppCheckFile = JSON.parse(localAppCheckFile);
    } else {
        cc.log("localAppCheckFile is null");
        localAppCheckFile = {};
    }
    me.httpLoader.LoadFormRomote(me.getRemoteUrl("app_check.json"), 15, 0, function (loader) {
        var tips = "你的网络不给力啊(Code:" + loader.getCode() + ")请检查网络";
        me.loadingNode.controller.setCheckVersionMsg(tips);
        QideaCC.showAlert("网络出错异常 ",
            tips,
            function (indx) {
                if (indx == 0) {
                    QideaCC.IOSOpenUrl("prefs:root=General&path=INTERNATIONAL");//打开网络设置
                }
            },
            "设置",
            "");

    }, function (loader) {
        cc.log(loader.getData());
        me.remoteAppCheckFileOriginData = loader.getData();
        me.remoteAppCheckFile = JSON.parse(loader.getData());
        if (typeof localAppCheckFile["app.json"] != "undefined" && me.remoteAppCheckFile["app.json"].md5 == localAppCheckFile["app.json"].md5) {
            cc.log("版本校验完成：无需更新资源列表");
            Pg.eval("Loading.hiddenGlobalLoading();database.getKingInfo(null);");

            //调用客户端音乐检查函数
            cc.Director.getInstance().startAnimation();
            cc.AudioEngine.getInstance().resumeAllEffects();
            me.checkMusic();
        } else {
            cc.log("版本校验完成：需要更新资源列表");
            me.loadingNode = me.loadLoadingScene();
            Pg.hidePg();
            Pg.showGL();
            //启动下载器，下载更新资源
            me.startDownLoader();
        }
    }, function (loader) {
    });
};


BootLoader.prototype.startBootLoader = function () {
    var me = this;
    //检测是否要下载新app.json
    var localAppCheckFile = me.readFile(me.getLocalPath("app_check.json"));
    if (localAppCheckFile != null) {
        cc.log("localAppCheckFile:" + localAppCheckFile);
        localAppCheckFile = JSON.parse(localAppCheckFile);
    } else {
        cc.log("localAppCheckFile is null");
        localAppCheckFile = {};
    }
    //每次下载资源设置标志位
    IsInGame = false;
    me.loadingNode.controller.show(CBootLoader.COMP_TYPE.CheckVersion);
    me.loadingNode.controller.updateMsg2("");//清空显示进度
    me.loadingNode.controller.setCheckVersionMsg("正在检测版本");
    cc.log("app_check.json");
    me.httpLoader.LoadFormRomote(me.getRemoteUrl("app_check.json"), 15, 0, function (loader) {
        var tips = "你的网络不给力啊(Code:" + loader.getCode() + ")请检查网络";
        me.loadingNode.controller.setCheckVersionMsg(tips);
        QideaCC.showAlert("网络出错异常 ", tips,
            function (indx) {
                if (indx == 0) {
                    QideaCC.IOSOpenUrl("prefs:root=General&path=INTERNATIONAL");//打开网络设置
                }
            }, "设置", "");

    }, function (loader) {
        cc.log(loader.getData());
        me.remoteAppCheckFileOriginData = loader.getData();
        me.remoteAppCheckFile = JSON.parse(loader.getData());
        if (typeof localAppCheckFile["app.json"] != "undefined" && me.remoteAppCheckFile["app.json"].md5 == localAppCheckFile["app.json"].md5) {
            cc.log("版本校验完成：无需更新资源列表");
            //进入游戏
            if (IsInGame) {
                cc.log("in Game");
            } else {
                cc.log("not in Game");
                qBootLoader.startGame();
                //qBootLoader.EnterGame();
                //me.loadingNode.controller.show(CBootLoader.COMP_TYPE.EnterGame );
            }

        } else {
            cc.log("版本校验完成：需要更新资源列表");
            //启动下载器，下载更新资源
            me.startDownLoader();
        }
    }, function (loader) {
    });
};

BootLoader.prototype.startGame = function () {
    var me = this;

    QideaCC.runJSscript("cocosGame.js");
    me.loadingNode.controller.show(CBootLoader.COMP_TYPE.CheckVersion);
    me.loadingNode.controller.setCheckVersionMsg("正在初始化0%");
    cc.log("正在启动游戏...");
    Pg.loadURL("index.html");
}
BootLoader.prototype.getDownLoadResourceList = function (loader, success, fail) {
    var me = this;
    me.httpLoader.LoadFormRomote(me.getRemoteUrl("app.json"), 15, 0, function (loader) {
        // me.loadingNode.controller.updateProgress(0,"资源更新列表加载失败:【Code:"+loader.getCode()+"]");
        fail();
    }, function (loader) {
        cc.log("app.json-------------------");
        cc.log(loader.getData());
        me.remoteAppJsonOriginData = loader.getData();
        me.remoteAppJson = JSON.parse(loader.getData());
        var localAppJson = me.readFile(me.getLocalPath("app.json"));
        cc.log(loader.getData());
        cc.log("-------------------");
        cc.log(localAppJson);
        if (localAppJson != null) {
            localAppJson = JSON.parse(localAppJson);
        } else {
            localAppJson = {};
        }
        //计算需要更新的文件
        me.downLoadInfo = {
            downLoadResList:[],
            totalSize:0
        };
        me.tickBegin();
        for (var file in me.remoteAppJson) {
            if (typeof localAppJson[file] == "undefined" ||
                typeof localAppJson[file]["md5"] == "undefined" ||
                localAppJson[file]["md5"] != me.remoteAppJson[file]["md5"]) {
                if (file != "app.json" && file != "app_check.json") {
                    me.downLoadInfo.downLoadResList.push({name:file, file:me.remoteAppJson[file], md5:me.remoteAppJson[file]["md5"]});
                    me.downLoadInfo.totalSize += me.remoteAppJson[file].size;
                }
            }
        }
        me.tickEnd("finish calc download res");
        //保存到临时文件
        me.writeFile(me.getSavePath("app_new.json"), loader.getData());
        //wifi tips
        if (me.downLoadInfo.totalSize > 0) {
            var sizeM = me.downLoadInfo.totalSize / 1024 / 1024;
            if (sizeM >= 3 && !QideaCC.IOSUsedWifi()) {
                QideaCC.showAlert("提示 ",
                    "游戏没有检测到wifi连接,是否使用2G/3G下载新的资源",
                    function (indx) {
                        if (indx == 0) {
                            success();
                        } else {
                            QideaCC.IOSOpenUrl("prefs:root=WIFI");//打开WIFI设置
                        }
                    },
                    "确定",
                    "取消");
            } else {
                success();
            }
        } else {
            success();
        }

    }, function (loader) {
        var status = loader.getLoadStatus();
        cc.log("正在下载资源列表:" + status);
        //me.loadingNode.controller.setCheckVersionMsg("正在下载资源列表");
    });
}

BootLoader.prototype.downLoadResources = function (loader) {
    var me = this;
    var lastDownloadSize = 0;
    var updateCount = 0;
    //更新需要下载的资源文件
    me.tickBegin();
    loader.clear();
    for (var i = 0; i < me.downLoadInfo.downLoadResList.length; i++) {
        var resObj = me.downLoadInfo.downLoadResList[i];
        cc.log("更新需要下载的资源文件:" + me.getRemoteUrl(resObj.name) + " ----------- " + resObj.md5);
        loader.addResource(me.getRemoteCDNUrl(resObj.name, JSON.parse(me.remoteAppCheckFileOriginData)), me.getSavePath(resObj.name), resObj.md5);
    }
    me.tickEnd("finish loader.addResource ..... ");
    var totalSize = me.downLoadInfo.totalSize / 1024 / 1024;
    me.loadingNode.controller.show(CBootLoader.COMP_TYPE.Process);
    me.loadingNode.controller.updateProgress(0, sprintf("正在更新资源,共 %sM (%3d/%3d)", totalSize.toFixed(1), 1, me.downLoadInfo.downLoadResList.length));
    me.loadingNode.controller.updateMsg2("0%");
    loader.setDelegate(this, function (status) {
        cc.log("Errcode" + loader.getErrcode() + "| curFileName:" + loader.getCurFileName());
        me.loadingNode.controller.updateMsg("更新资源失败(Code:" + loader.getErrcode() + ")");
        me.loadingNode.controller.show(CBootLoader.COMP_TYPE.DialogMsg);
    }, function (mgr) {

        //启动游戏
        cc.log("finished download start Game ");
        //cc.log("currFileName:"+mgr.getCurFileName()+me.tickEnd("finished DownLoaderBegin"));
        //进入游戏
        //me.loadingNode.controller.show(CBootLoader.COMP_TYPE.EnterGame );
        qBootLoader.startGame();
        //qBootLoader.EnterGame();
        //保存新的资源列表
        me.writeFile(me.getSavePath("app_check.json"), me.remoteAppCheckFileOriginData);
        me.writeFile(me.getSavePath("app.json"), me.remoteAppJsonOriginData);

    }, function (mgr) {
        cc.log(mgr.getDownLoadStatus());
        var downLoaderStatus = JSON.parse(mgr.getDownLoadStatus());
        //cc.log(downLoaderStatus.name);
        switch (downLoaderStatus.status) {
            case "download":
                // if(downLoaderStatus.size>0){
                // updateCount++;
                // var totalProgress =(downLoaderStatus.total/1024/1024).toFixed(2)+"/"+(me.downLoadInfo.totalSize/1024/1024).toFixed(2)+"M";
                var totalProgress = 0;
                if (downLoaderStatus.total > 0) {
                    totalProgress = downLoaderStatus.total / me.downLoadInfo.totalSize * 100;
                }

                var percent = 0;
                if (downLoaderStatus.size > 0) {
                    percent = (downLoaderStatus.cur / downLoaderStatus.size * 100);
                }
                var fileName = downLoaderStatus.name.substr(0, downLoaderStatus.name.lastIndexOf("."));
                var fileCount = downLoaderStatus.count;
                var currFileIndex = downLoaderStatus.curIndex;
                var strTip = sprintf("正在更新资源,共 %sM (%3d/%3d)", totalSize.toFixed(1), currFileIndex, fileCount);
                me.loadingNode.controller.updateProgress(parseInt(percent), strTip);
                me.loadingNode.controller.updateMsg2(totalProgress.toFixed(1) + "%");
                break;
            case "move":
            case "delete":
                var percent = (downLoaderStatus.curIndex / downLoaderStatus.count);
                me.loadingNode.controller.updateProgress(percent * 100.0, sprintf("正在配置资源, (%2d/%2d)", downLoaderStatus.curIndex, downLoaderStatus.count));
                me.loadingNode.controller.updateMsg2((percent.toFixed(3) * 100) + "%");
                break;
        }

    }, 0.0);
    me.tickBegin();
    loader.DownLoaderBegin();
    me.tickEnd("finished DownLoaderBegin");
};
BootLoader.prototype.startDownLoader = function () {
    var me = this;
    var loader = QideaCC.Downloader.getInstance();
    loader.setTimeOut(15, 15); //调整下载器超时时间
    cc.log("beigin soureces list");
    //获取资源更新列表

    me.loadingNode.controller.setCheckVersionMsg("正在下载资源列表");
    me.getDownLoadResourceList(loader, function () {
        me.downLoadResources(loader);
    }, function () {
        var tips = "你的网络不给力啊(Code:" + loader.getCode() + ")请检查网络";
        me.loadingNode.controller.setCheckVersionMsg(tips);
        QideaCC.showAlert("网络出错异常 ",
            tips,
            function (indx) {
                if (indx == 0) {
                    QideaCC.IOSOpenUrl("prefs:root=General&path=INTERNATIONAL");//打开网络设置
                }
            },
            "设置",
            "");
    });
};
BootLoader.prototype.loadLoadingScene = function () {
    var me = this;
    var loadingNode = null;

    try {
        loadingNode = cc.BuilderReader.load("image/interface/BootLoader.ccbi");
    } catch (e) {
        me.moveFile(me.getPackagePath("image/interface/BootLoader.ccbi"), me.getLocalPath("image/interface/BootLoader.ccbi"));
        loadingNode = cc.BuilderReader.load("image/interface/BootLoader.ccbi");
    }

    var runningScene = cc.Director.getInstance().getRunningScene();
    if (runningScene === null)
        cc.Director.getInstance().runWithScene(loadingNode);
    else
        cc.Director.getInstance().replaceScene(loadingNode);

    return loadingNode;
};
BootLoader.prototype.showLoading = function (msg) {
    var me = this;
    var loadingNode = me.loadLoadingScene();
    loadingNode.controller.show(CBootLoader.COMP_TYPE.CheckVersion);
    loadingNode.controller.checkVersionLog.setString(msg);
}
BootLoader.prototype.checkMusic = function () {
    //调用Pg里的checkMusic函数
    Pg.eval("Qidea.checkMusic();");
};
BootLoader.prototype.readFile = function (fileName) {
    return cc.FileUtils.getInstance().getFileData(fileName, "rb", 0);
};
BootLoader.prototype.writeFile = function (fileName, data) {
    cc.FileUtils.getInstance().setFileData(fileName, "w", data, data.replace(/[^\x00-\xff]/g, "**").length);
};
BootLoader.prototype.moveFile = function (source, target) {
    cc.FileUtils.getInstance().moveFileData(source, target, false);
};

BootLoader.prototype.getRemoteUrl = function (url) {
    return remoteServerSite + remoteMid + url;
};

BootLoader.prototype.getRemoteCDNUrl = function (url, data) {
    var me = this;
    var arr = data.r;
    if (arr && arr.length) {
        for (var i = 0; i < arr.length; i++) {
            var item = arr[i];
            //var reg = new RegExp(item.pattern,"i");
            //if ( reg.test(url)){
            if (url.substr(url.indexOf(".") + 1) == item.pattern) {
                var newURL = item.domain + remoteMid + url;
                cc.log("url via cdn:" + newURL);
                return newURL;
            }
        }
        return remoteServerSite + remoteMid + url;
    }
    else {
        return remoteServerSite + remoteMid + url;
    }
};

BootLoader.prototype.getSavePath = function (url) {
    return "../../" + rootName + "/" + url;
};
BootLoader.prototype.getLocalPath = function (url) {
    return "../../" + rootName + "/" + url;
};
BootLoader.prototype.getPackagePath = function (url) {
    return "../../" + appPackName + "/" + url;
};


//启动Loader
qBootLoader = new BootLoader();


function QSqlite() {
    cc.log("create qSqliteInstance  ");
    var me = this;
    this.db = QideaCC.Database(me.getLocalPath("JSGConst.db3"), Qidea_Open_FLag.Qidea_SQLITE_OPEN_READWRITE);
}

QSqlite.prototype.getLocalPath = function (url) {
    return "../../" + rootName + "/" + url;
};

/**
 *返回sqlite里面的全部数据,"#"
 **/
QSqlite.prototype.getDBData = function () {
    var res = "";
    var query = QideaCC.Query(this.db, "select * from CONST");
    while (query.executeStep()) {
        var name = query.getColumn(0).getText();
        cc.log("name:" + name);
        res += (name + "#");
        var version = query.getColumn(1).getText();
        cc.log("version:" + version);
        res += (version + "#");
        var data = query.getColumn(2).getText();
        cc.log("data:" + data.length);
        res += (data + "#");
    }
    cc.log("getDBData DONE");
    return res;
}

/**
 *设置一个item的数据
 **/
QSqlite.prototype.execSql = function (sqlStaement) {
    this.db.exec(sqlStaement);
}

qSqliteInstance = new QSqlite();