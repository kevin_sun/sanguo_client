window.bootLoader={loading:{}};
bootLoader.config={
    /**
     * 指向正式版或者测试版服务器
     */
    isDebug:false
    ,
    useLess:false
    ,
    URL_SCRIPTS:[]
    ,
    JSFILE:[{name:"cocosLib.js",isLocal:true},{name:"mjsgLib.js",isLocal:true},{name:"mjsg.js",isLocal:true}]
    ,
    totalStep:5
    ,
    currStep:0
};
var config = bootLoader.config;
config.serverIP = config.isDebug?"http://core.zdsanguo.com":"http://core.zdsanguo.com";
config.tdName="core";
config.serverPath = "/rescenter/v1/";

config.update=function(){
    var url = config.serverIP+config.serverPath;
    for(var i=0;i<config.JSFILE.length;i++)
    {
        if(!config.isDebug)
        {
            config.JSFILE[i]["name"] = config.JSFILE[i]["name"].replace(".js",".min.js");
        }
        if(config.JSFILE[i].isLocal){
            config.URL_SCRIPTS.push(config.JSFILE[i].name);
        }else{
            config.URL_SCRIPTS.push(url+config.JSFILE[i].name);
        }
    }
    if(config.useLess)
    {
        config.URL_CSS="mjsg.new.less";
    }
    else
    {
        //添加分辨率css
        config.URL_CSS="mjsg.new.css".replace(".css",document.width+"x"+document.height+".css");
        console.log(config.URL_CSS);
    }
    config.URL_HTML="mjsg.html";
    /**
     * 不同的版本做不同的修改
     * @type {String}
     */
    config.URL_LANG="i18n/lang_zh_cn.json";
    config.URL_APP_JSON="app.json";
};
config.update();
bootLoader.getFileName=function(filePath){
    var fileName=filePath.substr(filePath.lastIndexOf("/")+1);
    fileName=fileName.substr(fileName.lastIndexOf("\\")+1);
    return fileName;
};
bootLoader.isLoadFromRemote=function(filePath){
    var mustLoad=true;
    var fileName=bootLoader.getFileName(filePath);
    if(fileName in appJSON){
        mustLoad=appJSON[fileName].load;
    }
    return mustLoad;
};

bootLoader.tickBegin=function(){
    bootLoader.tickTime = Date.now();
};

bootLoader.tickEnd=function(msg){
    bootLoader.tickTime2 = Date.now();
    console.log(msg+" spend:"+(bootLoader.tickTime2-bootLoader.tickTime)+" ms.");
};

bootLoader.loader={
    start:function()
    {
        $.ajaxSetup({cache:true});
        GameLoading.init();
        GameLoading.showLoadStep(0.1);
        this.loadHtml();
    }
    ,
    loadCSS:function()
    {
        var me = this;
        var trytime=0;

        var percent =  100.0*config.currStep/config.totalStep;
        if(typeof cordova!="undefined" && typeof cordova.exec!="undefined")cordova.exec(function(){},function(){},"CcEval","evalInBg",[ "qBootLoader.loadingNode.controller.setCheckVersionMsg('正在初始化"+percent+"%');"]);
        bootLoader.tickBegin();
        var currentAjax=$.get(config.URL_CSS)
            .done(function(text, textStatus) {
                bootLoader.tickEnd(config.URL_CSS);
                //GameLoading.showLoadStep(config.currStep++);
                config.currStep++
                var style = document.createElement("style");
                style.type = config.useLess?"text/less": "text/css";
                style.innerHTML = text;
                document.head.appendChild(style);
                percent =  100.0*config.currStep/config.totalStep;
                if(typeof cordova!="undefined" && typeof cordova.exec!="undefined")cordova.exec(function(){},function(){},"CcEval","evalInBg",[ "qBootLoader.loadingNode.controller.setCheckVersionMsg('正在初始化"+percent+"%');"]);
                me.loadScript();
            })
            .fail(function(jqxhr, settings, exception){
                trytime++;
                currentAjax.abort();
                if(trytime<=3){
                    me.loadCSS();
                }else{
                    alert("网络异常 请检查网络");
                }
            });

    }
    ,
    loadScript:function()
    {
        $.ajaxSetup({cache:true});
        var me = this;
        var trytime=0;
        bootLoader.tickBegin();
        var percent =  100.0*config.currStep/config.totalStep;
        if(typeof cordova!="undefined" && typeof cordova.exec!="undefined")cordova.exec(function(){},function(){},"CcEval","evalInBg",[ "qBootLoader.loadingNode.controller.setCheckVersionMsg('正在初始化"+percent+"%');"]);
        var currentAjax=$.getScript(config.URL_SCRIPTS[0])
            .done(function(text, textStatus) {
                bootLoader.tickEnd(config.URL_SCRIPTS[0]);
                config.URL_SCRIPTS.shift();
                if(config.URL_SCRIPTS.length)
                {
                    console.log("Script:"+config.URL_SCRIPTS[0]);
                    //GameLoading.showLoadStep(config.currStep++);
                    config.currStep++
                    percent =  100.0*config.currStep/config.totalStep;
                    if(typeof cordova!="undefined" && typeof cordova.exec!="undefined")cordova.exec(function(){},function(){},"CcEval","evalInBg",[ "qBootLoader.loadingNode.controller.setCheckVersionMsg('正在初始化"+percent+"%');"]);
                    me.loadScript();
                }
                else
                {
                    me.loadLang();
                }
            })
            .fail(function(jqxhr, settings, exception){
                //解析出错，重新下载   mjsg.min.js
                if(jqxhr.statusText=="parsererror" || jqxhr.statusText=="error" ){

                    currentAjax.abort();
                    var strJs = "qBootLoader.reDownLoader('mjsg.min.js');";
                    if(typeof cordova!="undefined" && typeof cordova.exec!="undefined")cordova.exec(function(){},function(){},"CcEval","evalInBg",
                        [ strJs ]);
                }else{
                    trytime++;
                    currentAjax.abort();
                    if(trytime<=3){
                        me.loadScript();
                    }else{
                        alert("网络异常 请检查网络");
                    }
                }
            });
    }
    ,
    loadHtml:function()
    {
        var me = this;
        bootLoader.tickBegin();
        var percent =  100.0*config.currStep/config.totalStep;
        if(typeof cordova!="undefined" && typeof cordova.exec!="undefined")cordova.exec(function(){},function(){},"CcEval","evalInBg",[ "qBootLoader.loadingNode.controller.setCheckVersionMsg('正在初始化"+percent+"%');"]);
        $("#glsg-tmpl-wrapper").load(config.URL_HTML,function(text){
            bootLoader.tickEnd(config.URL_HTML);
            //GameLoading.showLoadStep(config.currStep++);
            config.currStep++
            percent =  100.0*config.currStep/config.totalStep;
            if(typeof cordova!="undefined" && typeof cordova.exec!="undefined")cordova.exec(function(){},function(){},"CcEval","evalInBg",[ "qBootLoader.loadingNode.controller.setCheckVersionMsg('正在初始化"+percent+"%');"]);
            me.loadCSS();
        });
    }
    ,
    loadLang:function()
    {
        var me = this;
        bootLoader.tickBegin();
        var trytime=0;
        var percent =  100.0*config.currStep/config.totalStep;
        if(typeof cordova!="undefined" && typeof cordova.exec!="undefined")cordova.exec(function(){},function(){},"CcEval","evalInBg",[ "qBootLoader.loadingNode.controller.setCheckVersionMsg('正在初始化"+percent+"%');"]);
        var currentAjax=$.getJSON(config.URL_LANG)
            .done(function(data,status) {
                bootLoader.tickEnd(config.URL_LANG);
                //GameLoading.showLoadStep(config.currStep++);
                config.currStep++
                CONFIG.langs = data;
                percent =  100.0*config.currStep/config.totalStep;
               // if(typeof cordova!="undefined" && typeof cordova.exec!="undefined")cordova.exec(function(){},function(){},"CcEval","evalInBg",["qBootLoader.loadingNode.controller.updateProgress("+percent+",'正在初始化资源 ("+config.currStep+"/"+config.totalStep+") ');"]);
                if(typeof cordova!="undefined" && typeof cordova.exec!="undefined")cordova.exec(function(){},function(){},"CcEval","evalInBg",[ "qBootLoader.loadingNode.controller.setCheckVersionMsg('正在初始化"+percent+"%');"]);
                me.finish();
            })
            .fail(function(jqxhr, settings, exception){
                trytime++;
                currentAjax.abort();
                if(trytime<=3){
                    me.loadLang();
                }else{
                    alert("网络异常 请检查网络");
                }
            });
    }
    ,
    finish:function()
    {
        console.log("finish loader.js");
        Qidea.InitGame();
        GameLoading.showLoadStep(100,"初始化完毕");
        GameLoading.remove();
        setTimeout(function()
        {
            if(typeof cordova!="undefined" && typeof cordova.exec!="undefined"){
                cordova.exec(function(){},function(){},"CcEval","showPg",[]);
                cordova.exec(function(){},function(){},"CcEval","hideGL",[]);
            }
        },500);
    }
};


if(typeof Array.prototype.___pop=="undefined")
{
    console.log("replace Array.prototype.pop");
    Array.prototype.___pop = Array.prototype.pop;
    
    /**
     * 弹出一些符合条件的元素
     * @param fn
     * @return {*}
     */
    Array.prototype.pop = function(fn)
    {
        if(fn === undefined)
        {
            return this.___pop();
        }
        var res = [];
        for(var i=0;i<this.length;i++)
        {
            if(!!(fn(this[i])))
            {
                res.push(this[i]);
                this.splice(i,1);
                i-=1;
            }
        }
        return res.length==1?res[0]:res;
    };
}