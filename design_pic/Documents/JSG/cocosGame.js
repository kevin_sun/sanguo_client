//     Underscore.js 1.4.3
//     http://underscorejs.org
//     (c) 2009-2012 Jeremy Ashkenas, DocumentCloud Inc.
//     Underscore may be freely distributed under the MIT license.

(function() {

  // Baseline setup
  // --------------

  // Establish the root object, `window` in the browser, or `global` on the server.
  var root = this;

  // Save the previous value of the `_` variable.
  var previousUnderscore = root._;

  // Establish the object that gets returned to break out of a loop iteration.
  var breaker = {};

  // Save bytes in the minified (but not gzipped) version:
  var ArrayProto = Array.prototype, ObjProto = Object.prototype, FuncProto = Function.prototype;

  // Create quick reference variables for speed access to core prototypes.
  var push             = ArrayProto.push,
      slice            = ArrayProto.slice,
      concat           = ArrayProto.concat,
      toString         = ObjProto.toString,
      hasOwnProperty   = ObjProto.hasOwnProperty;

  // All **ECMAScript 5** native function implementations that we hope to use
  // are declared here.
  var
    nativeForEach      = ArrayProto.forEach,
    nativeMap          = ArrayProto.map,
    nativeReduce       = ArrayProto.reduce,
    nativeReduceRight  = ArrayProto.reduceRight,
    nativeFilter       = ArrayProto.filter,
    nativeEvery        = ArrayProto.every,
    nativeSome         = ArrayProto.some,
    nativeIndexOf      = ArrayProto.indexOf,
    nativeLastIndexOf  = ArrayProto.lastIndexOf,
    nativeIsArray      = Array.isArray,
    nativeKeys         = Object.keys,
    nativeBind         = FuncProto.bind;

  // Create a safe reference to the Underscore object for use below.
  var _ = function(obj) {
    if (obj instanceof _) return obj;
    if (!(this instanceof _)) return new _(obj);
    this._wrapped = obj;
  };

  // Export the Underscore object for **Node.js**, with
  // backwards-compatibility for the old `require()` API. If we're in
  // the browser, add `_` as a global object via a string identifier,
  // for Closure Compiler "advanced" mode.
  if (typeof exports !== 'undefined') {
    if (typeof module !== 'undefined' && module.exports) {
      exports = module.exports = _;
    }
    exports._ = _;
  } else {
    root._ = _;
  }

  // Current version.
  _.VERSION = '1.4.3';

  // Collection Functions
  // --------------------

  // The cornerstone, an `each` implementation, aka `forEach`.
  // Handles objects with the built-in `forEach`, arrays, and raw objects.
  // Delegates to **ECMAScript 5**'s native `forEach` if available.
  var each = _.each = _.forEach = function(obj, iterator, context) {
    if (obj == null) return;
    if (nativeForEach && obj.forEach === nativeForEach) {
      obj.forEach(iterator, context);
    } else if (obj.length === +obj.length) {
      for (var i = 0, l = obj.length; i < l; i++) {
        if (iterator.call(context, obj[i], i, obj) === breaker) return;
      }
    } else {
      for (var key in obj) {
        if (_.has(obj, key)) {
          if (iterator.call(context, obj[key], key, obj) === breaker) return;
        }
      }
    }
  };

  // Return the results of applying the iterator to each element.
  // Delegates to **ECMAScript 5**'s native `map` if available.
  _.map = _.collect = function(obj, iterator, context) {
    var results = [];
    if (obj == null) return results;
    if (nativeMap && obj.map === nativeMap) return obj.map(iterator, context);
    each(obj, function(value, index, list) {
      results[results.length] = iterator.call(context, value, index, list);
    });
    return results;
  };

  var reduceError = 'Reduce of empty array with no initial value';

  // **Reduce** builds up a single result from a list of values, aka `inject`,
  // or `foldl`. Delegates to **ECMAScript 5**'s native `reduce` if available.
  _.reduce = _.foldl = _.inject = function(obj, iterator, memo, context) {
    var initial = arguments.length > 2;
    if (obj == null) obj = [];
    if (nativeReduce && obj.reduce === nativeReduce) {
      if (context) iterator = _.bind(iterator, context);
      return initial ? obj.reduce(iterator, memo) : obj.reduce(iterator);
    }
    each(obj, function(value, index, list) {
      if (!initial) {
        memo = value;
        initial = true;
      } else {
        memo = iterator.call(context, memo, value, index, list);
      }
    });
    if (!initial) throw new TypeError(reduceError);
    return memo;
  };

  // The right-associative version of reduce, also known as `foldr`.
  // Delegates to **ECMAScript 5**'s native `reduceRight` if available.
  _.reduceRight = _.foldr = function(obj, iterator, memo, context) {
    var initial = arguments.length > 2;
    if (obj == null) obj = [];
    if (nativeReduceRight && obj.reduceRight === nativeReduceRight) {
      if (context) iterator = _.bind(iterator, context);
      return initial ? obj.reduceRight(iterator, memo) : obj.reduceRight(iterator);
    }
    var length = obj.length;
    if (length !== +length) {
      var keys = _.keys(obj);
      length = keys.length;
    }
    each(obj, function(value, index, list) {
      index = keys ? keys[--length] : --length;
      if (!initial) {
        memo = obj[index];
        initial = true;
      } else {
        memo = iterator.call(context, memo, obj[index], index, list);
      }
    });
    if (!initial) throw new TypeError(reduceError);
    return memo;
  };

  // Return the first value which passes a truth test. Aliased as `detect`.
  _.find = _.detect = function(obj, iterator, context) {
    var result;
    any(obj, function(value, index, list) {
      if (iterator.call(context, value, index, list)) {
        result = value;
        return true;
      }
    });
    return result;
  };

  // Return all the elements that pass a truth test.
  // Delegates to **ECMAScript 5**'s native `filter` if available.
  // Aliased as `select`.
  _.filter = _.select = function(obj, iterator, context) {
    var results = [];
    if (obj == null) return results;
    if (nativeFilter && obj.filter === nativeFilter) return obj.filter(iterator, context);
    each(obj, function(value, index, list) {
      if (iterator.call(context, value, index, list)) results[results.length] = value;
    });
    return results;
  };

  // Return all the elements for which a truth test fails.
  _.reject = function(obj, iterator, context) {
    return _.filter(obj, function(value, index, list) {
      return !iterator.call(context, value, index, list);
    }, context);
  };

  // Determine whether all of the elements match a truth test.
  // Delegates to **ECMAScript 5**'s native `every` if available.
  // Aliased as `all`.
  _.every = _.all = function(obj, iterator, context) {
    iterator || (iterator = _.identity);
    var result = true;
    if (obj == null) return result;
    if (nativeEvery && obj.every === nativeEvery) return obj.every(iterator, context);
    each(obj, function(value, index, list) {
      if (!(result = result && iterator.call(context, value, index, list))) return breaker;
    });
    return !!result;
  };

  // Determine if at least one element in the object matches a truth test.
  // Delegates to **ECMAScript 5**'s native `some` if available.
  // Aliased as `any`.
  var any = _.some = _.any = function(obj, iterator, context) {
    iterator || (iterator = _.identity);
    var result = false;
    if (obj == null) return result;
    if (nativeSome && obj.some === nativeSome) return obj.some(iterator, context);
    each(obj, function(value, index, list) {
      if (result || (result = iterator.call(context, value, index, list))) return breaker;
    });
    return !!result;
  };

  // Determine if the array or object contains a given value (using `===`).
  // Aliased as `include`.
  _.contains = _.include = function(obj, target) {
    if (obj == null) return false;
    if (nativeIndexOf && obj.indexOf === nativeIndexOf) return obj.indexOf(target) != -1;
    return any(obj, function(value) {
      return value === target;
    });
  };

  // Invoke a method (with arguments) on every item in a collection.
  _.invoke = function(obj, method) {
    var args = slice.call(arguments, 2);
    return _.map(obj, function(value) {
      return (_.isFunction(method) ? method : value[method]).apply(value, args);
    });
  };

  // Convenience version of a common use case of `map`: fetching a property.
  _.pluck = function(obj, key) {
    return _.map(obj, function(value){ return value[key]; });
  };

  // Convenience version of a common use case of `filter`: selecting only objects
  // with specific `key:value` pairs.
  _.where = function(obj, attrs) {
    if (_.isEmpty(attrs)) return [];
    return _.filter(obj, function(value) {
      for (var key in attrs) {
        if (attrs[key] !== value[key]) return false;
      }
      return true;
    });
  };

  // Return the maximum element or (element-based computation).
  // Can't optimize arrays of integers longer than 65,535 elements.
  // See: https://bugs.webkit.org/show_bug.cgi?id=80797
  _.max = function(obj, iterator, context) {
    if (!iterator && _.isArray(obj) && obj[0] === +obj[0] && obj.length < 65535) {
      return Math.max.apply(Math, obj);
    }
    if (!iterator && _.isEmpty(obj)) return -Infinity;
    var result = {computed : -Infinity, value: -Infinity};
    each(obj, function(value, index, list) {
      var computed = iterator ? iterator.call(context, value, index, list) : value;
      computed >= result.computed && (result = {value : value, computed : computed});
    });
    return result.value;
  };

  // Return the minimum element (or element-based computation).
  _.min = function(obj, iterator, context) {
    if (!iterator && _.isArray(obj) && obj[0] === +obj[0] && obj.length < 65535) {
      return Math.min.apply(Math, obj);
    }
    if (!iterator && _.isEmpty(obj)) return Infinity;
    var result = {computed : Infinity, value: Infinity};
    each(obj, function(value, index, list) {
      var computed = iterator ? iterator.call(context, value, index, list) : value;
      computed < result.computed && (result = {value : value, computed : computed});
    });
    return result.value;
  };

  // Shuffle an array.
  _.shuffle = function(obj) {
    var rand;
    var index = 0;
    var shuffled = [];
    each(obj, function(value) {
      rand = _.random(index++);
      shuffled[index - 1] = shuffled[rand];
      shuffled[rand] = value;
    });
    return shuffled;
  };

  // An internal function to generate lookup iterators.
  var lookupIterator = function(value) {
    return _.isFunction(value) ? value : function(obj){ return obj[value]; };
  };

  // Sort the object's values by a criterion produced by an iterator.
  _.sortBy = function(obj, value, context) {
    var iterator = lookupIterator(value);
    return _.pluck(_.map(obj, function(value, index, list) {
      return {
        value : value,
        index : index,
        criteria : iterator.call(context, value, index, list)
      };
    }).sort(function(left, right) {
      var a = left.criteria;
      var b = right.criteria;
      if (a !== b) {
        if (a > b || a === void 0) return 1;
        if (a < b || b === void 0) return -1;
      }
      return left.index < right.index ? -1 : 1;
    }), 'value');
  };

  // An internal function used for aggregate "group by" operations.
  var group = function(obj, value, context, behavior) {
    var result = {};
    var iterator = lookupIterator(value || _.identity);
    each(obj, function(value, index) {
      var key = iterator.call(context, value, index, obj);
      behavior(result, key, value);
    });
    return result;
  };

  // Groups the object's values by a criterion. Pass either a string attribute
  // to group by, or a function that returns the criterion.
  _.groupBy = function(obj, value, context) {
    return group(obj, value, context, function(result, key, value) {
      (_.has(result, key) ? result[key] : (result[key] = [])).push(value);
    });
  };

  // Counts instances of an object that group by a certain criterion. Pass
  // either a string attribute to count by, or a function that returns the
  // criterion.
  _.countBy = function(obj, value, context) {
    return group(obj, value, context, function(result, key) {
      if (!_.has(result, key)) result[key] = 0;
      result[key]++;
    });
  };

  // Use a comparator function to figure out the smallest index at which
  // an object should be inserted so as to maintain order. Uses binary search.
  _.sortedIndex = function(array, obj, iterator, context) {
    iterator = iterator == null ? _.identity : lookupIterator(iterator);
    var value = iterator.call(context, obj);
    var low = 0, high = array.length;
    while (low < high) {
      var mid = (low + high) >>> 1;
      iterator.call(context, array[mid]) < value ? low = mid + 1 : high = mid;
    }
    return low;
  };

  // Safely convert anything iterable into a real, live array.
  _.toArray = function(obj) {
    if (!obj) return [];
    if (_.isArray(obj)) return slice.call(obj);
    if (obj.length === +obj.length) return _.map(obj, _.identity);
    return _.values(obj);
  };

  // Return the number of elements in an object.
  _.size = function(obj) {
    if (obj == null) return 0;
    return (obj.length === +obj.length) ? obj.length : _.keys(obj).length;
  };

  // Array Functions
  // ---------------

  // Get the first element of an array. Passing **n** will return the first N
  // values in the array. Aliased as `head` and `take`. The **guard** check
  // allows it to work with `_.map`.
  _.first = _.head = _.take = function(array, n, guard) {
    if (array == null) return void 0;
    return (n != null) && !guard ? slice.call(array, 0, n) : array[0];
  };

  // Returns everything but the last entry of the array. Especially useful on
  // the arguments object. Passing **n** will return all the values in
  // the array, excluding the last N. The **guard** check allows it to work with
  // `_.map`.
  _.initial = function(array, n, guard) {
    return slice.call(array, 0, array.length - ((n == null) || guard ? 1 : n));
  };

  // Get the last element of an array. Passing **n** will return the last N
  // values in the array. The **guard** check allows it to work with `_.map`.
  _.last = function(array, n, guard) {
    if (array == null) return void 0;
    if ((n != null) && !guard) {
      return slice.call(array, Math.max(array.length - n, 0));
    } else {
      return array[array.length - 1];
    }
  };

  // Returns everything but the first entry of the array. Aliased as `tail` and `drop`.
  // Especially useful on the arguments object. Passing an **n** will return
  // the rest N values in the array. The **guard**
  // check allows it to work with `_.map`.
  _.rest = _.tail = _.drop = function(array, n, guard) {
    return slice.call(array, (n == null) || guard ? 1 : n);
  };

  // Trim out all falsy values from an array.
  _.compact = function(array) {
    return _.filter(array, _.identity);
  };

  // Internal implementation of a recursive `flatten` function.
  var flatten = function(input, shallow, output) {
    each(input, function(value) {
      if (_.isArray(value)) {
        shallow ? push.apply(output, value) : flatten(value, shallow, output);
      } else {
        output.push(value);
      }
    });
    return output;
  };

  // Return a completely flattened version of an array.
  _.flatten = function(array, shallow) {
    return flatten(array, shallow, []);
  };

  // Return a version of the array that does not contain the specified value(s).
  _.without = function(array) {
    return _.difference(array, slice.call(arguments, 1));
  };

  // Produce a duplicate-free version of the array. If the array has already
  // been sorted, you have the option of using a faster algorithm.
  // Aliased as `unique`.
  _.uniq = _.unique = function(array, isSorted, iterator, context) {
    if (_.isFunction(isSorted)) {
      context = iterator;
      iterator = isSorted;
      isSorted = false;
    }
    var initial = iterator ? _.map(array, iterator, context) : array;
    var results = [];
    var seen = [];
    each(initial, function(value, index) {
      if (isSorted ? (!index || seen[seen.length - 1] !== value) : !_.contains(seen, value)) {
        seen.push(value);
        results.push(array[index]);
      }
    });
    return results;
  };

  // Produce an array that contains the union: each distinct element from all of
  // the passed-in arrays.
  _.union = function() {
    return _.uniq(concat.apply(ArrayProto, arguments));
  };

  // Produce an array that contains every item shared between all the
  // passed-in arrays.
  _.intersection = function(array) {
    var rest = slice.call(arguments, 1);
    return _.filter(_.uniq(array), function(item) {
      return _.every(rest, function(other) {
        return _.indexOf(other, item) >= 0;
      });
    });
  };

  // Take the difference between one array and a number of other arrays.
  // Only the elements present in just the first array will remain.
  _.difference = function(array) {
    var rest = concat.apply(ArrayProto, slice.call(arguments, 1));
    return _.filter(array, function(value){ return !_.contains(rest, value); });
  };

  // Zip together multiple lists into a single array -- elements that share
  // an index go together.
  _.zip = function() {
    var args = slice.call(arguments);
    var length = _.max(_.pluck(args, 'length'));
    var results = new Array(length);
    for (var i = 0; i < length; i++) {
      results[i] = _.pluck(args, "" + i);
    }
    return results;
  };

  // Converts lists into objects. Pass either a single array of `[key, value]`
  // pairs, or two parallel arrays of the same length -- one of keys, and one of
  // the corresponding values.
  _.object = function(list, values) {
    if (list == null) return {};
    var result = {};
    for (var i = 0, l = list.length; i < l; i++) {
      if (values) {
        result[list[i]] = values[i];
      } else {
        result[list[i][0]] = list[i][1];
      }
    }
    return result;
  };

  // If the browser doesn't supply us with indexOf (I'm looking at you, **MSIE**),
  // we need this function. Return the position of the first occurrence of an
  // item in an array, or -1 if the item is not included in the array.
  // Delegates to **ECMAScript 5**'s native `indexOf` if available.
  // If the array is large and already in sort order, pass `true`
  // for **isSorted** to use binary search.
  _.indexOf = function(array, item, isSorted) {
    if (array == null) return -1;
    var i = 0, l = array.length;
    if (isSorted) {
      if (typeof isSorted == 'number') {
        i = (isSorted < 0 ? Math.max(0, l + isSorted) : isSorted);
      } else {
        i = _.sortedIndex(array, item);
        return array[i] === item ? i : -1;
      }
    }
    if (nativeIndexOf && array.indexOf === nativeIndexOf) return array.indexOf(item, isSorted);
    for (; i < l; i++) if (array[i] === item) return i;
    return -1;
  };

  // Delegates to **ECMAScript 5**'s native `lastIndexOf` if available.
  _.lastIndexOf = function(array, item, from) {
    if (array == null) return -1;
    var hasIndex = from != null;
    if (nativeLastIndexOf && array.lastIndexOf === nativeLastIndexOf) {
      return hasIndex ? array.lastIndexOf(item, from) : array.lastIndexOf(item);
    }
    var i = (hasIndex ? from : array.length);
    while (i--) if (array[i] === item) return i;
    return -1;
  };

  // Generate an integer Array containing an arithmetic progression. A port of
  // the native Python `range()` function. See
  // [the Python documentation](http://docs.python.org/library/functions.html#range).
  _.range = function(start, stop, step) {
    if (arguments.length <= 1) {
      stop = start || 0;
      start = 0;
    }
    step = arguments[2] || 1;

    var len = Math.max(Math.ceil((stop - start) / step), 0);
    var idx = 0;
    var range = new Array(len);

    while(idx < len) {
      range[idx++] = start;
      start += step;
    }

    return range;
  };

  // Function (ahem) Functions
  // ------------------

  // Reusable constructor function for prototype setting.
  var ctor = function(){};

  // Create a function bound to a given object (assigning `this`, and arguments,
  // optionally). Binding with arguments is also known as `curry`.
  // Delegates to **ECMAScript 5**'s native `Function.bind` if available.
  // We check for `func.bind` first, to fail fast when `func` is undefined.
  _.bind = function(func, context) {
    var args, bound;
    if (func.bind === nativeBind && nativeBind) return nativeBind.apply(func, slice.call(arguments, 1));
    if (!_.isFunction(func)) throw new TypeError;
    args = slice.call(arguments, 2);
    return bound = function() {
      if (!(this instanceof bound)) return func.apply(context, args.concat(slice.call(arguments)));
      ctor.prototype = func.prototype;
      var self = new ctor;
      ctor.prototype = null;
      var result = func.apply(self, args.concat(slice.call(arguments)));
      if (Object(result) === result) return result;
      return self;
    };
  };

  // Bind all of an object's methods to that object. Useful for ensuring that
  // all callbacks defined on an object belong to it.
  _.bindAll = function(obj) {
    var funcs = slice.call(arguments, 1);
    if (funcs.length == 0) funcs = _.functions(obj);
    each(funcs, function(f) { obj[f] = _.bind(obj[f], obj); });
    return obj;
  };

  // Memoize an expensive function by storing its results.
  _.memoize = function(func, hasher) {
    var memo = {};
    hasher || (hasher = _.identity);
    return function() {
      var key = hasher.apply(this, arguments);
      return _.has(memo, key) ? memo[key] : (memo[key] = func.apply(this, arguments));
    };
  };

  // Delays a function for the given number of milliseconds, and then calls
  // it with the arguments supplied.
  _.delay = function(func, wait) {
    var args = slice.call(arguments, 2);
    return setTimeout(function(){ return func.apply(null, args); }, wait);
  };

  // Defers a function, scheduling it to run after the current call stack has
  // cleared.
  _.defer = function(func) {
    return _.delay.apply(_, [func, 1].concat(slice.call(arguments, 1)));
  };

  // Returns a function, that, when invoked, will only be triggered at most once
  // during a given window of time.
  _.throttle = function(func, wait) {
    var context, args, timeout, result;
    var previous = 0;
    var later = function() {
      previous = new Date;
      timeout = null;
      result = func.apply(context, args);
    };
    return function() {
      var now = new Date;
      var remaining = wait - (now - previous);
      context = this;
      args = arguments;
      if (remaining <= 0) {
        clearTimeout(timeout);
        timeout = null;
        previous = now;
        result = func.apply(context, args);
      } else if (!timeout) {
        timeout = setTimeout(later, remaining);
      }
      return result;
    };
  };

  // Returns a function, that, as long as it continues to be invoked, will not
  // be triggered. The function will be called after it stops being called for
  // N milliseconds. If `immediate` is passed, trigger the function on the
  // leading edge, instead of the trailing.
  _.debounce = function(func, wait, immediate) {
    var timeout, result;
    return function() {
      var context = this, args = arguments;
      var later = function() {
        timeout = null;
        if (!immediate) result = func.apply(context, args);
      };
      var callNow = immediate && !timeout;
      clearTimeout(timeout);
      timeout = setTimeout(later, wait);
      if (callNow) result = func.apply(context, args);
      return result;
    };
  };

  // Returns a function that will be executed at most one time, no matter how
  // often you call it. Useful for lazy initialization.
  _.once = function(func) {
    var ran = false, memo;
    return function() {
      if (ran) return memo;
      ran = true;
      memo = func.apply(this, arguments);
      func = null;
      return memo;
    };
  };

  // Returns the first function passed as an argument to the second,
  // allowing you to adjust arguments, run code before and after, and
  // conditionally execute the original function.
  _.wrap = function(func, wrapper) {
    return function() {
      var args = [func];
      push.apply(args, arguments);
      return wrapper.apply(this, args);
    };
  };

  // Returns a function that is the composition of a list of functions, each
  // consuming the return value of the function that follows.
  _.compose = function() {
    var funcs = arguments;
    return function() {
      var args = arguments;
      for (var i = funcs.length - 1; i >= 0; i--) {
        args = [funcs[i].apply(this, args)];
      }
      return args[0];
    };
  };

  // Returns a function that will only be executed after being called N times.
  _.after = function(times, func) {
    if (times <= 0) return func();
    return function() {
      if (--times < 1) {
        return func.apply(this, arguments);
      }
    };
  };

  // Object Functions
  // ----------------

  // Retrieve the names of an object's properties.
  // Delegates to **ECMAScript 5**'s native `Object.keys`
  _.keys = nativeKeys || function(obj) {
    if (obj !== Object(obj)) throw new TypeError('Invalid object');
    var keys = [];
    for (var key in obj) if (_.has(obj, key)) keys[keys.length] = key;
    return keys;
  };

  // Retrieve the values of an object's properties.
  _.values = function(obj) {
    var values = [];
    for (var key in obj) if (_.has(obj, key)) values.push(obj[key]);
    return values;
  };

  // Convert an object into a list of `[key, value]` pairs.
  _.pairs = function(obj) {
    var pairs = [];
    for (var key in obj) if (_.has(obj, key)) pairs.push([key, obj[key]]);
    return pairs;
  };

  // Invert the keys and values of an object. The values must be serializable.
  _.invert = function(obj) {
    var result = {};
    for (var key in obj) if (_.has(obj, key)) result[obj[key]] = key;
    return result;
  };

  // Return a sorted list of the function names available on the object.
  // Aliased as `methods`
  _.functions = _.methods = function(obj) {
    var names = [];
    for (var key in obj) {
      if (_.isFunction(obj[key])) names.push(key);
    }
    return names.sort();
  };

  // Extend a given object with all the properties in passed-in object(s).
  _.extend = function(obj) {
    each(slice.call(arguments, 1), function(source) {
      if (source) {
        for (var prop in source) {
          obj[prop] = source[prop];
        }
      }
    });
    return obj;
  };

  // Return a copy of the object only containing the whitelisted properties.
  _.pick = function(obj) {
    var copy = {};
    var keys = concat.apply(ArrayProto, slice.call(arguments, 1));
    each(keys, function(key) {
      if (key in obj) copy[key] = obj[key];
    });
    return copy;
  };

   // Return a copy of the object without the blacklisted properties.
  _.omit = function(obj) {
    var copy = {};
    var keys = concat.apply(ArrayProto, slice.call(arguments, 1));
    for (var key in obj) {
      if (!_.contains(keys, key)) copy[key] = obj[key];
    }
    return copy;
  };

  // Fill in a given object with default properties.
  _.defaults = function(obj) {
    each(slice.call(arguments, 1), function(source) {
      if (source) {
        for (var prop in source) {
          if (obj[prop] == null) obj[prop] = source[prop];
        }
      }
    });
    return obj;
  };

  // Create a (shallow-cloned) duplicate of an object.
  _.clone = function(obj) {
    if (!_.isObject(obj)) return obj;
    return _.isArray(obj) ? obj.slice() : _.extend({}, obj);
  };

  // Invokes interceptor with the obj, and then returns obj.
  // The primary purpose of this method is to "tap into" a method chain, in
  // order to perform operations on intermediate results within the chain.
  _.tap = function(obj, interceptor) {
    interceptor(obj);
    return obj;
  };

  // Internal recursive comparison function for `isEqual`.
  var eq = function(a, b, aStack, bStack) {
    // Identical objects are equal. `0 === -0`, but they aren't identical.
    // See the Harmony `egal` proposal: http://wiki.ecmascript.org/doku.php?id=harmony:egal.
    if (a === b) return a !== 0 || 1 / a == 1 / b;
    // A strict comparison is necessary because `null == undefined`.
    if (a == null || b == null) return a === b;
    // Unwrap any wrapped objects.
    if (a instanceof _) a = a._wrapped;
    if (b instanceof _) b = b._wrapped;
    // Compare `[[Class]]` names.
    var className = toString.call(a);
    if (className != toString.call(b)) return false;
    switch (className) {
      // Strings, numbers, dates, and booleans are compared by value.
      case '[object String]':
        // Primitives and their corresponding object wrappers are equivalent; thus, `"5"` is
        // equivalent to `new String("5")`.
        return a == String(b);
      case '[object Number]':
        // `NaN`s are equivalent, but non-reflexive. An `egal` comparison is performed for
        // other numeric values.
        return a != +a ? b != +b : (a == 0 ? 1 / a == 1 / b : a == +b);
      case '[object Date]':
      case '[object Boolean]':
        // Coerce dates and booleans to numeric primitive values. Dates are compared by their
        // millisecond representations. Note that invalid dates with millisecond representations
        // of `NaN` are not equivalent.
        return +a == +b;
      // RegExps are compared by their source patterns and flags.
      case '[object RegExp]':
        return a.source == b.source &&
               a.global == b.global &&
               a.multiline == b.multiline &&
               a.ignoreCase == b.ignoreCase;
    }
    if (typeof a != 'object' || typeof b != 'object') return false;
    // Assume equality for cyclic structures. The algorithm for detecting cyclic
    // structures is adapted from ES 5.1 section 15.12.3, abstract operation `JO`.
    var length = aStack.length;
    while (length--) {
      // Linear search. Performance is inversely proportional to the number of
      // unique nested structures.
      if (aStack[length] == a) return bStack[length] == b;
    }
    // Add the first object to the stack of traversed objects.
    aStack.push(a);
    bStack.push(b);
    var size = 0, result = true;
    // Recursively compare objects and arrays.
    if (className == '[object Array]') {
      // Compare array lengths to determine if a deep comparison is necessary.
      size = a.length;
      result = size == b.length;
      if (result) {
        // Deep compare the contents, ignoring non-numeric properties.
        while (size--) {
          if (!(result = eq(a[size], b[size], aStack, bStack))) break;
        }
      }
    } else {
      // Objects with different constructors are not equivalent, but `Object`s
      // from different frames are.
      var aCtor = a.constructor, bCtor = b.constructor;
      if (aCtor !== bCtor && !(_.isFunction(aCtor) && (aCtor instanceof aCtor) &&
                               _.isFunction(bCtor) && (bCtor instanceof bCtor))) {
        return false;
      }
      // Deep compare objects.
      for (var key in a) {
        if (_.has(a, key)) {
          // Count the expected number of properties.
          size++;
          // Deep compare each member.
          if (!(result = _.has(b, key) && eq(a[key], b[key], aStack, bStack))) break;
        }
      }
      // Ensure that both objects contain the same number of properties.
      if (result) {
        for (key in b) {
          if (_.has(b, key) && !(size--)) break;
        }
        result = !size;
      }
    }
    // Remove the first object from the stack of traversed objects.
    aStack.pop();
    bStack.pop();
    return result;
  };

  // Perform a deep comparison to check if two objects are equal.
  _.isEqual = function(a, b) {
    return eq(a, b, [], []);
  };

  // Is a given array, string, or object empty?
  // An "empty" object has no enumerable own-properties.
  _.isEmpty = function(obj) {
    if (obj == null) return true;
    if (_.isArray(obj) || _.isString(obj)) return obj.length === 0;
    for (var key in obj) if (_.has(obj, key)) return false;
    return true;
  };

  // Is a given value a DOM element?
  _.isElement = function(obj) {
    return !!(obj && obj.nodeType === 1);
  };

  // Is a given value an array?
  // Delegates to ECMA5's native Array.isArray
  _.isArray = nativeIsArray || function(obj) {
    return toString.call(obj) == '[object Array]';
  };

  // Is a given variable an object?
  _.isObject = function(obj) {
    return obj === Object(obj);
  };

  // Add some isType methods: isArguments, isFunction, isString, isNumber, isDate, isRegExp.
  each(['Arguments', 'Function', 'String', 'Number', 'Date', 'RegExp'], function(name) {
    _['is' + name] = function(obj) {
      return toString.call(obj) == '[object ' + name + ']';
    };
  });

  // Define a fallback version of the method in browsers (ahem, IE), where
  // there isn't any inspectable "Arguments" type.
  if (!_.isArguments(arguments)) {
    _.isArguments = function(obj) {
      return !!(obj && _.has(obj, 'callee'));
    };
  }

  // Optimize `isFunction` if appropriate.
  if (typeof (/./) !== 'function') {
    _.isFunction = function(obj) {
      return typeof obj === 'function';
    };
  }

  // Is a given object a finite number?
  _.isFinite = function(obj) {
    return isFinite(obj) && !isNaN(parseFloat(obj));
  };

  // Is the given value `NaN`? (NaN is the only number which does not equal itself).
  _.isNaN = function(obj) {
    return _.isNumber(obj) && obj != +obj;
  };

  // Is a given value a boolean?
  _.isBoolean = function(obj) {
    return obj === true || obj === false || toString.call(obj) == '[object Boolean]';
  };

  // Is a given value equal to null?
  _.isNull = function(obj) {
    return obj === null;
  };

  // Is a given variable undefined?
  _.isUndefined = function(obj) {
    return obj === void 0;
  };

  // Shortcut function for checking if an object has a given property directly
  // on itself (in other words, not on a prototype).
  _.has = function(obj, key) {
    return hasOwnProperty.call(obj, key);
  };

  // Utility Functions
  // -----------------

  // Run Underscore.js in *noConflict* mode, returning the `_` variable to its
  // previous owner. Returns a reference to the Underscore object.
  _.noConflict = function() {
    root._ = previousUnderscore;
    return this;
  };

  // Keep the identity function around for default iterators.
  _.identity = function(value) {
    return value;
  };

  // Run a function **n** times.
  _.times = function(n, iterator, context) {
    var accum = Array(n);
    for (var i = 0; i < n; i++) accum[i] = iterator.call(context, i);
    return accum;
  };

  // Return a random integer between min and max (inclusive).
  _.random = function(min, max) {
    if (max == null) {
      max = min;
      min = 0;
    }
    return min + (0 | Math.random() * (max - min + 1));
  };

  // List of HTML entities for escaping.
  var entityMap = {
    escape: {
      '&': '&amp;',
      '<': '&lt;',
      '>': '&gt;',
      '"': '&quot;',
      "'": '&#x27;',
      '/': '&#x2F;'
    }
  };
  entityMap.unescape = _.invert(entityMap.escape);

  // Regexes containing the keys and values listed immediately above.
  var entityRegexes = {
    escape:   new RegExp('[' + _.keys(entityMap.escape).join('') + ']', 'g'),
    unescape: new RegExp('(' + _.keys(entityMap.unescape).join('|') + ')', 'g')
  };

  // Functions for escaping and unescaping strings to/from HTML interpolation.
  _.each(['escape', 'unescape'], function(method) {
    _[method] = function(string) {
      if (string == null) return '';
      return ('' + string).replace(entityRegexes[method], function(match) {
        return entityMap[method][match];
      });
    };
  });

  // If the value of the named property is a function then invoke it;
  // otherwise, return it.
  _.result = function(object, property) {
    if (object == null) return null;
    var value = object[property];
    return _.isFunction(value) ? value.call(object) : value;
  };

  // Add your own custom functions to the Underscore object.
  _.mixin = function(obj) {
    each(_.functions(obj), function(name){
      var func = _[name] = obj[name];
      _.prototype[name] = function() {
        var args = [this._wrapped];
        push.apply(args, arguments);
        return result.call(this, func.apply(_, args));
      };
    });
  };

  // Generate a unique integer id (unique within the entire client session).
  // Useful for temporary DOM ids.
  var idCounter = 0;
  _.uniqueId = function(prefix) {
    var id = '' + ++idCounter;
    return prefix ? prefix + id : id;
  };

  // By default, Underscore uses ERB-style template delimiters, change the
  // following template settings to use alternative delimiters.
  _.templateSettings = {
    evaluate    : /<%([\s\S]+?)%>/g,
    interpolate : /<%=([\s\S]+?)%>/g,
    escape      : /<%-([\s\S]+?)%>/g
  };

  // When customizing `templateSettings`, if you don't want to define an
  // interpolation, evaluation or escaping regex, we need one that is
  // guaranteed not to match.
  var noMatch = /(.)^/;

  // Certain characters need to be escaped so that they can be put into a
  // string literal.
  var escapes = {
    "'":      "'",
    '\\':     '\\',
    '\r':     'r',
    '\n':     'n',
    '\t':     't',
    '\u2028': 'u2028',
    '\u2029': 'u2029'
  };

  var escaper = /\\|'|\r|\n|\t|\u2028|\u2029/g;

  // JavaScript micro-templating, similar to John Resig's implementation.
  // Underscore templating handles arbitrary delimiters, preserves whitespace,
  // and correctly escapes quotes within interpolated code.
  _.template = function(text, data, settings) {
    settings = _.defaults({}, settings, _.templateSettings);

    // Combine delimiters into one regular expression via alternation.
    var matcher = new RegExp([
      (settings.escape || noMatch).source,
      (settings.interpolate || noMatch).source,
      (settings.evaluate || noMatch).source
    ].join('|') + '|$', 'g');

    // Compile the template source, escaping string literals appropriately.
    var index = 0;
    var source = "__p+='";
    text.replace(matcher, function(match, escape, interpolate, evaluate, offset) {
      source += text.slice(index, offset)
        .replace(escaper, function(match) { return '\\' + escapes[match]; });

      if (escape) {
        source += "'+\n((__t=(" + escape + "))==null?'':_.escape(__t))+\n'";
      }
      if (interpolate) {
        source += "'+\n((__t=(" + interpolate + "))==null?'':__t)+\n'";
      }
      if (evaluate) {
        source += "';\n" + evaluate + "\n__p+='";
      }
      index = offset + match.length;
      return match;
    });
    source += "';\n";

    // If a variable is not specified, place data values in local scope.
    if (!settings.variable) source = 'with(obj||{}){\n' + source + '}\n';

    source = "var __t,__p='',__j=Array.prototype.join," +
      "print=function(){__p+=__j.call(arguments,'');};\n" +
      source + "return __p;\n";

    try {
      var render = new Function(settings.variable || 'obj', '_', source);
    } catch (e) {
      e.source = source;
      throw e;
    }

    if (data) return render(data, _);
    var template = function(data) {
      return render.call(this, data, _);
    };

    // Provide the compiled function source as a convenience for precompilation.
    template.source = 'function(' + (settings.variable || 'obj') + '){\n' + source + '}';

    return template;
  };

  // Add a "chain" function, which will delegate to the wrapper.
  _.chain = function(obj) {
    return _(obj).chain();
  };

  // OOP
  // ---------------
  // If Underscore is called as a function, it returns a wrapped object that
  // can be used OO-style. This wrapper holds altered versions of all the
  // underscore functions. Wrapped objects may be chained.

  // Helper function to continue chaining intermediate results.
  var result = function(obj) {
    return this._chain ? _(obj).chain() : obj;
  };

  // Add all of the Underscore functions to the wrapper object.
  _.mixin(_);

  // Add all mutator Array functions to the wrapper.
  each(['pop', 'push', 'reverse', 'shift', 'sort', 'splice', 'unshift'], function(name) {
    var method = ArrayProto[name];
    _.prototype[name] = function() {
      var obj = this._wrapped;
      method.apply(obj, arguments);
      if ((name == 'shift' || name == 'splice') && obj.length === 0) delete obj[0];
      return result.call(this, obj);
    };
  });

  // Add all accessor Array functions to the wrapper.
  each(['concat', 'join', 'slice'], function(name) {
    var method = ArrayProto[name];
    _.prototype[name] = function() {
      return result.call(this, method.apply(this._wrapped, arguments));
    };
  });

  _.extend(_.prototype, {

    // Start chaining a wrapped Underscore object.
    chain: function() {
      this._chain = true;
      return this;
    },

    // Extracts the result from a wrapped and chained object.
    value: function() {
      return this._wrapped;
    }

  });

}).call(this);
var ccb_resources = [
    {type:'plist', src:"image/var/skill/skill15.plist"},
    {type:'image', src:"image/framework/control/dialog_head.png"},
    {type:'image', src:"image/var/skill/attackEffect04.png"},
    {type:'image', src:"image/var/skill/zhuanSheng/zhuanSheng_buff_def.png"},
    {type:'ccbi', src:"image/interface/AttackSkill01_To.ccbi"},
    {type:'ccbi', src:"image/interface/FightLevel.ccbi"},
    {type:'image', src:"image/var/skill/skillWeapon/bishou.png"},
    {type:'image', src:"image/framework/control/footer_icon_zhanYi.png"},
    {type:'image', src:"image/var/skill/zhuanSheng/zhuanSheng_debuff_int.png"},
    {type:'plist', src:"image/var/skill/attackEffect03.plist"},
    {type:'ccbi', src:"image/interface/FightHeroTalk.ccbi"},
    {type:'plist', src:"image/var/skill/skill12.plist"},
    {type:'image', src:"image/framework/word/v-7.png"},
    {type:'ccbi', src:"image/interface/AttackSkill17.ccbi"},
    {type:'image', src:"image/var/background/fp_function.png"},
    {type:'image', src:"image/framework/word/fp_palace_bar.png"},
    {type:'image', src:"image/framework/control/line.png"},
    {type:'image', src:"image/framework/icon/icon_arrow_long.png"},
    {type:'ccbi', src:"image/interface/CDialog.ccbi"},
    {type:'ccbi', src:"image/interface/PageTeam.ccbi"},
    {type:'ccbi', src:"image/interface/AttackSkill05.ccbi"},
    {type:'plist', src:"image/var/skill/attackSkill02.plist"},
    {type:'ccbi', src:"image/interface/PageChallenge.ccbi"},
    {type:'image', src:"image/framework/control/level_small.png"},
    {type:'image', src:"image/var/NewPlayMovie/txt2.png"},
    {type:'plist', src:"image/var/skill/attackSkill03.plist"},
    {type:'image', src:"image/framework/control/card_level_blue.png"},
    {type:'plist', src:"image/var/skill/skill21.plist"},
    {type:'image', src:"image/framework/control/button_normal_decrease.png"},
    {type:'image', src:"image/framework/control/flag_dao.png"},
    {type:'image', src:"image/framework/control/zhanDou_shengLiKuang.png"},
    {type:'image', src:"image/framework/control/head_box.png"},
    {type:'image', src:"image/framework/control/button_map_selected.png"},
    {type:'image', src:"image/var/background/fp_bag_press.png"},
    {type:'image', src:"image/var/skill/skillName/zhenFa_bg.png"},
    {type:'image', src:"image/var/skill/skillWeapon/dao.png"},
    {type:'plist', src:"image/var/skill/attackSkill07.plist"},
    {type:'image', src:"image/framework/icon/neiwu_friend.png"},
    {type:'image', src:"image/framework/control/grcolor_line_02.png"},
    {type:'image', src:"image/var/background/KingLeveUp_zhujiemian.png"},
    {type:'image', src:"image/framework/word/fp_strategy_bar.png"},
    {type:'image', src:"image/var/skill/skillName/zhenFa_baiHu2.png"},
    {type:'image', src:"image/ccbResources/fight_blood_bg.png"},
    {type:'image', src:"image/var/background/fp_hero.png"},
    {type:'image', src:"image/var/skill/skill21.png"},
    {type:'image', src:"image/var/NewPlayMovie/txt5.png"},
    {type:'ccbi', src:"image/interface/BroadCastBar.ccbi"},
    {type:'ccbi', src:"image/interface/Loader.ccbi"},
    {type:'image', src:"image/framework/control/label_killed.png"},
    {type:'image', src:"image/framework/control/zhanDou_shiBaiKuang.png"},
    {type:'image', src:"image/framework/word/binFa_shiBai.png"},
    {type:'image', src:"image/var/background/fp_palace.png"},
    {type:'image', src:"image/framework/control/checkbox_selected.png"},
    {type:'image', src:"image/framework/icon/icon_xp.png"},
    {type:'sound', src:"music/effect_level_up.mp3"},
    {type:'image', src:"image/framework/control/bg_turn_right.png"},
    {type:'image', src:"image/framework/control/fight_gray_bg.png"},
    {type:'ccbi', src:"image/interface/AttackSkill23.ccbi"},
    {type:'image', src:"image/var/skill/skill23.png"},
    {type:'plist', src:"image/var/skill/skill03.plist"},
    {type:'image', src:"image/framework/control/flag_qun.png"},
    {type:'ccbi', src:"image/interface/AttackSkill11.ccbi"},
    {type:'image', src:"image/var/skill/skillWeapon/mao.png"},
    {type:'sound', src:"music/bg_tower.mp3"},
    {type:'plist', src:"image/var/skill/KingLeveUp_donghua.plist"},
    {type:'image', src:"image/var/background/bg_card_01.png"},
    {type:'plist', src:"image/var/skill/attackEffect02.plist"},
    {type:'image', src:"image/var/skill/skillName/dijihuihe.png"},
    {type:'sound', src:"music/effect_armor.mp3"},
    {type:'image', src:"image/framework/control/exp_bg_new.png"},
    {type:'image', src:"image/framework/control/head_box_white.png"},
    {type:'image', src:"image/framework/icon/icon_vm_big.png"},
    {type:'image', src:"image/framework/icon/icon_rmb.png"},
    {type:'image', src:"image/var/skill/skill25.png"},
    {type:'ccbi', src:"image/interface/newplayer3.ccbi"},
    {type:'image', src:"image/framework/word/num2.png"},
    {type:'image', src:"image/framework/icon/icon_arrow_evolve.png"},
    {type:'image', src:"image/framework/control/login_hot.png"},
    {type:'image', src:"image/framework/control/head_bg.png"},
    {type:'image', src:"image/framework/control/time_part_bg.png"},
    {type:'image', src:"image/framework/control/footer_icon_leiTai.png"},
    {type:'image', src:"image/framework/control/battleSkip.png"},
    {type:'image', src:"image/framework/word/qiangHua_dengJJ_tips.png"},
    {type:'image', src:"image/framework/control/head_box_red.png"},
    {type:'ccbi', src:"image/interface/CLoading.ccbi"},
    {type:'image', src:"image/framework/control/tiny_narrow_header_bg.png"},
    {type:'image', src:"image/framework/control/flag_dun.png"},
    {type:'ccbi', src:"image/interface/FindHero.ccbi"},
    {type:'image', src:"image/var/background/fp_tech.png"},
    {type:'image', src:"image/framework/icon/icon_command.png"},
    {type:'image', src:"image/var/skill/skill11.png"},
    {type:'image', src:"image/framework/control/login_recent.png"},
    {type:'image', src:"image/newplayer/bg_scene1.png"},
    {type:'image', src:"image/framework/control/head_box_yellow.png"},
    {type:'image', src:"image/framework/word/num8.png"},
    {type:'plist', src:"image/var/skill/skill17.plist"},
    {type:'image', src:"image/framework/control/footer_icon_shangCheng.png"},
    {type:'image', src:"image/framework/control/login_nameBoard.png"},
    {type:'ccbi', src:"image/interface/AttackSkill16.ccbi"},
    {type:'sound', src:"music/effect_do.mp3"},
    {type:'image', src:"image/ccbResources/green_btn.png"},
    {type:'ccbi', src:"image/interface/AttackSkill04.ccbi"},
    {type:'image', src:"image/framework/control/radiobox_disable.png"},
    {type:'image', src:"image/framework/icon/icon_player.png"},
    {type:'ccbi', src:"image/interface/FooterMenu.ccbi"},
    {type:'image', src:"image/var/background/loginLoading2.png"},
    {type:'fnt', src:"image/framework/word/FightNumber.fnt"},
    {type:'image', src:"image/framework/control/tab_unselected.png"},
    {type:'image', src:"image/ccbResources/herobox.png"},
    {type:'image', src:"image/framework/icon/icon_attack.png"},
    {type:'image', src:"image/framework/word/zhanDou_daSheng.png"},
    {type:'image', src:"image/var/skill/skill13.png"},
    {type:'image', src:"image/ccbResources/Default.png"},
    {type:'sound', src:"music/effect_skill.mp3"},
    {type:'image', src:"image/var/skill/zhuanSheng/zhuanSheng_buff_att.png"},
    {type:'image', src:"image/framework/word/fp_function_bar.png"},
    {type:'image', src:"image/framework/icon/icon_defence.png"},
    {type:'plist', src:"image/var/skill/skill23.plist"},
    {type:'ccbi', src:"image/interface/AttackSkillWeapon.ccbi"},
    {type:'sound', src:"music/effect_slotmachine.mp3"},
    {type:'image', src:"image/framework/word/FightNumberLvSe.png"},
    {type:'image', src:"image/var/skill/skillWeapon/bian.png"},
    {type:'ccbi', src:"image/interface/PageShop.ccbi"},
    {type:'image', src:"image/framework/word/num5.png"},
    {type:'image', src:"image/var/skill/skill15.png"},
    {type:'image', src:"image/var/skill/Logo.png"},
    {type:'image', src:"image/framework/icon/icon_hero.png"},
    {type:'image', src:"image/var/skill/zhaojiang/ren.png"},
    {type:'ccbi', src:"image/interface/HeroBlood.ccbi"},
    {type:'plist', src:"image/var/skill/skill20.plist"},
    {type:'image', src:"image/framework/word/v-0.png"},
    {type:'ccbi', src:"image/interface/AttackSkill09.ccbi"},
    {type:'image', src:"image/var/background/bg_card_02.png"},
    {type:'image', src:"image/framework/control/notice.png"},
    {type:'image', src:"image/framework/icon/red_point.png"},
    {type:'image', src:"image/var/skill/zhuanSheng/zhuanSheng_debuff_def.png"},
    {type:'image', src:"image/framework/control/button_normal_large_transparent.png"},
    {type:'image', src:"image/var/skill/skill01.png"},
    {type:'image', src:"image/framework/control/chakan_wofang.png"},
    {type:'image', src:"image/framework/control/button_unselected_left.png"},
    {type:'image', src:"image/var/skill/skill17.png"},
    {type:'image', src:"image/var/Loading.png"},
    {type:'image', src:"image/framework/word/zhanDou_xiaoBai.png"},
    {type:'image', src:"image/framework/control/checkbox_disable.png"},
    {type:'image', src:"image/framework/word/v-3.png"},
    {type:'image', src:"image/framework/control/star_bg.png"},
    {type:'image', src:"image/framework/control/head_box_purple.png"},
    {type:'plist', src:"image/var/skill/attackEffect01.plist"},
    {type:'sound', src:"music/effect_female_voice.mp3"},
    {type:'image', src:"image/framework/word/0-9.png"},
    {type:'image', src:"image/framework/control/button_normal_decrease_dianji.png"},
    {type:'ccbi', src:"image/interface/AttackSkill22.ccbi"},
    {type:'image', src:"image/framework/icon/icon_arrow_small_right.png"},
    {type:'image', src:"image/framework/control/dialog_btn_close_press.png"},
    {type:'fnt', src:"image/framework/word/FightNumberLvSe.fnt"},
    {type:'plist', src:"image/var/skill/skill08.plist"},
    {type:'image', src:"image/framework/control/flag_wei.png"},
    {type:'image', src:"image/framework/control/head_light.png"},
    {type:'image', src:"image/var/skill/skill03.png"},
    {type:'ccbi', src:"image/interface/AttackSkill10.ccbi"},
    {type:'image', src:"image/framework/word/v-6.png"},
    {type:'image', src:"image/framework/control/battleSkip_dianji.png"},
    {type:'image', src:"image/var/skill/skill19.png"},
    {type:'ccbi', src:"image/interface/AttackEffect04.ccbi"},
    {type:'image', src:"image/var/background/task_bg_1.png"},
    {type:'ccbi', src:"image/interface/PageInstanceZone.ccbi"},
    {type:'image', src:"image/var/background/fp_strategy_press.png"},
    {type:'plist', src:"image/var/skill/skill05.plist"},
    {type:'image', src:"image/framework/control/footer_icon_zhenRong.png"},
    {type:'image', src:"image/framework/word/zhanDou_wanSheng.png"},
    {type:'image', src:"image/framework/control/bg_turn_left.png"},
    {type:'image', src:"image/var/NewPlayMovie/txt4.png"},
    {type:'image', src:"image/framework/word/v-9.png"},
    {type:'image', src:"image/var/skill/skill05.png"},
    {type:'image', src:"image/framework/control/chongzhi_dianji.png"},
    {type:'plist', src:"image/var/skill/skill02.plist"},
    {type:'plist', src:"image/var/skill/skill14.plist"},
    {type:'ccbi', src:"image/interface/AttackSkillName.ccbi"},
    {type:'ccbi', src:"image/interface/FooterBar.ccbi"},
    {type:'fnt', src:"image/framework/word/0-9.fnt"},
    {type:'image', src:"image/framework/control/dialog_border.png"},
    {type:'ccbi', src:"image/interface/AttackSkill15.ccbi"},
    {type:'image', src:"image/framework/control/fight_blood_bg.png"},
    {type:'image', src:"image/var/skill/skill07.png"},
    {type:'ccbi', src:"image/interface/AttackSkill03.ccbi"},
    {type:'plist', src:"image/var/skill/skill11.plist"},
    {type:'ccbi', src:"image/interface/AttackRound.ccbi"},
    {type:'image', src:"image/ccbResources/500001.png"},
    {type:'image', src:"image/var/background/bg_card_03.png"},
    {type:'image', src:"image/ccbResources/fight_blood_red.png"},
    {type:'image', src:"image/framework/icon/icon_ep_big.png"},
    {type:'image', src:"image/var/background/bg_shop_1.png"},
    {type:'sound', src:"music/effect_new_card.mp3"},
    {type:'image', src:"image/framework/control/teshu_button_dianji.png"},
    {type:'image', src:"image/framework/control/wuJiang_shangZhen.png"},
    {type:'ccbi', src:"image/interface/KingLeveUp_guang.ccbi"},
    {type:'image', src:"image/framework/icon/icon_rmb_big.png"},
    {type:'image', src:"image/var/background/fp_weapon.png"},
    {type:'image', src:"image/framework/word/ward_form_add.png"},
    {type:'image', src:"image/framework/control/tips.png"},
    {type:'image', src:"image/framework/control/button_unselected_right.png"},
    {type:'image', src:"image/var/skill/skill09.png"},
    {type:'fnt', src:"image/framework/word/LevelNumber.fnt"},
    {type:'image', src:"image/framework/word/shangcheng_shouchong.png"},
    {type:'image', src:"image/var/background/fp_weapon_press.png"},
    {type:'ccbi', src:"image/interface/PageFightActiveZone.ccbi"},
    {type:'image', src:"image/var/NewPlayMovie/txt1.png"},
    {type:'image', src:"image/framework/control/flag_shu.png"},
    {type:'image', src:"image/var/skill/attackEffect01.png"},
    {type:'image', src:"image/framework/icon/icon_add_chara.png"},
    {type:'image', src:"image/framework/icon/neiwu_set.png"},
    {type:'image', src:"image/framework/control/shaoDang_duShuTiao.png"},
    {type:'image', src:"image/var/background/bg_top_01.png"},
    {type:'image', src:"image/framework/word/num0.png"},
    {type:'image', src:"image/var/skill/zhaojiang/guangquan.png"},
    {type:'image', src:"image/framework/control/button_normal_increase.png"},
    {type:'plist', src:"image/var/Loading.plist"},
    {type:'image', src:"image/var/NewPlayMovie/bg_text.png"},
    {type:'image', src:"image/var/active/endlessWar/ttt_bg1.png"},
    {type:'image', src:"image/framework/control/button_map_disable.png"},
    {type:'image', src:"image/framework/control/sell.png"},
    {type:'image', src:"image/framework/icon/icon_ep.png"},
    {type:'image', src:"image/framework/control/fight_blood_blue.png"},
    {type:'image', src:"image/var/skill/skillIcon/buff_def.png"},
    {type:'sound', src:"music/bg_battle1.mp3"},
    {type:'ccbi', src:"image/interface/AttackSkill08.ccbi"},
    {type:'image', src:"image/framework/control/dot_line.png"},
    {type:'image', src:"image/framework/control/bg_turn_cloud.png"},
    {type:'image', src:"image/var/skill/zhuanSheng/zhuanSheng_debuff_att.png"},
    {type:'image', src:"image/ccbResources/label_killed.png"},
    {type:'ccbi', src:"image/interface/KingLeveUp_dengji.ccbi"},
    {type:'sound', src:"music/bg_main.mp3"},
    {type:'ccbi', src:"image/interface/AttackEffect03.ccbi"},
    {type:'image', src:"image/framework/control/chuShou_tiaoKuang.png"},
    {type:'image', src:"image/framework/control/exp_line_new.png"},
    {type:'image', src:"image/framework/control/zhanDou_wuJiangMing.png"},
    {type:'image', src:"image/framework/control/zhanDou_suo.png"},
    {type:'image', src:"image/var/NewPlayMovie/zhuiji.png"},
    {type:'image', src:"image/framework/control/checkbox_normal.png"},
    {type:'image', src:"image/ccbResources/310018.png"},
    {type:'plist', src:"image/var/skill/Logo.plist"},
    {type:'sound', src:"music/effect_mount.mp3"},
    {type:'image', src:"image/var/skill/skillIcon/buff_int.png"},
    {type:'ccbi', src:"image/interface/AttackSkill21.ccbi"},
    {type:'image', src:"image/framework/control/login_new.png"},
    {type:'image', src:"image/framework/icon/neiwu_pic.png"},
    {type:'image', src:"image/var/background/fp_strategy.png"},
    {type:'image', src:"image/var/NewPlayMovie/fabang.png"},
    {type:'ccbi', src:"image/interface/KingLeveUp.ccbi"},
    {type:'image', src:"image/framework/control/star.png"},
    {type:'image', src:"image/framework/control/tab_selected.png"},
    {type:'image', src:"image/var/background/task_bg_2.png"},
    {type:'image', src:"image/framework/control/login_line.png"},
    {type:'image', src:"image/framework/word/num3.png"},
    {type:'image', src:"image/framework/control/btn_refresh_player.png"},
    {type:'image', src:"image/framework/control/button_normal_left.png"},
    {type:'image', src:"image/framework/control/button_normal_small.png"},
    {type:'image', src:"image/framework/control/login_jinDu.png"},
    {type:'image', src:"image/newplayer/bg_scene2.png"},
    {type:'image', src:"image/framework/word/binFa_chengGong.png"},
    {type:'image', src:"image/framework/control/footer_icon_zhengTao.png"},
    {type:'sound', src:"music/effect_male_voice.mp3"},
    {type:'sound', src:"music/effect_book.mp3"},
    {type:'ccbi', src:"image/interface/PageHome.ccbi"},
    {type:'image', src:"image/framework/word/num9.png"},
    {type:'image', src:"image/framework/word/fp_bag_bar.png"},
    {type:'image', src:"image/var/NewPlayMovie/bg_scene1.png"},
    {type:'plist', src:"image/var/skill/skill19.plist"},
    {type:'image', src:"image/var/skill/skillWeapon/chui.png"},
    {type:'image', src:"image/framework/word/fp_hero_bar.png"},
    {type:'image', src:"image/framework/control/minis_large_flag.png"},
    {type:'ccbi', src:"image/interface/AttackSkill14.ccbi"},
    {type:'image', src:"image/framework/control/flag_all.png"},
    {type:'image', src:"image/framework/control/button_map_normal.png"},
    {type:'image', src:"image/var/skill/skill20.png"},
    {type:'image', src:"image/var/background/fp_bg.png"},
    {type:'plist', src:"image/var/skill/KingLeveUp.plist"},
    {type:'image', src:"image/framework/control/chongzhi.png"},
    {type:'image', src:"image/framework/icon/icon_sp.png"},
    {type:'image', src:"image/framework/control/radiobox_normal.png"},
    {type:'image', src:"image/framework/control/button_normal_large_dianji.png"},
    {type:'image', src:"image/framework/control/btn_continue_task.png"},
    {type:'image', src:"image/framework/control/flag_bing.png"},
    {type:'image', src:"image/framework/control/chuangGuan_jiXuHuangKuang.png"},
    {type:'plist', src:"image/var/skill/skill16.plist"},
    {type:'image', src:"image/var/background/bg_shop_2.png"},
    {type:'ccbi', src:"image/interface/AttackSkill02_From.ccbi"},
    {type:'plist', src:"image/newplayer/kaichangyuansu.plist"},
    {type:'image', src:"image/var/skill/skill22.png"},
    {type:'ccbi', src:"image/interface/AttackEffect02.ccbi"},
    {type:'plist', src:"image/var/skill/skill13.plist"},
    {type:'image', src:"image/framework/control/sell_hui.png"},
    {type:'plist', src:"image/var/skill/skill25.plist"},
    {type:'image', src:"image/framework/control/footer_icon_shouYe.png"},
    {type:'image', src:"image/var/skill/attackEffect02.png"},
    {type:'image', src:"image/var/background/fp_tech_press.png"},
    {type:'image', src:"image/framework/word/num6.png"},
    {type:'image', src:"image/framework/control/button_normal_large_hui.png"},
    {type:'image', src:"image/ccbResources/task_bg_2.png"},
    {type:'image', src:"image/framework/control/footer_icon_leiTai_transparent.png"},
    {type:'image', src:"image/framework/word/zhanDou_xiaoSheng.png"},
    {type:'image', src:"image/framework/word/v-2.png"},
    {type:'image', src:"image/framework/control/grcolor_line_01.png"},
    {type:'image', src:"image/framework/control/flag_ma.png"},
    {type:'image', src:"image/var/background/bg_chuangGuan.png"},
    {type:'image', src:"image/framework/control/login_grayBoard.png"},
    {type:'image', src:"image/framework/control/dialog_btn_close_normal.png"},
    {type:'ccbi', src:"image/interface/AttackSkill19.ccbi"},
    {type:'plist', src:"image/var/skill/skill22.plist"},
    {type:'ccbi', src:"image/interface/HeroIcon.ccbi"},
    {type:'image', src:"image/var/skill/skill24.png"},
    {type:'ccbi', src:"image/interface/AttackSkill07.ccbi"},
    {type:'image', src:"image/framework/word/huodong_duobao.png"},
    {type:'image', src:"image/framework/control/login_enter2.png"},
    {type:'image', src:"image/framework/word/v-5.png"},
    {type:'image', src:"image/var/skill/skillName/zhenFa_qingLong.png"},
    {type:'image', src:"image/framework/control/chuangGuan_anNiu.png"},
    {type:'image', src:"image/framework/control/task_star_bar_bg.png"},
    {type:'image', src:"image/framework/control/chuangGuan_anNiu_transparent.png"},
    {type:'image', src:"image/framework/word/FightNumber.png"},
    {type:'image', src:"image/framework/control/teshu_button.png"},
    {type:'image', src:"image/framework/control/challenge_top_info_bg.png"},
    {type:'image', src:"image/ccbResources/head_box_blue.png"},
    {type:'image', src:"image/var/background/fp_palace_press.png"},
    {type:'sound', src:"music/effect_battle_win.mp3"},
    {type:'image', src:"image/framework/word/qiangHua_dengJJ.png"},
    {type:'image', src:"image/var/skill/skill10.png"},
    {type:'image', src:"image/framework/control/tab_selected_transparent.png"},
    {type:'image', src:"image/framework/icon/neiwu_message.png"},
    {type:'image', src:"image/framework/word/v-8.png"},
    {type:'image', src:"image/framework/icon/refine.png"},
    {type:'ccbi', src:"image/interface/AttackSkill20.ccbi"},
    {type:'image', src:"image/var/NewPlayMovie/kaichangyuansu.png"},
    {type:'image', src:"image/framework/control/head.png"},
    {type:'image', src:"image/var/background/fp_bag.png"},
    {type:'plist', src:"image/var/NewPlayMovie/kaichangyuansu.plist"},
    {type:'ccbi', src:"image/interface/FightNumber.ccbi"},
    {type:'image', src:"image/framework/control/grline_name_01.png"},
    {type:'image', src:"image/framework/control/chuangGuan_anNiu_dianji.png"},
    {type:'image', src:"image/framework/word/fp_tech_bar.png"},
    {type:'image', src:"image/var/skill/skill12.png"},
    {type:'image', src:"image/var/skill/KingLeveUp_donghua.png"},
    {type:'image', src:"image/framework/word/jiangHun_yinZhang.png"},
    {type:'plist', src:"image/var/skill/skill07.plist"},
    {type:'image', src:"image/var/background/loginLoading.png"},
    {type:'ccbi', src:"image/interface/BootLoader.ccbi"},
    {type:'image', src:"image/framework/control/sell_dianji.png"},
    {type:'plist', src:"image/var/skill/skill04.plist"},
    {type:'ccbi', src:"image/interface/AttackZhenFaName.ccbi"},
    {type:'image', src:"image/var/background/fp_hero_press.png"},
    {type:'image', src:"image/var/skill/skill14.png"},
    {type:'ccbi', src:"image/interface/AttackEffect01.ccbi"},
    {type:'image', src:"image/framework/control/footer_selected_light.png"},
    {type:'ccbi', src:"image/interface/KingLeveUp_dianjipingmu.ccbi"},
    {type:'ccbi', src:"image/interface/AttackSkill25.ccbi"},
    {type:'image', src:"image/framework/control/button_normal_small_transparent.png"},
    {type:'image', src:"image/var/background/fp_function_press.png"},
    {type:'ccbi', src:"image/interface/AttackSkill13.ccbi"},
    {type:'plist', src:"image/var/skill/skill01.plist"},
    {type:'image', src:"image/var/NewPlayMovie/txt3.png"},
    {type:'image', src:"image/framework/control/head_bg_xin.png"},
    {type:'image', src:"image/framework/icon/icon_arrow_small_left.png"},
    {type:'ccbi', src:"image/interface/LoginNameBoard.ccbi"},
    {type:'image', src:"image/framework/icon/active_levBox.png"},
    {type:'image', src:"image/framework/word/fp_weapon_bar.png"},
    {type:'image', src:"image/framework/control/chuangGuan_anNiu_hui.png"},
    {type:'image', src:"image/framework/control/zhanDou_huiBan.png"},
    {type:'image', src:"image/var/background/bg_shop_3.png"},
    {type:'image', src:"image/framework/control/radiobox_selected.png"},
    {type:'image', src:"image/var/skill/skill16.png"},
    {type:'image', src:"image/var/background/bg_reborn_01.png"},
    {type:'image', src:"image/framework/control/head_box_blue.png"},
    {type:'image', src:"image/framework/control/active_deco.png"},
    {type:'image', src:"image/framework/control/active_enter.png"},
    {type:'plist', src:"image/var/skill/skill10.plist"},
    {type:'image', src:"image/framework/control/level_large.png"},
    {type:'image', src:"image/framework/word/login_allServer.png"},
    {type:'image', src:"image/framework/control/footer.png"},
    {type:'image', src:"image/ccbResources/fight_blood_blue.png"},
    {type:'image', src:"image/var/skill/attackEffect03.png"},
    {type:'image', src:"image/framework/control/task_instance_info_bg.png"},
    {type:'image', src:"image/var/skill/skill02.png"},
    {type:'image', src:"image/framework/control/fight_blood_red.png"},
    {type:'image', src:"image/framework/word/num1.png"},
    {type:'image', src:"image/framework/control/card_level_line.png"},
    {type:'ccbi', src:"image/interface/CardIcon.ccbi"},
    {type:'image', src:"image/framework/word/ward_form_switch.png"},
    {type:'image', src:"image/var/skill/skill18.png"},
    {type:'sound', src:"music/effect_battle_lose.mp3"},
    {type:'image', src:"image/framework/control/flag_wu.png"},
    {type:'ccbi', src:"image/interface/AttackSkill18.ccbi"},
    {type:'image', src:"image/framework/word/num7.png"},
    {type:'image', src:"image/framework/control/button_normal_large.png"},
    {type:'ccbi', src:"image/interface/AttackSkill06.ccbi"},
    {type:'plist', src:"image/var/skill/attackEffect04.plist"},
    {type:'image', src:"image/framework/word/zhanDou_wanBai.png"},
    {type:'image', src:"image/var/skill/skill04.png"},
    {type:'image', src:"image/framework/control/chakan_difang.png"},
    {type:'image', src:"image/framework/control/chuangGan_bg_diWen.png"},
    {type:'image', src:"image/var/skill/skillWeapon/qiang.png"},
    {type:'ccbi', src:"image/interface/Logo.ccbi"},
    {type:'ccbi', src:"image/interface/HeroListBar.ccbi"},
    {type:'image', src:"image/framework/control/teshu_button_hui.png"},
    {type:'image', src:"image/framework/icon/icon_sp_big.png"},
    {type:'image', src:"image/var/skill/skillWeapon/ji.png"},
    {type:'plist', src:"image/var/skill/skill24.plist"},
    {type:'plist', src:"image/ccbResources/herobox.plist"},
    {type:'image', src:"image/framework/control/pvp_ziji.png"},
    {type:'image', src:"image/framework/control/button_normal_small_hui.png"},
    {type:'image', src:"image/framework/control/button_normal_increase_dianji.png"},
    {type:'image', src:"image/framework/icon/icon_vm.png"},
    {type:'image', src:"image/framework/control/button_normal_right.png"},
    {type:'image', src:"image/framework/word/vip.png"},
    {type:'image', src:"image/framework/word/zhanDou_daBai.png"},
    {type:'image', src:"image/var/skill/skill06.png"},
    {type:'image', src:"image/framework/control/head_box_green.png"},
    {type:'image', src:"image/framework/word/LevelNumber.png"},
    {type:'ccbi', src:"image/interface/AttackSkill02_To.ccbi"},
    {type:'ccbi', src:"image/interface/newplayer1.ccbi"},
    {type:'image', src:"image/framework/control/login_enter1.png"},
    {type:'image', src:"image/var/NewPlayMovie/bg_scene2.png"},
    {type:'image', src:"image/framework/word/num4.png"},
    {type:'image', src:"image/var/background/loginLoading_line.png"},
    {type:'image', src:"image/framework/word/login_history.png"},
    {type:'image', src:"image/framework/control/wuJiang_xuanZhe.png"},
    {type:'ccbi', src:"image/interface/DieLabel.ccbi"},
    {type:'image', src:"image/var/skill/skill08.png"},
    {type:'ccbi', src:"image/interface/TinyNarrowHeader.ccbi"},
    {type:'image', src:"image/var/NewPlayMovie/dialogue.png"},
    {type:'image', src:"image/newplayer/kaichangyuansu.png"},
    {type:'plist', src:"image/var/skill/skill09.plist"},
    {type:'image', src:"image/ccbResources/head_box_red.png"},
    {type:'image', src:"image/framework/control/chuShou_tiaoKuang_wuHuaWen.png"},
    {type:'ccbi', src:"image/interface/AttackSkill24.ccbi"},
    {type:'ccbi', src:"image/interface/NarrowHeader.ccbi"},
    {type:'image', src:"image/var/skill/zhuanSheng/zhuanSheng_buff_int.png"},
    {type:'sound', src:"music/effect_attack.mp3"},
    {type:'image', src:"image/framework/control/footer_selected.png"},
    {type:'ccbi', src:"image/interface/AttackSkill12.ccbi"},
    {type:'image', src:"image/var/active/endlessWar/ttt_bg4.png"},
    {type:'image', src:"image/var/skill/skillIcon/buff_att.png"},
    {type:'image', src:"image/framework/word/v-1.png"},
    {type:'image', src:"image/framework/control/head_box_red_transparent.png"},
    {type:'ccbi', src:"image/interface/FightBlankLevel.ccbi"},
    {type:'plist', src:"image/var/skill/skill06.plist"},
    {type:'plist', src:"image/var/skill/skill18.plist"},
    {type:'image', src:"image/var/skill/KingLeveUp.png"},
    {type:'image', src:"image/framework/word/shengJi_daCheng.png"},
    {type:'sound', src:"music/effect_weapon.mp3"},
    {type:'image', src:"image/var/skill/skillWeapon/guzi.png"},
    {type:'ccbi', src:"image/interface/AttackSkill01_From.ccbi"},
    {type:'image', src:"image/framework/icon/icon_intelligence.png"},
    {type:'image', src:"image/framework/control/button_normal_small_dianji.png"},
    {type:'image', src:"image/framework/word/v-4.png"},
];
/**
 * Created with JetBrains WebStorm.
 * User: qidea
 * Date: 6/24/13
 * Time: 3:36 下午
 * To change this template use File | Settings | File Templates.
 */
function FightBlankLevel(){

};
FightBlankLevel.prototype.onDidLoadFromCCB=function(){
    cc.log("onDidLoading");
    this.rootNode.onEnterTransitionDidFinish=function(){
        //rootNodeEnterFun();
        if(sys.platform!="browser"){

            cc.log("before clear memory...O(∩_∩)O~");
            cc.TextureCache.getInstance().dumpCachedTextureInfo();
            cc.TextureCache.getInstance().removeAllTextures();
            cc.SpriteFrameCache.getInstance().removeSpriteFrames();

            //cc.TextureCache.getInstance().removeUnusedTextures();
            //cc.SpriteFrameCache.getInstance().removeUnusedSpriteFrames();
            cc.log("after clear memory....O(∩_∩)O~");
            cc.TextureCache.getInstance().dumpCachedTextureInfo();
            cc.log("finished memory.......O(∩_∩)O~");
        }
    }
};
FightBlankLevel.prototype. onEnterTransitionDidFinish =function(){
    
};function LoadingRes(){

};
LoadingRes.prototype.onDidLoadFromCCB=function(){
    cc.log("LoadingRes onDidLOadFromCCB");
    var me = this;
    me.loadingLine.setAnchorPoint({x:0,y:0.5});
    me.updateProgress(0,"正在加载资源");
};


LoadingRes.prototype.updateProgress=function(percent,msg){
    var me = this;
    if(percent>100){
        percent=100;
    }
    if(typeof me.updatePrecentCallBack=="function"){
        cc.Director.getInstance().getScheduler().unscheduleAllCallbacksForTarget(me.msg);
    }
    me.loadingLine.setPreferredSize({width:520.0*percent/100+26,height:20});
    me.msg.setString(msg);
};
LoadingRes.prototype.updateMsg=function(msg){
    var me = this;
    me.msg.setString(msg);
};
LoadingRes.prototype.updateMsg2=function(msg){
    var me = this;
    me.msg2.setString(msg);
};

LoadingRes.prototype.updatePrecent=function(percent,time){
    var me = this;
    var curPercent =0;
    cc.Director.getInstance().getScheduler().scheduleCallbackForTarget(me.msg,function(){
        cc.log("update precent:"+curPercent);
        curPercent+=1;
        if(curPercent>=percent){
            curPercent=percent;
            cc.Director.getInstance().getScheduler().unscheduleAllCallbacksForTarget(me.msg);
        }
        me.loadingLine.setPreferredSize({width:520.0*curPercent/100+26,height:20});

    },time/percent,cc.REPEAT_FOREVER ,0, false);
    cc.log("------updatePercent-------");

};

LoadingRes.prototype.showDialogMsg=function(){
    var me = this;
    me.DialogMsg.setVisible(true);
};

LoadingRes.prototype.hideDialogMsg=function(){
    var me = this;
    me.DialogMsg.setVisible(false);
};

LoadingRes.prototype.btnOKClick=function(){
    var me = this;
    cc.log("----------btnOKClick---------------");
    me.hideDialogMsg();
    var loader = Qidea.Downloader.getInstance();
    loader.DownLoaderBegin();
};/*****************************************
 Qidea-Cocos native+html 兼容API   Begin
 ****************************************/
var QCC={};
/**
 * 查询节点是否可见
 * @param node
 * @return {Boolean}
 */
QCC.isNodeVisible=function(node){
    var isVisible=true;
    if(node.isVisible()==false){
        return false;
    }else{
        var curNode = node.getParent();
        while(curNode!=null){
            if(curNode.isVisible()==false){
                return false;
            }
            curNode = curNode.getParent();
        }
    }
    return isVisible;
}
/**
 * 判断node是否包含touch点
 * @param node
 * @param touch
 * @return {Boolean}
 */
QCC.rectContainsTouchPoint=function (node,touch) {
    if(sys.platform=="browser"){
        //for browser
        cc.log(node.getBoundingBoxToWorld());
        cc.log(touch.getLocation());
        if(cc.rectContainsPoint(node.getBoundingBoxToWorld(),touch.getLocation()) && QCC.isNodeVisible(node)){
            return true;
        }else{
            return false;
        }
    }else{
        //for native
        var  s =node.getContentSize();
        var rect= cc.rect(0, 0, s.width, s.height);

        cc.log("{"+rect.x+","+rect.y+"}  {"+rect.width+","+rect.height+"}");
        //查询子节点的BoundingBox
        var children = node.getChildren();
        if (children){
            for (var i = 0; i < children.length; i++) {
                var child = children[i];
                if (child && child.isVisible()) {
                    var childSize = child.getContentSize();
                    var childPoint = node.convertToNodeSpace(child.getPosition());
                    var childRect = cc.rect(childPoint.x, childPoint.y, childSize.width, childSize.height);
                    if (childRect) {
                        rect = cc.rectUnion(rect, childRect);
                    }
                }
            }
        }

        cc.log(node.getAnchorPoint().x+","+node.getAnchorPoint().y);
        cc.log("{"+rect.x+","+rect.y+"}  {"+rect.width+","+rect.height+"}");
        cc.log("{"+node.convertTouchToNodeSpaceAR(touch).x+","+node.convertTouchToNodeSpaceAR(touch).y+"} ");
        if(cc.rectContainsPoint(rect,node.convertTouchToNodeSpace(touch)) && QCC.isNodeVisible(node) ){
            cc.log("clicked");
            return true;
        }else{
            cc.log("not clicked");
            return false;
        }
    }
}
/**
 * 代理对象添加事件
 * @param delegate  代理对象
 * @param priority  优先级  0+
 * @param swallowsTouches 是否运行多点触碰
 */
QCC.addTargetedDelegate=function(delegate, priority, swallowsTouches){
    if(sys.platform=="browser"){
        //for browser
        cc.Director.getInstance().getTouchDispatcher().addTargetedDelegate(delegate,priority,swallowsTouches);
    }else{
        //for native
        cc.registerTargettedDelegate(priority,swallowsTouches,delegate);
    }
};

QCC.addClick=function(node,callback){
    node.onTouchBegan=function(touch,event){
        return QCC.rectContainsTouchPoint(node,touch);
    }
    node.onTouchEnded =function(touch,event){
        callback();
    }
    QCC.addTargetedDelegate(node,0,true);
};
/*****************************************
 Qidea-Cocos native+html 兼容API   End
 ****************************************/

function CBootLoader(){

};
CBootLoader.prototype.onDidLoadFromCCB=function(){
    cc.log("CBootLoader onDidLOadFromCCB");
    var me = this;
    me.loadingLine.setAnchorPoint({x:0,y:0.5});
    me.updateProgress(0,"正在加载资源");
    me.btnOK.setCallback(me.btnOKClick,me);
    me.hideAll();
    me.bindClickTxtChooseServer();
    me.bindClickChooseServer();
    //点击选区按钮上添加事件
    me.show(CBootLoader.COMP_TYPE.EnterGame);

};

CBootLoader.prototype.bindClickChooseServer=function(){
    var me = this;
    QCC.addClick(me[CBootLoader.COMP_TYPE.ChooseServer],function(){
        me.show(CBootLoader.COMP_TYPE.EnterGame);
    });
};

CBootLoader.prototype.bindClickTxtChooseServer=function(){
    var me = this;
    QCC.addClick(me.txtChooseServer,function(){
        me.show(CBootLoader.COMP_TYPE.ChooseServer);
    });
};

CBootLoader.prototype.updateProgress=function(percent,msg){
    var me = this;
    if(percent>100){
        percent=100;
    }
    me.loadingLine.setPreferredSize({width:520.0*percent/100+26,height:20});
    me.msg.setString(msg);
};
CBootLoader.prototype.updateMsg=function(msg){
    var me = this;
    me.msg.setString(msg);
};
CBootLoader.prototype.updateMsg2=function(msg){
    var me = this;
    me.msg2.setString(msg);
};

CBootLoader.prototype.clearMsgSchedule=function(){
    var me = this;
    cc.Director.getInstance().getScheduler().unscheduleAllCallbacksForTarget(me.msg);
};
CBootLoader.prototype.updatePrecent=function(percent,time){
    var me = this;
    var curPercent =0;
    cc.Director.getInstance().getScheduler().scheduleCallbackForTarget(me.msg,function(){
        cc.log("update precent:"+curPercent);
        curPercent+=1;
        if(curPercent>=percent){
            curPercent=percent;
            cc.Director.getInstance().getScheduler().unscheduleAllCallbacksForTarget(me.msg);
        }
        me.loadingLine.setPreferredSize({width:520.0*curPercent/100+26,height:20});

    },time/percent,cc.REPEAT_FOREVER ,0, false);
    cc.log("------updatePercent-------");

};

CBootLoader.COMP_TYPE={
    DialogMsg:"dialogMsg",
    Process:"loadingProcess",
    EnterGame:"loadingEnter",
    CheckVersion:"loadingCheckVersion",
    ChooseServer:"loadingChooseServer"

}

CBootLoader.prototype.hideAll=function(){
    var me = this;
    me.hide(CBootLoader.COMP_TYPE.DialogMsg);
    me.hide(CBootLoader.COMP_TYPE.Process);
    me.hide(CBootLoader.COMP_TYPE.EnterGame);
    me.hide(CBootLoader.COMP_TYPE.CheckVersion);
    me.hide(CBootLoader.COMP_TYPE.ChooseServer);
};
CBootLoader.prototype.hide=function(compType){
    var me = this;
    me[compType].setVisible(false);
}
CBootLoader.prototype.show=function(compType){
    var me = this;
    me.hideAll();
    me[compType].setVisible(true);
};
CBootLoader.prototype.runLoading=function(){
    var me = this;

};

CBootLoader.prototype.btnOKClick=function(item){
    cc.log("----------btnOKClick---------------");
    this.hideDialogMsg(false);
    var loader = QideaCC.Downloader.getInstance();
    loader.DownLoaderBegin();
};

CBootLoader.prototype.onEnterGame=function(){
    cc.log("----------onEnterGame---------------");
    qBootLoader.startGame();
};
/**
 * Created with JetBrains WebStorm.
 * User: qidea
 * Date: 5/31/13
 * Time: 11:36 上午
 * To change this template use File | Settings | File Templates.
 */
var FightHeroTalk=function(){

};

FightHeroTalk.prototype.onDidLoadFromCCB=function(){

};

FightHeroTalk.prototype.setMsg=function(msg,isRight){
    var me = this;
    var msgTxt = msg;
    if(isRight){
        me.msgBg.setScaleX(-1.0);
        me.msg.setScaleX(-1.0);
    }
    if(msg.length>6){
        msgTxt=msgTxt.substr(0,6)+"\n"+msgTxt.substr(6);
    }
    me.msg.setString(msgTxt);
}

/**
 * Created with JetBrains WebStorm.
 * User: qidea
 * Date: 5/31/13
 * Time: 5:02 下午
 * To change this template use File | Settings | File Templates.
 */
var NewPlayer3=function(){

};
NewPlayer3.prototype.onDidLoadFromCCB=function(){
    var me = this;
    if (typeof CONFIG != "undefined" && typeof CONFIG.settingInfo != "undefined" && CONFIG.settingInfo.bgMusic) {
        CocosEval('cc.AudioEngine.getInstance().pauseMusic();');
    }
    var actionObj = cc.Sequence.create(cc.DelayTime.create(10.0), cc.CallFunc.create(function () {
        if(sys.platform=="browser"){
            Qidea.pageManager.gotoPage(PageNewUser);
        }else{
            //in native
            Pg.hideGL();
            forceGC();
        }
    }));
    me.rootNode.runAction(actionObj);
};
/**
 * Created with JetBrains WebStorm.
 * User: qidea
 * Date: 2/21/13
 * Time: 10:19 下午
 * To change this template use File | Settings | File Templates.
 */
var CCAnimUpgrade=function(){
    //cc.log("FightAnimHeroComp constructor");
    this.number=0;
};
CCAnimUpgrade.prototype.onUpdate=function(){
    //cc.log("FightAnimHeroComp onUpdate");
};
CCAnimUpgrade.prototype.onDidLoadFromCCB=function(){
    var actionObj = cc.Sequence.create(cc.DelayTime.create(2), cc.CallFunc.create(function () {
        //cc.log("removeChild");
        if(sys.platform=="browser"){
            $("#pageFightAnimateClient").hide();
            $db.trigger("PlayCCAnimUpgradeDone");
        }else{
            CocosEval('typeof Pg!="undefined" && typeof Pg.hideGL=="function" && Pg.hideGL();');
            Pg.eval('$db.trigger("PlayCCAnimUpgradeDone");');
            cc.TextureCache.getInstance().removeAllTextures();
            QideaCC.GLclearColor(cc.c4f(0.0,0.0,0.0,1.0));
            if(sys.platform!="browser"){
                QideaCC.setIdleTimerDisabled(false);
            }

        }
    }));
    this.rootNode.runAction(actionObj);
};
/**
 * Created with JetBrains WebStorm.
 * User: qidea
 * Date: 2/21/13
 * Time: 10:19 下午
 * To change this template use File | Settings | File Templates.
 */
var AttackRound=function(){
    //cc.log("FightAnimHeroComp constructor");
    this.numb=1;
};
AttackRound.prototype.onUpdate=function(){
    //cc.log("FightAnimHeroComp onUpdate");
};
AttackRound.prototype.onDidLoadFromCCB=function(){
    //cc.log("FightAnimHeroComp onDidLOadFromCCB");
};
AttackRound.prototype.setNumber=function(num){
    if(typeof num=="number"){
        this.num=num;
    }
    this.rootNode.controller.number.setString(this.num+"");
};
/**
 * CDialog.js
 * Desc: 功能描述
 * User: Colin3dmax
 * Date: 12-12-24
 * Time: 下午9:00
 */
function CDialog(){
    this.uuid=this.constructor.caller.name+QUtil.createId();
    this.uuidClient=this.uuid+"-client";

};
CDialog.prototype.onUpdate=function(){
    cc.log("CDialog onUpdate");
};
CDialog.prototype.onDidLoadFromCCB=function(){
    cc.log("CDialog onDidLOadFromCCB");
    var me = this;
    me.btnClose.addTargetWithActionForControlEvent(me,me.onBtnCloseClick,cc.CONTROL_EVENT_TOUCH_DOWN);
    me.btnOK.addTargetWithActionForControlEvent(me,me.onBtnOKClick,cc.CONTROL_EVENT_TOUCH_DOWN);
    me.btnCancel.addTargetWithActionForControlEvent(me,me.onBtnCancelClick,cc.CONTROL_EVENT_TOUCH_DOWN);
};
CDialog.prototype.getUUID=function(){
    return this.uuid;
};
/**
 *  二进制 标识位
 *   x        x      x       x
 * Cancel    OK     Close   Client
 * 对应位上有标识  1  为显示 ，0为不显示
* */
CDialog.DLG_TYPE={
    CLIEN:1,
    CLOSE:2,
    CANCEL:4,
    OK:8,
    ALL:15
};
CDialog.prototype.hasDlgType=function( dlgType ){
    return (this.cfg.dlgType & dlgType)!=0;
};

CDialog.prototype.setTitle=function(titleName){
    var me = this;
    if(typeof titleName=="string" && titleName!=""){
        me.title.setString(titleName);
    }
}
CDialog.prototype.getTitle=function(){
    return this.title.getString();
}

CDialog.prototype.onBtnCloseClick=function(){
    cc.log("onBtnCloseClick");
    var me = this;
    var dialogWrapper=me.cdialog.getParent();
    dialogWrapper.setVisible(false);
    dialogWrapper.removeAllChildren();
}

CDialog.prototype.onBtnOKClick=function(){
    cc.log("onBtnOKClick");
}

CDialog.prototype.onBtnCancelClick=function(){
    cc.log("onBtnCancelClick");
}

function CHeroListBar(cfg){
    var me = this;

};
CHeroListBar.prototype.onDidLoadFromCCB=function(){
    cc.log("CHeroListBar onDidLOadFromCCB");
    var me = this;
    /*
    //添加滚动
    var winSize = cc.Director.getInstance().getWinSize();
    var tableView = cc.TableView.create(this, me.scrollArea.getContentSize());
    tableView.setDirection(cc.SCROLLVIEW_DIRECTION_HORIZONTAL);
    tableView.setPosition(me.scrollArea.getPosition());
    tableView.setDelegate(this);
    me.rootNode.addChild(tableView,999);
    var sprite = cc.BuilderReader.load("image/interface/CardIcon.ccbi");

    me.scrollArea.setZOrder(999);
    me.scrollArea.addChild(sprite,99);
    tableView.reloadData();
    *////////sss
};

/****************TableView****************/
    /*
var CustomTableViewCell = cc.TableViewCell.extend({
    draw:function (ctx) {
        this._super(ctx);
    }
});

CHeroListBar.prototype.toExtensionsMainLayer=function (sender) {

};

CHeroListBar.prototype.scrollViewDidScroll=function (view) {
};
CHeroListBar.prototype.scrollViewDidZoom=function (view) {
};

CHeroListBar.prototype.tableCellTouched=function(table, cell) {
    cc.log("cell touched at index: " + cell.getIdx());
};

CHeroListBar.prototype.cellSizeForTable=function (table) {
    return cc.SizeMake(96, 140);
};

CHeroListBar.prototype.tableCellAtIndex=function (table, idx) {
    var strValue = idx.toFixed(0);
    var cell = table.dequeueCell();
    var label;
    if (!cell) {
        cell = new CustomTableViewCell();
        var sprite = cc.BuilderReader.load("image/interface/CardIcon.ccbi");
        //sprite.setAnchorPoint(cc.p(0,0));
        //sprite.setPosition(cc.p(0, 0));
        cell.addChild(sprite);

        label = cc.LabelTTF.create(strValue, "Helvetica", 20.0);
        label.setPosition(cc.p(0,0));
        label.setAnchorPoint(cc.p(0,0));
        label.setTag(123);
        cell.addChild(label);
    } else {
        label = cell.getChildByTag(123);
        label.setString(strValue);
    }

    return cell;
};

CHeroListBar.prototype.numberOfCellsInTableView=function (table) {
    return 25;
};
*/
/******************************************//**
 * CFooter.js
 * Desc: 功能描述
 * User: Colin3dmax
 * Date: 13-1-11
 * Time: 下午6:10
 */
function CFooter() {
    var me = this;
    this.lastHightLightMenuName=null;
};

CFooter.prototype.onUpdate=function(){
    cc.log("FightAnimHeroComp onUpdate");
};
CFooter.prototype.onDidLoadFromCCB=function(){
    cc.log("FightAnimHeroComp onDidLOadFromCCB");

};

CFooter.prototype.getMenuNameByTag=function(tag){
    var menuName="";
    switch(tag){
        case 0:
            menuName="home";
            break;
        case 1:
            menuName="team";
            break;
        case 2:
            menuName="instanceZone";
            break;
        case 3:
            menuName="challenge";
            break;
        case 4:
            menuName="fightActiveZone";
            break;
        case 5:
            menuName="shop";
            break;
    }
    return menuName;
};

CFooter.prototype.getPageNameByTag=function(tag){
    var pageName="";
    switch(tag){
        case 0:
            pageName="PageHome";
            break;
        case 1:
            pageName="PageTeam";
            break;
        case 2:
            pageName="PageInstanceZone";
            break;
        case 3:
            pageName="PageChallenge";
            break;
        case 4:
            pageName="PageFightActiveZone";
            break;
        case 5:
            pageName="PageShop";
            break;
    }
    return pageName;
};

CFooter.prototype.lightOffMenu=function(menuName){
    var me = this;
    if(typeof menuName=="string" && menuName!=""){
        me["footer_"+menuName+"_selected"].setVisible(false);
        me["footer_"+menuName+"_light"].setVisible(false);
    }
};

CFooter.prototype.hightLightMenu=function(menuName){
    var me = this;
    me.lightOffMenu(me.lastHightLightMenuName);
    if(typeof menuName=="string" && menuName!=""){
        me["footer_"+menuName+"_selected"].setVisible(true);
        me["footer_"+menuName+"_light"].setVisible(true);
    }
    me.lastHightLightMenuName=menuName;
};

CFooter.prototype.gotoPage=function(pageName,menuName){
    var me = this;
    //场景切换
    /*
    if(typeof pageName=="string" && pageName!=""){
        var scene = cc.BuilderReader.loadAsScene("image/interface/"+pageName+".ccbi");
        var runningScene = cc.Director.getInstance().getRunningScene();
        if (runningScene === null)
            cc.Director.getInstance().runWithScene(scene);
        else
            cc.Director.getInstance().replaceScene(scene);

    }
    */
    var scene = cc.BuilderReader.loadAsScene("image/interface/Loader.ccbi");
    var runningScene = cc.Director.getInstance().getRunningScene();
    if (runningScene === null)
        cc.Director.getInstance().runWithScene(scene);
    else
        cc.Director.getInstance().replaceScene(scene);


};

CFooter.prototype.click=function(node){
    var me = this;
    var menuName = me.getMenuNameByTag(node.getTag());
    var pageName = me.getPageNameByTag(node.getTag());
    me.hightLightMenu(menuName);
    me.gotoPage(pageName,menuName);
};

/**
 * CFooterBar.js
 * Desc: 功能描述
 * User: Colin3dmax
 * Date: 13-1-11
 * Time: 下午6:10
 */
function CFooterBar() {
    var me = this;
};

CFooterBar.prototype.onUpdate=function(){
    cc.log("CFooterBar onUpdate");
};
CFooterBar.prototype.onDidLoadFromCCB=function(){
    cc.log("CFooterBar onDidLOadFromCCB");

};
function CNarrowHeader(cfg){
    var me = this;

};
CNarrowHeader.prototype.onDidLoadFromCCB=function(){
    cc.log("CDialog onDidLOadFromCCB");
    var me = this;

    me.touchLayer.setTouchPriority(-999);
    me.touchLayer.setMousePriority(-999);
    
    me.bind(me.touchLayer,"click",function(touches,event){
        cc.log("me.touchLayer: click");
        cc.log(touches,event);
        new CDialogKingInfo(me.rootNode.getParent().controller.footerBar.controller.dialogWrapper);
        return true;
    });
};

CNarrowHeader.prototype.bind=function(layer,eventName,callback){
    switch(eventName){
        case "click":
            layer.onTouchBegan=function(){return true; };
            layer.onTouchEnd=callback;
            layer.onMouseDown=function(){return true; };;
            layer.onMouseUp=callback;
            break;
    }
};
/**
 * CDialogKingInfo
 * Desc: 君主信息对话框
 * User: Colin3dmax
 * Date: 13-1-7
 * Time: 上午9:54
 */
function CDialogKingInfo($wrapper){
    var me=this;
    me.winSize=cc.Director.getInstance().getWinSize();
    me.dialogNode= cc.BuilderReader.load("image/interface/CDialog.ccbi");
    $wrapper.addChild(me.dialogNode);
    me.layoutCenter();
    $wrapper.setVisible(true);
};

CDialogKingInfo.prototype.layoutCenter=function(){
    var me = this;
    //注意：屏幕高度为实际的像素大小，而对话框所在分辨率未640*960分辨率
    me.dialogNode.setPosition(me.winSize.width,me.winSize.height+me.dialogNode.getContentSize().height/2);
    me.dialogNode.controller.setTitle("君主信息");
};
/**
 *
 Qidea.pageManager.playCocosAnim("image/interface/KingLeveUp.ccbi",CPageKingLevelUp,{
         kingLevel:{from:0,to:0},
         heroLevel:{from:0,to:0},
         equipmentLevel:{from:0,to:0},
         onLineHeroMumber:{from:0,to:0}
     });
 * @constructor
 */
function CPageKingLevelUp() {
    var me = this;
    me.canClose = false;
};
CPageKingLevelUp.prototype.onDidLoadFromCCB = function () {
    var me = this;
    cc.log("CPageKingLevelUp onDidLoadFromCCB");
    me.setData(CPageKingLevelUp.cfg);
    var canHidePgCallBack = function () {
        me.canClose = true;
    };
    me.rootNode.scheduleOnce(canHidePgCallBack, 3);
};
CPageKingLevelUp.prototype.setData = function (config) {
    var me = this;
    var cfg = _.extend({
        kingLevel: {from: 0, to: 0},
        heroLevel: {from: 0, to: 0},
        equipmentLevel: {from: 0, to: 0},
        onLineHeroMumber: {from: 0, to: 0},
        rewards: {gold: 0, silver: 0}
    }, config);

    me.txtLevelFrom.setString(cfg.kingLevel.from);
    me.txtLevelTo.setString(cfg.kingLevel.to);
    me.txtHeroLevelFrom.setString(cfg.heroLevel.from);
    me.txtHeroLevelTo.setString(cfg.heroLevel.to);
    me.txtEquipmentLevelFrom.setString(cfg.equipmentLevel.from);
    me.txtEquipmentLevelTo.setString(cfg.equipmentLevel.to);
    me.txtOnLineHeroNumberFrom.setString(cfg.onLineHeroMumber.from);
    me.txtOnLineHeroNumberTo.setString(cfg.onLineHeroMumber.to);
    me.txtGold.setString(cfg.rewards.gold);
    me.txtSilver.setString(cfg.rewards.silver);
    if (cfg.onLineHeroMumber.from == cfg.onLineHeroMumber.to) {
        me.txtMsg1.setString("武将提升等级上限，可以继续升级了！");
        me.txtMsg2.setString("装备提升等级上限了，赶快去强化吧！");
        me.txtMsg3.setVisible(false);
        me.ccbOnLineHeroNumberWrapper.setVisible(false);
    }
};
CPageKingLevelUp.prototype.closeScreen = function () {
    var me = this;
    if (me.canClose) {
        cc.log("closeScreen");
        if (sys.platform == "browser") {
            $("#pageFightAnimateClient").hide();
            new DialogNewOpen();
        } else {
            //in cocos-GL View
            Pg.eval('new DialogNewOpen()');
//            Pg.eval('Qidea.pageManager.openDialog(DialogNewOpen);');
            Pg.hideGL();
            forceGC();
        }
    }
};/**
 * CPageHome.js
 * Desc: 功能描述
 * User: Colin3dmax
 * Date: 13-1-11
 * Time: 下午6:10
 */
function CPageHome() {
    var me = this;
    this.lastHightLightMenuName=null;
};

CPageHome.prototype.onUpdate=function(){
    cc.log("CPageHome onUpdate");
};
CPageHome.prototype.onDidLoadFromCCB=function(){
    cc.log("CPageHome onDidLOadFromCCB");
    var me = this;
    me.footerBar.controller.footerMenu.controller.hightLightMenu("home");
    //添加返回到HTML版本
    me.btnHTML.addTargetWithActionForControlEvent(me,me.backToHTML,cc.CONTROL_EVENT_TOUCH_DOWN);
};

CPageHome.prototype.backToHTML=function()
{
    if(CONFIG.cocosEngine=="canvas"){
        //canvas版
        if(typeof $=="function"){
            $("#pageFightAnimateClient").hide();
            var blankScene=new BlankScene();
            cc.Director.getInstance().pushScene(blankScene);
			if(typeof cordova!="undefined" && typeof cordova.exec!="undefined")
			cordova.exec(function(){},function(){},"CcEval","evalInBg",[ "qBootLoader.EnterGame();"]);
        }
    }

};
/**
 * CPageTeam.js
 * Desc: 功能描述
 * User: Colin3dmax
 * Date: 13-1-11
 * Time: 下午6:10
 */
function CPageTeam() {
    var me = this;
    this.lastHightLightMenuName=null;
};

CPageTeam.prototype.onUpdate=function(){
    cc.log("CPageTeam onUpdate");
};
CPageTeam.prototype.onDidLoadFromCCB=function(){
    cc.log("CPageTeam onDidLOadFromCCB");
    var me = this;
    me.footerBar.controller.footerMenu.controller.hightLightMenu("team");
};/**
 * CPageInstanceZone.js
 * Desc: 功能描述
 * User: Colin3dmax
 * Date: 13-1-11
 * Time: 下午6:10
 */
function CPageInstanceZone() {
    var me = this;
    this.lastHightLightMenuName=null;
};

CPageInstanceZone.prototype.onUpdate=function(){
    cc.log("CPageInstanceZone onUpdate");
};
CPageInstanceZone.prototype.onDidLoadFromCCB=function(){
    cc.log("CPageInstanceZone onDidLOadFromCCB");
    var me = this;
    me.footerBar.controller.footerMenu.controller.hightLightMenu("instanceZone");
};/**
 * CPageChallenge.js
 * Desc: 功能描述
 * User: Colin3dmax
 * Date: 13-1-11
 * Time: 下午6:10
 */
function CPageChallenge() {
    var me = this;
    this.lastHightLightMenuName=null;
};

CPageChallenge.prototype.onUpdate=function(){
    cc.log("CPageChallenge onUpdate");
};
CPageChallenge.prototype.onDidLoadFromCCB=function(){
    cc.log("CPageChallenge onDidLOadFromCCB");
    var me = this;
    me.footerBar.controller.footerMenu.controller.hightLightMenu("challenge");
};
/**
 * CPageFightActiveZone.js
 * Desc: 功能描述
 * User: Colin3dmax
 * Date: 13-1-11
 * Time: 下午6:10
 */
function CPageFightActiveZone() {
    var me = this;
    this.lastHightLightMenuName=null;
};

CPageFightActiveZone.prototype.onUpdate=function(){
    cc.log("CPageFightActiveZone onUpdate");
};
CPageFightActiveZone.prototype.onDidLoadFromCCB=function(){
    cc.log("CPageFightActiveZone onDidLOadFromCCB");
    var me = this;
    me.footerBar.controller.footerMenu.controller.hightLightMenu("fightActiveZone");
};/**
 * CPageShop.js
 * Desc: 功能描述
 * User: Colin3dmax
 * Date: 13-1-11
 * Time: 下午6:10
 */
function CPageShop() {
    var me = this;
    this.lastHightLightMenuName=null;
};

CPageShop.prototype.onUpdate=function(){
    cc.log("CPageShop onUpdate");
};
CPageShop.prototype.onDidLoadFromCCB=function(){
    cc.log("CPageShop onDidLOadFromCCB");
    var me = this;
    me.footerBar.controller.footerMenu.controller.hightLightMenu("shop");
};/**
 * Created with JetBrains WebStorm.
 * User: qidea
 * Date: 2/21/13
 * Time: 10:19 下午
 * To change this template use File | Settings | File Templates.
 */
var AttackSkillName=function(){
    cc.log("AttackSkillName constructor");
    this.number=0;
};
AttackSkillName.prototype.onUpdate=function(){
    cc.log("AttackSkillName onUpdate");
};
AttackSkillName.prototype.onDidLoadFromCCB=function(){
    cc.log("AttackSkillName onDidLOadFromCCB");
};
AttackSkillName.prototype.setSkillName=function(skillName){
    var skillNameStr=skillName;
    if(typeof skillName=="undefined" || skillName==""){
        skillNameStr=" ";
    }
    this.skillNameLabel.setString(skillNameStr);
};
AttackSkillName.prototype.setSkillNameTextureBySkillId=function(skillId){
    var skillNameImageTexture=cc.TextureCache.getInstance().addImage("image/var/skill/skillName/"+skillId+".png");
    this.skillNamePic.setTexture(skillNameImageTexture);
};/**
 * Created with JetBrains WebStorm.
 * User: qidea
 * Date: 2/21/13
 * Time: 10:19 下午
 * To change this template use File | Settings | File Templates.
 */
var ZhenFaSkillName=function(){
    this.number=0;
};
ZhenFaSkillName.prototype.onUpdate=function(){
};
ZhenFaSkillName.prototype.onDidLoadFromCCB=function(){
};
ZhenFaSkillName.prototype.setSkillNameTexture=function(skillName,isRight){
    var skillNameImageTexture=cc.TextureCache.getInstance().addImage("image/var/skill/skillName/"+skillName+".png");
    this.zhenFaName.setTexture(skillNameImageTexture);
    if(typeof isRight!="undefined" && isRight){
        this.zhenFaName.setScaleX(-1.0);
    }
};/**
 * Created with JetBrains WebStorm.
 * User: qidea
 * Date: 5/21/13
 * Time: 7:44 下午
 * To change this template use File | Settings | File Templates.
 */
var SkillBookBuff=function(){

};
SkillBookBuff.prototype.onDidLoadFromCCB=function(){

};
SkillBookBuff.prototype.setData=function(cfg){

}

/**
 * Created with JetBrains WebStorm.
 * User: qidea
 * Date: 2/21/13
 * Time: 10:19 下午
 * To change this template use File | Settings | File Templates.
 */
var FightAnimBlood=function(){
    //cc.log("FightAnimBlood constructor");
};
FightAnimBlood.prototype.onUpdate=function(){
    //cc.log("FightAnimBlood onUpdate");
};
FightAnimBlood.prototype.onDidLoadFromCCB=function(){
    //cc.log("FightAnimBlood onDidLOadFromCCB");
};
FightAnimBlood.prototype.setBlood=function(number,isRight){
    var me = this;
    if(isRight){
        me.bloodNumber.setScaleX(-1.0);
    }
    if(number>=0){
        number="+"+number;
    }
    me.bloodNumber.setString(number);
};/**
 * Created with JetBrains WebStorm.
 * User: qidea
 * Date: 2/21/13
 * Time: 10:19 下午
 * To change this template use File | Settings | File Templates.
 */
var FightAnimHeroComp=function(){
    //cc.log("FightAnimHeroComp constructor");
};
FightAnimHeroComp.prototype.onUpdate=function(){
    //cc.log("FightAnimHeroComp onUpdate");
};
FightAnimHeroComp.prototype.onDidLoadFromCCB=function(){
    //cc.log("FightAnimHeroComp onDidLOadFromCCB");
};
//control a CCLayer
FightAnimFightLevel = function () {
    //cc.log("FightAnimFightLevel constructor");
    var me = this;
    me.timeLine = 0;
    me.actionSequences = [];
    me.heroListWraperOffsetY = 0;
    if (sys.platform != "browser") {
        QideaCC.setIdleTimerDisabled(true);
    }
};
FightAnimFightLevel.prototype.onUpdate = function () {
    var me = this;
    cc.log("FightAnimFightLevel onUpdate...");
};

FightAnimFightLevel.prototype.onDidLoadFromCCB = function () {
    var me = this;
    var report = null;
//
//    // Schedule callback
//    this.rootNode.onUpdate = function(dt) {
//        this.controller.onUpdate();
//    };
//    this.rootNode.schedule(this.rootNode.onUpdate);

    if (typeof Qidea.taskResult == "object") {
        report = Qidea.taskResult.dataObj.data.report;
        if (typeof Qidea.taskResult.pageFrom != "undefined" && Qidea.taskResult.pageFrom == "endlessWar") {
            //a CCSprite
            me.levelBg.setTexture(cc.Sprite.create("image/var/active/endlessWar/ttt_bg4.png").getTexture());
        }
    }
    if (report) {
        //设置战斗双方玩家名称
        if (typeof report.king != "undefined") {
            if (typeof report.king.left == "string") {
                me.playerNameLeft.setString(report.king.left);
            }
            if (typeof report.king.right == "string") {
                me.playerNameRight.setString(report.king.right);
            }
        }
        //初始化阵法名称
        if (typeof report.prepare != "undefined" && typeof report.prepare.fromMatrix != "undefined" && typeof report.prepare.fromMatrix.icon != "undefined") {
            me.zhengFaLeft.controller.setSkillNameTexture(report.prepare.fromMatrix.icon);
            cc.log(report.prepare.fromMatrix.icon);
            me.zhengFaLeft.setVisible(true);
        } else {
            me.zhengFaLeft.setVisible(false);
        }
        if (typeof report.prepare != "undefined" && typeof report.prepare.toMatrix != "undefined" && typeof report.prepare.toMatrix.icon != "undefined") {
            me.zhengFaRight.controller.setSkillNameTexture(report.prepare.toMatrix.icon, true);
            cc.log(report.prepare.toMatrix.icon);
            me.zhengFaRight.setVisible(true);
        } else {
            me.zhengFaRight.setVisible(false);
        }
        if (Qidea.taskResult.dataObj.data.report.skipSec >= 0) {
            me.playAction(function () {
                var secToShowBtn = Qidea.taskResult.dataObj.data.report.skipSec;
                var actionShowSkipBtn = cc.Sequence.create(cc.DelayTime.create(secToShowBtn), cc.CallFunc.create(function () {
                    me.addCloseBtn();
                }));
                me.rootNode.runAction(actionShowSkipBtn);
            }, 0);
        } else if (Qidea.taskResult.dataObj.data["@class"] == "opera") {
        }
//            me.playAction(function () {
//                var actionShowSkipBtn = cc.Sequence.create(cc.DelayTime.create(0), cc.CallFunc.create(function () {
//                    me.addNewPlayerCloseBtn();
//                }));
//                me.rootNode.runAction(actionShowSkipBtn);
//            }, 0);
//        me.playAction(function () {
//            //cc.log("sleep 1000 ms");
//        }, 500);
        me.initHeroHeader(report);
        me.startAttack(report);
        me.playAction(function () {
            //cc.log("goBackToTask");
            me.goBackToTask();
        }, 1800);

        var createParams = "";
        for (var i = 0; i < me.actionSequences.length; i++) {
            createParams += "me.actionSequences[" + i + "],";
        }
        createParams = createParams.substr(0, createParams.length - 1);
        eval("me.rootNode.runAction(cc.Sequence.create(" + createParams + "));");
    }
    if (typeof CONFIG != "undefined" && typeof CONFIG.settingInfo != "undefined" && CONFIG.settingInfo.bgMusic) {
        CocosEval('cc.AudioEngine.getInstance().playMusic("music/bg_battle1.mp3", true)');
    }
    var hidePgCallBack = function () {
        cc.log("show cocos view ");
        CocosEval('typeof Pg!="undefined" && typeof Pg.showGL=="function" && Pg.showGL();');

    };

    me.rootNode.scheduleOnce(hidePgCallBack, 0);
    //add for Android Test by Colin3dmax
    cc.log("finish ccb...");
    if (typeof Pg != "undefined" && typeof Pg.showGL == "function") {
        cc.log("Pg.showGL();");
        Pg.showGL();
    }
    cc.log("after Pg.showGL();");

};


/**
 * 获得卡牌颜色
 * @param preClass
 * @param tool
 */
FightAnimFightLevel.prototype.changeHeroIcon = function (node, hero) {
    var me = this;
    var headSyleArray = ["red", "white", "green", "blue", "purple", "yellow"];
    var className = headSyleArray[0];
    if (hero && hero.star) {
        className = headSyleArray[hero.star];
    }
    var headImageTexture = cc.TextureCache.getInstance().addImage("image/framework/control/head_box_" + className + ".png");
    node.setTexture(headImageTexture);
};

FightAnimFightLevel.prototype.initHeroHeader = function (report) {
    var me = this;
    var maxPos = 0;//统计双方最大坐标位置
    for (var i = 0; i < 8; i++) {
        this["heroIconRight0" + (i + 1)].setVisible(false);
        this["heroIconLeft0" + (i + 1)].setVisible(false);
    }
    for (var i = 0; i < 8; i++) {
        //翻转敌方武将头像名称
        this["heroIconRight0" + (i + 1)].controller.heroName.setScaleX(-0.5);
        //翻转血条文字
        this["heroIconRight0" + (i + 1)].controller.bloodValue.setScaleX(-0.45);
        //设置敌方阵营颜色
        this["heroIconRight0" + (i + 1)].controller.heroBoxBlue.setVisible(false);
        this["heroIconRight0" + (i + 1)].controller.heroBoxRed.setVisible(true);
        this["heroIconRight0" + (i + 1)].controller.fightBloodBlue.setVisible(false);
        this["heroIconRight0" + (i + 1)].controller.fightBloodRed.setVisible(true);
        //设置我方阵营颜色
        this["heroIconLeft0" + (i + 1)].controller.heroBoxBlue.setVisible(false);
        this["heroIconLeft0" + (i + 1)].controller.heroBoxRed.setVisible(true);
        this["heroIconLeft0" + (i + 1)].controller.fightBloodBlue.setVisible(true);
        this["heroIconLeft0" + (i + 1)].controller.fightBloodRed.setVisible(false);
        //设置武将名称
        if (report) {
            if (i < report.attackers.length) {
                var hero = report.attackers[i];
                //hero=$db.getCardById(hero.id);
                //设置头像
                var headImageTexture = cc.TextureCache.getInstance().addImage("image/var/120/" + hero.icon + ".png");
                this["heroIconLeft0" + (hero.pos)].controller.heroHeader.setTexture(headImageTexture);
                me.changeHeroIcon(this["heroIconLeft0" + (hero.pos)].controller.heroBoxRed, hero);
                this["heroIconLeft0" + (hero.pos)].controller.heroName.setString(hero.name);
                this["heroIconLeft0" + (hero.pos)].controller.bloodValue.setString(hero.blood);
                this["heroIconLeft0" + (hero.pos)].setVisible(true);
                if (hero.pos > maxPos)maxPos = hero.pos;
            }

            if (i < report.defenders.length) {
                var hero = report.defenders[i];
                if (hero && hero.icon) {
                    //需要配置icon
                    var headImageTexture = cc.TextureCache.getInstance().addImage("image/var/120/" + hero.icon + ".png");
                    this["heroIconRight0" + (hero.pos)].controller.heroHeader.setTexture(headImageTexture);
                    this["heroIconRight0" + (hero.pos)].controller.heroName.setString(hero.name);
                    this["heroIconRight0" + (hero.pos)].controller.bloodValue.setString(hero.blood);
                    me.changeHeroIcon(this["heroIconRight0" + (hero.pos)].controller.heroBoxRed, hero);
                }
                this["heroIconRight0" + (hero.pos)].setVisible(true);
                if (hero.pos > maxPos)maxPos = hero.pos;
            }

        }
    }

    var rowCount = (maxPos / 2).toFixed(0);
    me.heroListWraperOffsetY = -1.0 * (4 - rowCount) * 50;
    //设定武将整体偏移量
    me.heroListWrapper.setPositionY(me.heroListWraperOffsetY);
};

FightAnimFightLevel.prototype.addCloseBtn = function () {
    var me = this;
    var size = cc.Director.getInstance().getWinSize();
    var closeItem = cc.MenuItemImage.create(
        "image/framework/control/battleSkip.png",
        "image/framework/control/battleSkip_dianji.png",
        me.goBackToTask, me);
    closeItem.setAnchorPoint(cc.p(0.5, 0.5));
    closeItem.setScaleX(0.5);
    closeItem.setScaleY(0.5);
    var menu = cc.Menu.create(closeItem);
    menu.setPosition(cc.p(0, 0));
    me.rootNode.addChild(menu, 1);
    closeItem.setPosition(cc.p(size.width / 2, 43));
};


FightAnimFightLevel.prototype.addNewPlayerCloseBtn = function () {
    var me = this;
    var size = cc.Director.getInstance().getWinSize();
    var closeItem = cc.MenuItemImage.create(
        "image/framework/control/newplayerskip.png",
        "image/framework/control/newplayerskip_dianji.png",
        me.goBackToTask, me);
    closeItem.setAnchorPoint(cc.p(0.5, 0.5));
    closeItem.setScaleX(0.5);
    closeItem.setScaleY(0.5);
    var menu = cc.Menu.create(closeItem);
    menu.setPosition(cc.p(0, 0));
    me.rootNode.addChild(menu, 1);
    closeItem.setPosition(cc.p(size.width / 2, 43));
};

FightAnimFightLevel.prototype.timeLineGo = function (time) {
    this.timeLine += time;
};
FightAnimFightLevel.prototype.getDelayTime = function (delayTime) {
    return 1.0 * (this.timeLine + delayTime) / 1000;
};
FightAnimFightLevel.prototype.startAttack = function (report) {
    var me = this;
    for (var i = 0; i < report.rounds.length; i++) {
        var round = report.rounds[i];
        if (typeof round.seq != "undefined") {
            var roundCfg = {number: round.seq};
            me.playAction(function (data) {
                me.attackRound.controller.setNumber(data.number);
            }, 100, null, roundCfg);
        }
        for (var j = 0; j < round.beats.length; j++) {
            var beat = round.beats[j];
            me.fight(beat);
        }
    }

};

FightAnimFightLevel.prototype.fight = function (beat) {
    var me = this;
    //test by Colin3dmax for bookBuffGroup
    /*
     if(beat.type=="bookBuff"){
     var list = [];
     var beat1 = _.clone(beat);
     beat.pos=1;
     list.push(beat1);

     var beat2 = _.clone(beat);
     beat.pos=3;
     list.push(beat2);

     var beat3 = _.clone(beat);
     beat.pos=5;
     list.push(beat3);

     beat = {type:"bookBuffGroup",list:list};
     }
     */

    if (beat.type == "buff") {
        //展示转生技能
        me.showRebornBuff(beat);
    } else if (beat.type == "bookBuffGroup") {
        me.showBookBuffGroup(beat);
    } else if (beat.type == "bookBuff") {
        me.showBookBuff(beat);

    } else if (beat.type == "talk") {
        me.showTalk(beat);
    } else if (beat.type == "buffBlood") {
        me.showBlood(beat);
    } else {
        //展示普通技能
        me.showNormalSkill(beat);
    }

};
FightAnimFightLevel.prototype.showBlood = function (beat) {
    var me = this;

    me.showSkillName(beat, "from", {pos: beat.pos}, 200);
    me.showAttack(beat, 800);
    me.showAttackEffect(beat, 0);
    me.playAction(function () {
        for (var i = 0; i < beat.to.length; i++) {
            var index = i;
            var toHero = beat.to[i];
            var heroTarget = toHero.side == "left" ? "Left" : "Right";
            var isRight = toHero.side == "left" ? false : true;
            var heroIcon = me["heroIcon" + heroTarget + "0" + toHero.pos];
            var skillCfg = FightAnimFightLevel.skillList[beat.res1];
            var node = cc.BuilderReader.load("image/interface/" + skillCfg.to.name + ".ccbi");
            me.rootNode.addChild(node);
            node.controller.setBlood(-1.0 * toHero.reduce, isRight);
            //fixed html not scale bug
            if (sys.platform == "browser") {
                node.setScale(cc.p(0.5, 0.5));
            }
            me.playFightEffectMusic(beat);
            if (beat.side == "left") {
                node.setPosition(heroIcon.getPositionX() - skillCfg.to.offset.x, heroIcon.getPositionY() + me.heroListWraperOffsetY + skillCfg.to.offset.y);
            } else {
                node.setPosition(heroIcon.getPositionX() + skillCfg.to.offset.x, heroIcon.getPositionY() + me.heroListWraperOffsetY + skillCfg.to.offset.y);
            }

            node.cfg = {heroIcon: heroIcon, toHero: toHero};
            var actionObj = cc.Sequence.create(cc.DelayTime.create(1.2), cc.CallFunc.create(function (selector) {
                cc.log("msg remove");
                var bloodPrecent = 1.0 * selector.cfg.toHero.cur / selector.cfg.toHero.total;
                selector.cfg.heroIcon.controller.bloodValue.setString(selector.cfg.toHero.cur);
                //血条加血动画
                selector.cfg.heroIcon.controller.fightBloodRed.setScaleX(0.5 * bloodPrecent);
                selector.cfg.heroIcon.controller.fightBloodBlue.setScaleX(0.5 * bloodPrecent);

                me.rootNode.removeChild(node, true);
            }, this));
            node.runAction(actionObj);
        }

    }, 200);
};

FightAnimFightLevel.prototype.showTalk = function (beat) {
    var me = this;
    me.playAction(function () {
        var heroTarget = beat.side == "left" ? "Left" : "Right";
        var isRight = beat.side == "left" ? false : true;
        var heroIcon = me["heroIcon" + heroTarget + "0" + beat.pos];
        var node = cc.BuilderReader.load("image/interface/FightHeroTalk.ccbi");
        var offset = {x: 33, y: 10 };
        me.rootNode.addChild(node);
        node.controller.setMsg(beat.msg, isRight);
        //fixed html not scale bug
        if (sys.platform == "browser") {
            node.setScale(cc.p(0.5, 0.5));
        }

        if (beat.side != "left") {
            node.setPosition(heroIcon.getPositionX() - offset.x, heroIcon.getPositionY() + me.heroListWraperOffsetY + offset.y);
        } else {
            node.setPosition(heroIcon.getPositionX() + offset.x, heroIcon.getPositionY() + me.heroListWraperOffsetY + offset.y);
        }

        var actionObj = cc.Sequence.create(cc.DelayTime.create(1.8), cc.CallFunc.create(function () {
            cc.log("msg remove");
            me.rootNode.removeChild(node, true);
        }));
        node.runAction(actionObj);
    }, 800);
    me.playAction(function () {
        //延时 1800ms
    }, 1800);
};

FightAnimFightLevel.prototype.showBookBuffGroup = function (beat) {
    var me = this;
    //显示RebornBuf效果
    me.getBookBuffGroup(beat, 500);
};
FightAnimFightLevel.prototype.showBookBuff = function (beat) {
    var me = this;
    //转生技能名称
    //me.showSkillName(beat,"from",{pos:beat.pos},0);
    //显示RebornBuf效果
    me.getBookBuff(beat, 500);
};
FightAnimFightLevel.prototype.showRebornBuff = function (beat) {
    var me = this;
    //转生技能名称
    me.showSkillName(beat, "from", {pos: beat.pos}, 0);
    //显示RebornBuf效果
    me.getRebornBuff(beat, 500);
};

/*

 {"pos":1,"side":"left","type":"buff","skill":"500006","skillName":"破军★群Ⅵ","res1":"skill10","res2":"zhuanSheng_debuff_def","to":[{"pos":1,"side":"left","to":"12033"}]}

 */
FightAnimFightLevel.prototype.showNormalSkill = function (beat) {
    var me = this;
    //by Colin3dmax for test
    //beat.res1="skill25";
    if (beat.side == "left") {
        //我方攻击
        me.showSkillName(beat, "from", {pos: beat.pos});
        me.showAttack(beat, 800);
        me.showAttackEffect(beat, 0);
        me.showSkill(beat, "from", {pos: beat.pos});
        me.showSkill(beat, "to", beat.to);
        for (var i = 0; i < beat.to.length; i++) {
            var toHero = beat.to[i];
            me.attacked(beat, toHero, 0);
        }

    } else {
        //敌方攻击
        me.showSkillName(beat, "from", {pos: beat.pos});
        me.showAttack(beat, 800);
        me.showAttackEffect(beat, 0);
        me.showSkill(beat, "from", {pos: beat.pos});
        me.showSkill(beat, "to", beat.to);
        for (var i = 0; i < beat.to.length; i++) {
            var toHero = beat.to[i];
            me.attacked(beat, toHero, 0);
        }
    }
};

FightAnimFightLevel.prototype.playAction = function (node, actionName, delayTime, data) {
    var me = this;
    var delay = me.getDelayTime(delayTime);
    var delayAfter = 0;

    if (typeof node == "function") {
        delay = me.getDelayTime(actionName);
        if (typeof delayTime == "number" && delayTime > 0) {
            //cc.log("delayAfter:" + delayTime);
            delayAfter = me.getDelayTime(delayTime);
        }
    }
    var actionObj = cc.Sequence.create(cc.DelayTime.create(delay), cc.CallFunc.create(function (selector) {
        //cc.log("actionObj.......run");
        if (typeof node == "function") {
            node(data);

        } else {
            node.animationManager.runAnimationsForSequenceNamed(actionName);
        }
    }, cc.DelayTime.create(delayAfter), data));

    me.actionSequences.push(actionObj);
};
FightAnimFightLevel.prototype.getAttackObj = function (beat) {
    var me = this;
    var attackObj = {number: 1, offset: {x: 40, y: 30}};
    switch (beat.hit) {
        case "local":
            attackObj.number = 1;
            attackObj.offset.x = 40;
            attackObj.offset.y = 30;
            break;

        case "remote":
            attackObj.number = 3;
            attackObj.offset.x = 40;
            attackObj.offset.y = 30;
            break;

        default:
            break;
    }
    cc.log(beat.hit);
    return attackObj;
};
FightAnimFightLevel.prototype.showAttack = function (beat, delayTime) {
    var me = this;
    var heroTarget = beat.side == "left" ? "Left" : "Right";
    var attackName = null;
    switch (beat.hit) {
        case "local":
            attackName = "attackLocal";
            break;
        case "remote":
            attackName = "attackRemote";
            break;

        default:
            attackName = "attackLocal";
            break;
    }
    me.playAction(this["heroIcon" + heroTarget + "0" + beat.pos], attackName, delayTime);
    //出击音效
    if (typeof CONFIG != "undefined" && typeof CONFIG.settingInfo != "undefined" && CONFIG.settingInfo.effectMusic) {
        me.playAction(function () {
            //播放音效
            QUtil.playEffect("music/effect_attack.mp3");
        }, 0);
    }
};
FightAnimFightLevel.prototype.showAttackEffect = function (beat, delayTime) {
    var me = this;
    var attackObj = me.getAttackObj(beat);

    me.playAction(function () {
        var heroTarget = beat.side == "left" ? "Left" : "Right";
        var node = cc.BuilderReader.load("image/interface/AttackEffect0" + attackObj.number + ".ccbi");
        me.rootNode.addChild(node);

        var heroIcon = me["heroIcon" + heroTarget + "0" + beat.pos];
        if (beat.side != "left") {
            node.getChildren()[0].setScaleX(-1);
            node.setPosition(heroIcon.getPositionX() - attackObj.offset.x, heroIcon.getPositionY() + me.heroListWraperOffsetY + attackObj.offset.y);
        } else {
            node.setPosition(heroIcon.getPositionX() + attackObj.offset.x, heroIcon.getPositionY() + me.heroListWraperOffsetY + attackObj.offset.y);
        }

        var actionObj = cc.Sequence.create(cc.DelayTime.create(0.5), cc.CallFunc.create(function () {
            //cc.log("removeChild");
            me.rootNode.removeChild(node, true);
        }));
        node.runAction(actionObj);

    }, delayTime);
};
//英雄挂掉
FightAnimFightLevel.prototype.die = function (beat, toHero, delayTime) {
    var me = this;
    me.playAction(function () {
        var heroTarget = beat.side != "left" ? "Left" : "Right";
        var node = cc.BuilderReader.load(("image/interface/DieLabel.ccbi"));
        var heroObj = null;
        me.rootNode.addChild(node);
        if (beat.side == "left") {
            heroObj = me["heroIcon" + heroTarget + "0" + toHero.pos];
            node.setPosition(me["heroIcon" + heroTarget + "0" + toHero.pos].getPositionX() - 65, me["heroIcon" + heroTarget + "0" + toHero.pos].getPositionY() + me.heroListWraperOffsetY - 60);
        } else {
            heroObj = me["heroIcon" + heroTarget + "0" + toHero.pos];
            node.setPosition(me["heroIcon" + heroTarget + "0" + toHero.pos].getPositionX() - 5, me["heroIcon" + heroTarget + "0" + toHero.pos].getPositionY() + me.heroListWraperOffsetY - 60);
        }
        //隐藏BookBuff技能图标
        if (heroObj.getChildren()[0].getChildren().length >= 4) {
            cc.log("remove flag");
            for (var i = heroObj.getChildren()[0].getChildren().length - 1; i >= 3; i--) {
                heroObj.getChildren()[0].getChildren()[i].setVisible(false);
                heroObj.getChildren()[0].removeChild(heroObj.getChildren()[0].getChildren()[i], true);
            }
            me.rootNode.removeChild(heroObj, true);
        }
    }, 100);
};

FightAnimFightLevel.skillList = {
    skill01: {
        effectMusic: "effect_arrow_fire",
        type: "both",
        from: {name: "AttackSkill01_From", offset: {x: 70, y: 70}, delay: 400, ms: 500},
        to: {name: "AttackSkill01_To", offset: {x: -50, y: 60}, delay: 500, ms: 300}
    },
    skill02: {
        effectMusic: "effect_wood",
        type: "both",
        from: {name: "AttackSkill02_From", offset: {x: 60, y: 50}, delay: 400, ms: 330},
        to: {name: "AttackSkill02_To", offset: {x: -30, y: 30}, delay: 400, ms: 420}
    },
    skill03: {
        effectMusic: "effect_trap_clip",
        type: "to",
        to: {name: "AttackSkill03", offset: {x: -25, y: 30}, delay: 200, ms: 700}
    },
    skill04: {
        effectMusic: "effect_wood",
        type: "to",
        to: {name: "AttackSkill04", offset: {x: -30, y: 80}, delay: 200, ms: 700}
    },
    skill05: {
        type: "to",
        to: {name: "AttackSkill05", offset: {x: -32, y: 32}, delay: 200, ms: 167}
    },
    skill06: {
        effectMusic: "effect_wind",
        type: "to",
        to: {name: "AttackSkill06", offset: {x: -50, y: 40}, delay: 200, ms: 600}
    },
    skill07: {
        effectMusic: "effect_stone",
        type: "to",
        to: {name: "AttackSkill07", offset: {x: -30, y: 70}, delay: 200, ms: 805}
    },
    skill08: {
        effectMusic: "effect_ice",
        type: "to",
        to: {name: "AttackSkill08", offset: {x: -30, y: 30}, delay: 200, ms: 500}
    },
    skill09: {
        effectMusic: "effect_fire",
        type: "to",
        to: {name: "AttackSkill09", offset: {x: -30, y: 30}, delay: 200, ms: 570}
    },
    skill10: {
        effectMusic: "effect_reborn_debuff",
        type: "to",
        to: {name: "AttackSkill10", offset: {x: -30, y: 30}, delay: 400, ms: 1000}
    },
    skill11: {
        effectMusic: "effect_buff",
        type: "to",
        to: {name: "AttackSkill11", offset: {x: -30, y: 30}, delay: 400, ms: 1000}
    },
    skill12: {
        effectMusic: "effect_buff",
        type: "from",
        from: {name: "AttackEffect04", offset: {x: 0, y: 0}, delay: 400, ms: 20000}
    },
    skill13: {
        effectMusic: "effect_thunder",
        type: "to",
        to: {name: "AttackSkill13", offset: {x: -30, y: 30}, delay: 200, ms: 770}
    },
    skill14: {
        effectMusic: "effect_weapon_spear",
        type: "to",
        to: {name: "AttackSkill14", offset: {x: -50, y: 90}, delay: 200, ms: 600}
    },
    skill15: {
        effectMusic: "effect_weapon_axe",
        type: "to",
        to: {name: "AttackSkill15", offset: {x: -50, y: 40}, delay: 200, ms: 800}
    },
    skill16: {
        effectMusic: "effect_trap_sword",
        type: "to",
        to: {name: "AttackSkill16", offset: {x: -25, y: 35}, delay: 200, ms: 700}
    },
    skill17: {
        effectMusic: "effect_trap_bamboo",
        type: "to",
        to: {name: "AttackSkill17", offset: {x: -30, y: 30}, delay: 200, ms: 700}
    },
    skill18: {
        effectMusic: "effect_footman",
        type: "to",
        to: {name: "AttackSkill18", offset: {x: -30, y: 30}, delay: 200, ms: 800}
    },
    skill19: {
        effectMusic: "effect_footman",
        type: "to",
        to: {name: "AttackSkill19", offset: {x: -30, y: 30}, delay: 200, ms: 600}
    },
    skill21: {
        effectMusic: "effect_heal",
        type: "to",
        to: {name: "AttackSkill21", offset: {x: -30, y: 30}, delay: 200, ms: 1100}
    },
    skill23: {
        effectMusic: "effect_weapon_spear",
        type: "to",
        to: {name: "AttackSkill23", offset: {x: -30, y: 30}, delay: 400, ms: 1000}
    },
    skill24: {
        effectMusic: "effect_weapon_spear",
        type: "to",
        to: {name: "AttackSkill24", offset: {x: -30, y: 30}, delay: 400, ms: 1000}
    },
    skill25: {
        effectMusic: "effect_weapon_hammer",
        type: "to",
        to: {name: "AttackSkill25", offset: {x: -65, y: 30}, delay: 400, ms: 1000}
    },
    effect02: {
        type: "to",
        to: {name: "AttackEffect02", offset: {x: -30, y: 30}, delay: 200, ms: 1100}
    },
    skillWeapon: {
        effectMusicList: {
            bian: "effect_weapon_hammer",
            bishou: "effect_weapon_spear",
            chui: "effect_weapon_hammer",
            dao: "effect_weapon_spear",
            guzi: "effect_weapon_spear",
            ji: "effect_weapon_spear",
            mao: "effect_weapon_spear",
            qiang: "effect_weapon_spear"
        },
        type: "to",
        to: {name: "AttackSkillWeapon", offset: {x: 0, y: 0}, delay: 200, ms: 500}
    }

};

FightAnimFightLevel.prototype.getShowSkillHeroTargetList = function (actionCfg) {
    var me = this;
    var heroTargetList = [];
    for (var i = 0; i < actionCfg.heroList.length; i++) {
        var heroTarget = null;
        if (actionCfg.beat.side == "left") {
            if (actionCfg.type == "from") {
                heroTarget = me["heroIconLeft0" + actionCfg.heroList[i].pos];
            } else {
                heroTarget = me["heroIconRight0" + actionCfg.heroList[i].pos];
            }
        } else {
            if (actionCfg.type == "from") {
                heroTarget = me["heroIconRight0" + actionCfg.heroList[i].pos];
            } else {
                heroTarget = me["heroIconLeft0" + actionCfg.heroList[i].pos];
            }
        }
        heroTargetList.push(heroTarget);
    }
    return heroTargetList;
};
//战斗技能展示
FightAnimFightLevel.prototype.showSkillName = function (beat, type, heroList, delayTime) {
    var me = this;
    var flag = beat.side == "left" ? 1.0 : -1.0;
    var delayTime = 0 || delayTime;
    if (beat.type != "simple" && type == "from") {
        me.playAction(function () {
            //cc.log("show skill name");
            var actionCfg = {beat: beat, type: type};
            if (!(heroList instanceof Array)) {
                actionCfg.heroList = [heroList];
            } else {
                actionCfg.heroList = heroList;
            }
            var heroTarget = me.getShowSkillHeroTargetList(actionCfg)[0];
            //cc.log("add actionSkillName ------------------");
            var skillName = cc.BuilderReader.load("image/interface/AttackSkillName.ccbi");
            //cc.log("add actionSkillName ------------------done");
            //跟换技能名称图片
            skillName.controller.setSkillName(beat.skillName);
            skillName.controller.setSkillNameTextureBySkillId(beat.skill);
            me.rootNode.addChild(skillName);
            skillName.setPosition(heroTarget.getPositionX() + flag * 40, heroTarget.getPositionY() + me.heroListWraperOffsetY + 30);

            var actionSkillNameSequence = cc.Sequence.create(cc.DelayTime.create(1400 / 1000), cc.CallFunc.create(function (selector) {
                //cc.log("removeChild actionSkillName-------");
                me.rootNode.removeChild(selector, true);
            }));
            skillName.runAction(actionSkillNameSequence);
        }, 500, 100 + delayTime);
    }
};
//设定技能大小
FightAnimFightLevel.prototype.updateSkillScale = function (node, beat) {

    var scale = beat.scale != "undefined" ? beat.scale : 1.0;
    node.setScaleX(node.getScaleX() * scale);
    node.setScaleY(node.getScaleY() * scale);
}
FightAnimFightLevel.prototype.showSkill = function (beat, type, heroList) {
    var me = this;
    var flag = beat.side == "left" ? 1.0 : -1.0;
    if (beat.type != "simple") {
        var skillCfg = FightAnimFightLevel.skillList["skill01"];
        if (beat.res1) {
            if (beat.res1 == "skill-wenpan") {
                beat.res1 = "skillWeapon";
            }

            skillCfg = FightAnimFightLevel.skillList[beat.res1];
            if (beat.res2) {
                skillCfg.weaponPic = beat.res2;
            }
        }

        if (typeof skillCfg != "undefined" && (type in skillCfg)) {
            var skillObj = skillCfg[type];
            me.playAction(function () {

                //cc.log("show skill");
                var actionCfg = {beat: beat, type: type};
                if (!(heroList instanceof Array)) {
                    actionCfg.heroList = [heroList];
                } else {
                    actionCfg.heroList = heroList;
                }

                var heroTargetList = me.getShowSkillHeroTargetList(actionCfg);
                for (var i = 0; i < heroTargetList.length; i++) {
                    var heroTarget = heroTargetList[i];
                    var skill = cc.BuilderReader.load("image/interface/" + skillObj.name + ".ccbi");
                    if (typeof skillCfg.weaponPic != "undefined") {
                        var weaponImageTexture = cc.TextureCache.getInstance().addImage("image/var/skill/skillWeapon/" + skillCfg.weaponPic + ".png");
                        skill.getChildren()[0].getChildren()[0].setTexture(weaponImageTexture);
                    }
                    if (actionCfg.beat.side == "right") {
                        skill.setScaleX(-1);
                    }
                    me.updateSkillScale(skill, beat);
                    me.rootNode.addChild(skill);
                    skill.setPosition(heroTarget.getPositionX() + flag * skillObj.offset.x, heroTarget.getPositionY() + me.heroListWraperOffsetY + skillObj.offset.y);

                    var actionSkillSequence = cc.Sequence.create(cc.DelayTime.create(skillObj.ms / 1000), cc.CallFunc.create(function (selector) {

                        //cc.log("removeChild actionSkill");
                        me.rootNode.removeChild(selector, true);
                    }));
                    skill.runAction(actionSkillSequence);
                }

            }, skillObj.delay);
        }
    }

};
//获得BookBuff动画
FightAnimFightLevel.prototype.getBookBuffGroup = function (beatGroup, delayTime) {
    var me = this;
    me.playAction(function () {
        _.each(beatGroup.list, function (beat) {
            var skillCfg = FightAnimFightLevel.skillList[beat.res1];
            if (beat.skill) {
                skillCfg.bookNamePic = beat.skill;
            }
            if (beat.attr) {
                skillCfg.iconFlagPic = beat.attr;
            }
            var toHero = beat;
            var heroTarget = toHero.side == "left" ? "Left" : "Right";
            var heroIconName = "heroIcon" + heroTarget + "0" + toHero.pos;
            var heroIcon = me[heroIconName];
            var flag = toHero.side == "left" ? 1.0 : -1.0;
            var skill = cc.BuilderReader.load("image/interface/" + skillCfg.from.name + ".ccbi");
            cc.log("load:-->image/var/skill/skillIcon/" + skillCfg.iconFlagPic + ".png");
            var iconFlagImageTexture = cc.TextureCache.getInstance().addImage("image/var/skill/skillIcon/" + skillCfg.iconFlagPic + ".png");
            skill.controller.iconFlag.setTexture(iconFlagImageTexture);
            cc.log("load done:-->image/var/skill/skillIcon/" + skillCfg.iconFlagPic + ".png");
            if (typeof skillCfg.bookNamePic != "undefined") {
                cc.log("load:-->image/var/skill/skillName/" + skillCfg.bookNamePic + ".png");
                var bookNameImageTexture = cc.TextureCache.getInstance().addImage("image/var/skill/skillName/" + skillCfg.bookNamePic + ".png");
                skill.controller.skillName.setTexture(bookNameImageTexture);

                cc.log("load-done:-->image/var/skill/skillName/" + skillCfg.bookNamePic + ".png");
            }
            if (toHero.side == "left") {
                skill.setPosition(beat.sub * 10, 0);
            } else {
                skill.setScaleX(-1.0 * skill.getScaleX());
                skill.setPosition(heroIcon.getContentSize().width - (beat.sub) * 10, 0);
            }
            //me.updateSkillScale(skill,beat);
            heroIcon.setZOrder(2);
            //skill.setPosition(heroIcon.getPositionX() + flag *  skillCfg.from.offset.x+beat.sub*28, heroIcon.getPositionY()+me.heroListWraperOffsetY +  skillCfg.from.offset.y);
            me.playFightEffectMusic(beat);
            heroIcon.controller.bookBufWrapper.addChild(skill);
            var actionSkillSequence = cc.Sequence.create(cc.DelayTime.create(skillCfg.from.ms / 1000), cc.CallFunc.create(function (selector) {
                cc.log("removeChild rebornBuff Skill");
                heroIcon.setZOrder(0);
            }));
            skill.runAction(actionSkillSequence);
        });
    }, delayTime);
    me.playAction(function () {
        //延时 1800ms
    }, 450);
};
//获得BookBuff动画
FightAnimFightLevel.prototype.getBookBuff = function (beat, delayTime) {
    var me = this;

    me.playAction(function () {
        var skillCfg = FightAnimFightLevel.skillList[beat.res1];
        if (beat.skill) {
            skillCfg.bookNamePic = beat.skill;
        }
        if (beat.attr) {
            skillCfg.iconFlagPic = beat.attr;
        }
        var toHero = beat;
        var heroTarget = toHero.side == "left" ? "Left" : "Right";
        var heroIconName = "heroIcon" + heroTarget + "0" + toHero.pos;
        var heroIcon = me[heroIconName];
        var flag = toHero.side == "left" ? 1.0 : -1.0;
        var skill = cc.BuilderReader.load("image/interface/" + skillCfg.from.name + ".ccbi");
        cc.log("load:-->image/var/skill/skillIcon/" + skillCfg.iconFlagPic + ".png");
        var iconFlagImageTexture = cc.TextureCache.getInstance().addImage("image/var/skill/skillIcon/" + skillCfg.iconFlagPic + ".png");
        skill.controller.iconFlag.setTexture(iconFlagImageTexture);
        cc.log("load done:-->image/var/skill/skillIcon/" + skillCfg.iconFlagPic + ".png");
        if (typeof skillCfg.bookNamePic != "undefined") {
            cc.log("load:-->image/var/skill/skillName/" + skillCfg.bookNamePic + ".png");
            var bookNameImageTexture = cc.TextureCache.getInstance().addImage("image/var/skill/skillName/" + skillCfg.bookNamePic + ".png");
            skill.controller.skillName.setTexture(bookNameImageTexture);

            cc.log("load-done:-->image/var/skill/skillName/" + skillCfg.bookNamePic + ".png");
        }
        if (toHero.side == "left") {
            skill.setPosition(beat.sub * 10, 0);
        } else {
            skill.setScaleX(-1.0 * skill.getScaleX());
            skill.setPosition(heroIcon.getContentSize().width - (beat.sub) * 10, 0);
        }
        //me.updateSkillScale(skill,beat);
        heroIcon.setZOrder(2);
        //skill.setPosition(heroIcon.getPositionX() + flag *  skillCfg.from.offset.x+beat.sub*28, heroIcon.getPositionY()+me.heroListWraperOffsetY +  skillCfg.from.offset.y);
        me.playFightEffectMusic(beat);
        heroIcon.controller.bookBufWrapper.addChild(skill);
        var actionSkillSequence = cc.Sequence.create(cc.DelayTime.create(skillCfg.from.ms / 1000), cc.CallFunc.create(function (selector) {
            cc.log("removeChild rebornBuff Skill");
            heroIcon.setZOrder(0);
        }));
        skill.runAction(actionSkillSequence);

    }, delayTime);
    me.playAction(function () {
        //延时 1800ms
    }, 50);
};
//获得转生Buff动画
FightAnimFightLevel.prototype.getRebornBuff = function (beat, delayTime) {
    var me = this;


    var skillCfg = FightAnimFightLevel.skillList[beat.res1];
    if (beat.res2) {
        skillCfg.buffNamePic = beat.res2;
    }
    me.playAction(function () {
        for (var i = 0; i < beat.to.length; i++) {
            var toHero = beat.to[i];
            var heroTarget = toHero.side == "left" ? "Left" : "Right";
            var heroIconName = "heroIcon" + heroTarget + "0" + toHero.pos;
            var heroIcon = me[heroIconName];
            var flag = toHero.side == "left" ? -1.0 : 1.0;
            var skill = cc.BuilderReader.load("image/interface/" + skillCfg.to.name + ".ccbi");
            skill.setPosition(heroIcon.getPositionX() + flag * skillCfg.to.offset.x, heroIcon.getPositionY() + me.heroListWraperOffsetY + skillCfg.to.offset.y);
            if (typeof skillCfg.buffNamePic != "undefined") {
                var weaponImageTexture = cc.TextureCache.getInstance().addImage("image/var/skill/zhuanSheng/" + skillCfg.buffNamePic + ".png");
                skill.getChildren()[1].setTexture(weaponImageTexture);
            }
            me.rootNode.addChild(skill);
            me.playFightEffectMusic(beat);
            var actionSkillSequence = cc.Sequence.create(cc.DelayTime.create(skillCfg.to.ms / 1000), cc.CallFunc.create(function (selector) {
                cc.log("removeChild rebornBuff Skill");
                me.rootNode.removeChild(selector, true);
            }));
            skill.runAction(actionSkillSequence);
        }
    }, delayTime + 100);
};
//受击动画
FightAnimFightLevel.prototype.attacked = function (beat, toHero, delayTime) {
    var me = this;
    var heroTarget = beat.side != "left" ? "Left" : "Right";
    var heroIconName = "heroIcon" + heroTarget + "0" + toHero.pos;
    var heroIcon = me[heroIconName];
    if (toHero.cur <= 0) {
        me.playAction(heroIcon, "attackedAndDie", delayTime);
        me.die(beat, toHero, delayTime);
    } else {
        me.playAction(heroIcon, "attacked", delayTime);
    }
    me.playAction(function () {
        //受击音效
        me.playFightEffectMusic(beat);
        var bloodPrecent = 1.0 * toHero.cur / toHero.total;
        if (beat.side == "left") {
            heroIcon.controller.fightBloodRed.setAnchorPoint({x: 1, y: 0});
            heroIcon.controller.fightBloodRed.setPositionX(55);
        }


        var lostBloodNumber = cc.BuilderReader.load(("image/interface/FightNumber.ccbi"));
        lostBloodNumber.controller.setNumber(-1 * toHero.reduce);
        me.rootNode.addChild(lostBloodNumber);


        if (beat.side == "left") {
            lostBloodNumber.setPosition(me["heroIcon" + heroTarget + "0" + toHero.pos].getPositionX() - 30, me["heroIcon" + heroTarget + "0" + toHero.pos].getPositionY() + me.heroListWraperOffsetY + 60);
        } else {
            lostBloodNumber.controller.fightNumberNode.setScaleX(-1);
            lostBloodNumber.controller.fightNumberIn.setScaleX(-1);
            lostBloodNumber.setPosition(me["heroIcon" + heroTarget + "0" + toHero.pos].getPositionX() + 30, me["heroIcon" + heroTarget + "0" + toHero.pos].getPositionY() + me.heroListWraperOffsetY + 60);
        }

        var actionRemoveLostBloodNumber = cc.Sequence.create(cc.DelayTime.create(1), cc.CallFunc.create(function () {
            //cc.log("removeChild actionRemoveLostBloodNumber");
            me.rootNode.removeChild(lostBloodNumber, true);
            heroIcon.controller.bloodValue.setString(toHero.cur);
            //血条少血动画
            heroIcon.controller.fightBloodRed.setScaleX(0.5 * bloodPrecent);
            heroIcon.controller.fightBloodBlue.setScaleX(0.5 * bloodPrecent);
        }));
        lostBloodNumber.runAction(actionRemoveLostBloodNumber);
    }, delayTime + 100);

};
FightAnimFightLevel.prototype.playFightEffectMusic = function (beat) {
    var me = this;
    if (typeof CONFIG != "undefined" && typeof CONFIG.settingInfo != "undefined" && CONFIG.settingInfo.effectMusic) {
        //播放音效
        if (beat.type != "simple" && typeof beat.res1 != "undefined" && typeof FightAnimFightLevel.skillList[beat.res1] != "undefined") {
            if (typeof FightAnimFightLevel.skillList[beat.res1].effectMusic == "string") {
                QUtil.playEffect("music/" + FightAnimFightLevel.skillList[beat.res1].effectMusic + ".mp3");
            } else if (typeof FightAnimFightLevel.skillList[beat.res1].effectMusicList != "undefined") {
                var skillCfg = FightAnimFightLevel.skillList[beat.res1];
                QUtil.playEffect("music/" + skillCfg.effectMusicList[beat.res2] + ".mp3");
            }
        } else {
            cc.log("attacked hit");
            //普通受击音效
            QUtil.playEffect("music/effect_hit.mp3");
        }
    }
};

//返回游戏战报
FightAnimFightLevel.prototype.goBackToTask = function () {
    var me = this;
    me.rootNode.getActionManager().removeAllActions();
    var scene = cc.BuilderReader.loadAsScene("image/interface/FightBlankLevel.ccbi");
    var runningScene = cc.Director.getInstance().getRunningScene();
    if (runningScene === null)
        cc.Director.getInstance().runWithScene(scene);
    else
        cc.Director.getInstance().replaceScene(scene);
    if (typeof CONFIG != "undefined" && typeof CONFIG.settingInfo != "undefined" && CONFIG.settingInfo.effectMusic) {
        if (!(typeof Qidea.taskResult.dataObj.pageFrom != "undefined" && Qidea.taskResult.dataObj.pageFrom == "NewPlayMovie")) {
            if (Qidea.taskResult.dataObj.data.report.isWin) {
                QUtil.playEffect("music/effect_battle_win.mp3");
            } else {
                QUtil.playEffect("music/effect_battle_lose.mp3");
            }
            if (typeof CONFIG != "undefined" && typeof CONFIG.settingInfo != "undefined" && CONFIG.settingInfo.bgMusic) {
                CocosEval('cc.AudioEngine.getInstance().playMusic("music/bg_main.mp3", true)');
            }
        }
    }


    if (typeof CONFIG != "undefined" && CONFIG.cocosEngine == "canvas") {
        if (typeof Qidea.taskResult.dataObj.pageFrom != "undefined" && Qidea.taskResult.dataObj.pageFrom == "NewPlayMovie") {
            Qidea.pageManager.playCocosAnim("image/interface/newplayer3.ccbi");
        } else if (typeof Qidea.endlessWarBattleRes == "object") {
            endlessWar.aniFinish();
        }
        else if (typeof Qidea.taskResult == "object") {
            Qidea.pageManager.gotoPage(PageTaskResult,
                {dataObj: Qidea.taskResult.dataObj,
                    result: Qidea.taskResult.dataObj.data,
                    lastHeros: Qidea.taskResult.lastHeros,
                    lastKingInfo: Qidea.taskResult.lastKingInfo,
                    cardCompList: Qidea.taskResult.cardCompList,
                    pageInstanceZone: Qidea.taskResult.pageInstanceZone});
        } else {
            Qidea.pageManager.gotoPage(PageHome);
        }
    } else if (typeof Qidea.taskResult.dataObj.pageFrom != "undefined" && Qidea.taskResult.dataObj.pageFrom == "NewPlayMovie") {
        Pg.eval('Qidea.pageManager.playCocosAnim("image/interface/newplayer3.ccbi");');
    } else {
        Pg.hideGL();
        forceGC();
        if (sys.platform != "browser") {
            QideaCC.setIdleTimerDisabled(false);
        }
    }
};/**
 * Created with JetBrains WebStorm.
 * User: qidea
 * Date: 2/21/13
 * Time: 10:19 下午
 * To change this template use File | Settings | File Templates.
 */
var FightNumber=function(){
    //cc.log("FightAnimHeroComp constructor");
    this.number=0;
};
FightNumber.prototype.onUpdate=function(){
    //cc.log("FightAnimHeroComp onUpdate");
};
FightNumber.prototype.onDidLoadFromCCB=function(){
    //cc.log("FightAnimHeroComp onDidLOadFromCCB");
};
FightNumber.prototype.setNumber=function(num){
    if(typeof num=="number"){
        this.number=num;
    }
    this.rootNode.controller.fightNumber.setString(this.number+"");
};var MainScene = function () {
};

// Create callback for button
MainScene.prototype.onPressButton = function () {
    // Rotate the label when the button is pressed
    this.helloLabel.runAction(cc.RotateBy.create(1, 360));
};

var FightAnim = cc.Layer.extend({
    isMouseDown: false,
    helloImg: null,
    helloLabel: null,
    circle: null,
    sprite: null,

    init: function () {
        var selfPointer = this;
        this._super();
        var size = cc.Director.getInstance().getWinSize();
        var node = cc.BuilderReader.load("image/interface/FightLevel.ccbi");

        this.addChild(node);
        this.setPosition(cc.p(0, 0));
        var t = cc.config.platform;
        if (t == 'browser') {
            this.setMouseEnabled(true);
            this.setKeyboardEnabled(true);
        } else if (t == 'desktop') {
            this.setMouseEnabled(true);
        } else if (t == 'mobile') {
            this.setTouchEnabled(true);
        }
        this.adjustSizeForWindow();
        window.addEventListener("resize", function (event) {
            selfPointer.adjustSizeForWindow();
        });
        return true;
    },

    adjustSizeForWindow: function () {
        var margin = document.documentElement.clientWidth - document.body.clientWidth;
        if (document.documentElement.clientWidth < cc.originalCanvasSize.width) {
            cc.canvas.width = cc.originalCanvasSize.width;
        } else {
            cc.canvas.width = document.documentElement.clientWidth - margin;
        }
        if (document.documentElement.clientHeight < cc.originalCanvasSize.height) {
            cc.canvas.height = cc.originalCanvasSize.height;
        } else {
            cc.canvas.height = document.documentElement.clientHeight - margin;
        }

        var xScale = cc.canvas.width / cc.originalCanvasSize.width;
        var yScale = cc.canvas.height / cc.originalCanvasSize.height;
        if (xScale > yScale) {
            xScale = yScale;
        }
        cc.canvas.width = cc.originalCanvasSize.width * xScale;
        cc.canvas.height = cc.originalCanvasSize.height * xScale;
        var parentDiv = document.getElementById("Cocos2dGameContainer");
        if (parentDiv) {
            parentDiv.style.width = cc.canvas.width + "px";
            parentDiv.style.height = cc.canvas.height + "px";
        }
        cc.renderContext.translate(0, cc.canvas.height);
        cc.renderContext.scale(xScale, xScale);
        cc.Director.getInstance().setContentScaleFactor(xScale);
    },
    // a selector callback
    menuCloseCallback: function (sender) {

        if (typeof Qidea.taskResult == "object") {
            Qidea.pageManager.gotoPage(PageTaskResult,
                {result: Qidea.taskResult.dataObj.data,
                    lastHeros: Qidea.taskResult.lastHeros,
                    lastKingInfo: Qidea.taskResult.lastKingInfo,
                    cardCompList: Qidea.taskResult.cardCompList,
                    pageInstanceZone: Qidea.taskResult.pageInstanceZone});
            Qidea.taskReuslt = null;
        } else {
            Qidea.pageManager.gotoPage(PageHome);
        }

    },
    onTouchesBegan: function (touches, event) {
        this.isMouseDown = true;
    },
    onTouchesMoved: function (touches, event) {
        if (this.isMouseDown) {
            if (touches) {
                //this.circle.setPosition(cc.p(touches[0].getLocation().x, touches[0].getLocation().y));
            }
        }
    },
    onTouchesEnded: function (touches, event) {
        this.isMouseDown = false;
    },
    onTouchesCancelled: function (touches, event) {
        cc.log("onTouchesCancelled");
    }
});

var FightAnimScene = cc.Scene.extend({
    onEnter: function () {
        this._super();
        var layer = new FightAnim();
        layer.config = this.config;
        layer.init();
        this.addChild(layer);
    }
});

var LoadingLayer = cc.Layer.extend({
    init: function () {
        var loading = cc.BuilderReader.load("image/interface/CLoading.ccbi");
        loading.setPosition({x: 100, y: 100});
        this.addChild(loading);
        cc.log("CLoading.ccbi is loaded");
        return true;
    }
});
var LoadingScene = cc.Scene.extend({
    onEnter: function () {
        this._super();
        var layer = new LoadingLayer();
        layer.config = this.config;
        layer.init();
        this.addChild(layer);
    }
});

var BlankScene = cc.Scene.extend({
    onEnter: function () {
        cc.log("debugging: recv the command for blank scene:onEnter");
        this._super();
        var scene = cc.BuilderReader.loadAsScene("image/interface/FightBlankLevel.ccbi");
        var runningScene = cc.Director.getInstance().getRunningScene();
        if (runningScene === null)
            cc.Director.getInstance().runWithScene(scene);
        else
            cc.Director.getInstance().replaceScene(scene);
    }
});

var ShowCCLoading = function () {
    cc.log("enter ShowCCLoading");
    QideaCC.GLclearColor(cc.c4f(0.0, 0.0, 0.0, 0.0));
    var scene = cc.BuilderReader.loadAsScene("image/interface/FightBlankLevel.ccbi");
    var runningScene = cc.Director.getInstance().getRunningScene();
    if (runningScene === null)
        cc.Director.getInstance().runWithScene(scene);
    else
        cc.Director.getInstance().replaceScene(scene);
    CocosEval('typeof Pg!="undefined" && typeof Pg.showGL=="function" && Pg.showGL();');
};

var HideCCLoading = function () {
    cc.log("enter HideCCLoading");
    CocosEval('typeof Pg!="undefined" && typeof Pg.hideGL=="function" && Pg.hideGL();forceGC();');
    QideaCC.GLclearColor(cc.c4f(0.0, 0.0, 0.0, 1.0));
};

var HomeScene = cc.Scene.extend({
    onEnter: function () {

        var selfPointer = this;
        cc.log("debugging: recv the command for blank scene:onEnter");
        this._super();
        var node = cc.BuilderReader.loadAsScene("image/interface/PageHome.ccbi");
        var runningScene = cc.Director.getInstance().getRunningScene();
        if (runningScene === null)
            cc.Director.getInstance().runWithScene(node);
        else
            cc.Director.getInstance().replaceScene(node);

        this.adjustSizeForWindow();
        window.addEventListener("resize", function (event) {
            selfPointer.adjustSizeForWindow();
        });

    },

    adjustSizeForWindow: function () {
        var margin = document.documentElement.clientWidth - document.body.clientWidth;
        if (document.documentElement.clientWidth < cc.originalCanvasSize.width) {
            cc.canvas.width = cc.originalCanvasSize.width;
        } else {
            cc.canvas.width = document.documentElement.clientWidth - margin;
        }
        if (document.documentElement.clientHeight < cc.originalCanvasSize.height) {
            cc.canvas.height = cc.originalCanvasSize.height;
        } else {
            cc.canvas.height = document.documentElement.clientHeight - margin;
        }

        var xScale = cc.canvas.width / cc.originalCanvasSize.width;
        var yScale = cc.canvas.height / cc.originalCanvasSize.height;
        if (xScale > yScale) {
            xScale = yScale;
        }
        cc.canvas.width = cc.originalCanvasSize.width * xScale;
        cc.canvas.height = cc.originalCanvasSize.height * xScale;
        var parentDiv = document.getElementById("Cocos2dGameContainer");
        if (parentDiv) {
            parentDiv.style.width = cc.canvas.width + "px";
            parentDiv.style.height = cc.canvas.height + "px";
        }
        cc.renderContext.translate(0, cc.canvas.height);
        cc.renderContext.scale(xScale, xScale);
        cc.Director.getInstance().setContentScaleFactor(xScale);
    }
});

function PlayAnim() {
    var fightAnimScene = new FightAnimScene();
    cc.Director.getInstance().pushScene(fightAnimScene);
}

function ShowLogoScene() {
    var scene = cc.BuilderReader.loadAsScene("image/interface/Logo.ccbi");

    var runningScene = cc.Director.getInstance().getRunningScene();
    if (runningScene === null)
        cc.Director.getInstance().runWithScene(scene);
    else
        cc.Director.getInstance().replaceScene(scene);

}

function ShowFindHero() {

    if (sys.platform != "browser") {
        QideaCC.setIdleTimerDisabled(true);
    }
    cc.log("showFindHero");
    if (sys.platform == "browser") {
        $("#pageFightAnimateClient").show();
    } else {
        QideaCC.GLclearColor(cc.c4f(0.0, 0.0, 0.0, 0.0));
    }
    var scene = cc.BuilderReader.loadAsScene("image/interface/FindHero.ccbi");
    var runningScene = cc.Director.getInstance().getRunningScene();
    if (runningScene === null)
        cc.Director.getInstance().runWithScene(scene);
    else
        cc.Director.getInstance().replaceScene(scene);

    var hidePgCallBack = function () {
        cc.log("show cocos view ");
        CocosEval('typeof Pg!="undefined" && typeof Pg.showGL=="function" && Pg.showGL();');
    };
    scene.scheduleOnce(hidePgCallBack, 0);

    var actionObj = cc.Sequence.create(cc.DelayTime.create(1.5), cc.CallFunc.create(function () {
        //cc.log("removeChild");
        if (sys.platform == "browser") {
            $("#pageFightAnimateClient").hide();
        } else {

            CocosEval('typeof Pg!="undefined" && typeof Pg.hideGL=="function" && Pg.hideGL();forceGC();');
            cc.TextureCache.getInstance().removeAllTextures();
            QideaCC.GLclearColor(cc.c4f(0.0, 0.0, 0.0, 1.0));
            if (sys.platform != "browser") {
                QideaCC.setIdleTimerDisabled(false);
            }
        }
    }));
    scene.runAction(actionObj);
}

function PlayBlankAnim() {
    cc.log("debugging: recv the command for PlayBlankAnim for new!");
    var scene = cc.BuilderReader.loadAsScene("image/interface/FightLevel.ccbi");
    var runningScene = director.getRunningScene();
    if (runningScene === null)
        cc.Director.getInstance().runWithScene(scene);
    else
        cc.Director.getInstance().replaceScene(scene);
}
function testLoadReal() {
    cc.log("debugging: try to load the ccbi file");
    var node = cc.BuilderReader.load("image/interface/FightLevel.ccbi");
}
function PlayReport(resultDataStr) {
    cc.log("debugging: recv the parameter length : " + resultDataStr.length);
    var resultData = JSON.parse(resultDataStr);
    if (typeof Qdiea === "undefined") {
        Qidea = {};
    }

    Qidea.taskResult = resultData;
    var fightAnimScene = cc.BuilderReader.loadAsScene("image/interface/FightLevel.ccbi");
    cc.log("after:" + fightAnimScene);
    cc.Director.getInstance().pushScene(fightAnimScene);
}


//
////tool-util.js 中也有此函数，用来做跨 Cocos Eval调用
if(typeof CocosEval!="function"){
    CocosEval =function (cmd,success,fail){
        var strJs=cmd+"";
        cc.log("sys.platform"+sys.platform);

        if(typeof CONFIG=="undefined" && sys.platform!="browser"){
            CONFIG=JSON.parse(Pg.eval("JSON.stringify(CONFIG)"));
            cc.log(CONFIG);
        }

        if(CONFIG.cocosEngine!="canvas"){
            if(sys.platform=="browser"){
                cordova.exec(
                    function(param) {
                        if(typeof success=="function"){
                            success(param);
                        }
                    },
                    function(error) {
                        if(typeof fail=="function"){
                            fail(error);
                        }
                    },
                    "CcEval","evalInBg",
                    [strJs]);
            }else{
                var returnVal=eval(strJs);
                if(typeof success=="function"){
                    success(returnVal);
                }
            }

        }else{
            var returnVal=eval(strJs);
            if(typeof success=="function"){
                success(returnVal);
            }
        }
    }
}
//
//if (typeof console != "undefined") {
//    log = console.log;
//}
//else {
//    log = cc.log;
//}
//
//CocosEval = function (cmd, success, fail, needRet) {
//    var strJs = cmd + "";
//    if (CONFIG.cocosEngine != "canvas") {
//        if (sys.platform == "browser" && typeof cordova !== "undefined" && typeof cordova.exec !== "undefined") {
//            log("CoscosEval:" + cmd);
//            cordova.exec(
//                function (param) {
//                    if (typeof success == "function") {
//                        success(param);
//                    }
//                },
//                function (error) {
//                    if (typeof fail == "function") {
//                        fail(error);
//                    }
//                },
//                "CcEval", needRet ? "evalWithRet" : "evalInBg",
//                [strJs]);
//        } else {
//            var returnVal = eval(strJs);
//            if (typeof success == "function") {
//                success(returnVal);
//            }
//        }
//
//    } else {
//        var returnVal = eval(strJs);
//        if (typeof success == "function") {
//            success(returnVal);
//        }
//    }
//}

if (typeof QUtil == "undefined") {
    QUtil = {};
}
if (typeof QUtil.playEffect == "undefined") {
    QUtil.playEffect = function (name) {

        if (typeof CONFIG == "undefined" && sys.platform != "browser") {
            CONFIG = JSON.parse(Pg.eval("JSON.stringify(CONFIG)"));
        }
        if (typeof CONFIG != "undefined" && typeof CONFIG.settingInfo != "undefined" && CONFIG.settingInfo.effectMusic) {
            var cmd = 'cc.AudioEngine.getInstance().playEffect("' + name + '");';
            CocosEval(cmd);
        }

    };
}
;

if (typeof QUtil.playBgMusic == "undefined") {
    QUtil.playBgMusic = function (name) {
        if (typeof CONFIG == "undefined" && sys.platform != "browser") {
            CONFIG = JSON.parse(Pg.eval("JSON.stringify(CONFIG)"));
        }
        if (typeof CONFIG != "undefined" && typeof CONFIG.settingInfo != "undefined" && CONFIG.settingInfo.bgMusic) {
            if (!QUtil.playingMusic || QUtil.playingMusic != name) {
                var cmd = 'cc.AudioEngine.getInstance().playMusic("' + name + '");';
                CocosEval(cmd);
                QUtil.playingMusic = name;
            }
        }
    };
}
;