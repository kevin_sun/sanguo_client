
#include "lua_extensions.h"

#if __cplusplus
extern "C" {
#endif

// cjson
#include "lua_cjson.h"

// zlib
#include "lua_zlib.h"

// lpack
#include "lpack/lpack.h"

// socket
#include "luasocket.h"
#include "mime.h"
#include "socket_scripts.h"

// lsqlite3
#include "lsqlite3/lsqlite3.h"
#include "lfs.h"

static luaL_Reg luax_exts[] = {
    {"cjson", luaopen_cjson_safe},
    {"zlib", luaopen_zlib},
    {"pack", luaopen_pack},
    {"socket.core", luaopen_socket_core},
    {"mime.core", luaopen_mime_core},
    {"lfs", luaopen_lfs},
    {"lsqlite3", luaopen_lsqlite3},
    {NULL, NULL}
};

#include "tolua_fix.h"

void luaopen_lua_extensions(lua_State *L)
{
    // load extensions
    luaL_Reg* lib = luax_exts;
    lua_getglobal(L, "package");
    lua_getfield(L, -1, "preload");
    for (; lib->func; lib++)
    {
        lua_pushcfunction(L, lib->func);
        lua_setfield(L, -2, lib->name);
    }
    lua_pop(L, 2);

    // load extensions script
    luaopen_socket_scripts(L);
    // load lpeg support
    //luaopen_lpeg(L);
    // load pbc support
    //luaopen_protobuf_c(L);
}

#if __cplusplus
} // extern "C"
#endif
