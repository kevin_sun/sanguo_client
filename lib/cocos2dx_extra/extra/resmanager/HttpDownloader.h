//
//  HttpDownloader.h
//  casinogame
//
//  Created by 孙 伟 on 13-8-6.
//
//

#ifndef _HTTP_DOWNLOADER_H_
#define _HTTP_DOWNLOADER_H_
#include "cocos2d.h"
#include "cocos2dx_extra.h"
#include "cocos-ext.h"
#include "network/CCHTTPRequest.h"
#include "network/CCHTTPRequestDelegate.h"
#include "network/CCNetwork.h"
#include "ResConstants.h"
USING_NS_CC_EXTRA;
USING_NS_CC_EXT;

enum DownloadResult
{
    kDownloadSucceed,
    kDownloadError,
    kDownloadProgress
};
//declare the HttpDownLoader class,because you will used it at the next step about typedef command
class HttpDownloader;
typedef void (cocos2d::CCObject::*DownloadCallBack)(HttpDownloader* call,DownloadResult result);

class HttpDownloader:public cocos2d::CCObject, public CCHTTPRequestDelegate {
public:
    HttpDownloader();
    virtual ~HttpDownloader();
    // 要下载文件的url
    void setUrl(const char* url);
    
    const char* getUrl();
    void setFullPath(const char* fullPath);
    const char* getFullPath();
    void setDownloadCallBack(cocos2d::CCObject* pTarget,DownloadCallBack downloadCallBack);
    bool download();
    float getProcess();
    const char* getErrorMsg();
    virtual void requestFinished(CCHTTPRequest* request);
    virtual void requestFailed(CCHTTPRequest* request);
    CC_SYNTHESIZE(int, _errorCode, ErrorCode);
protected:
    bool createFile();
    void doCallback(DownloadResult result);
    static int progressFunc(void* userdata, double dltotal, double dlnow, double ultotal, double ulnow);
protected:
    cocos2d::CCString _fullPath;
    cocos2d::CCString _url;
    cocos2d::CCObject* _pTarget;
    DownloadCallBack _downloadCallBack;
    size_t _totalSize;
    float _process;
    cocos2d::CCString _errorMsg;
    DownloadResult _result;
    FILE* _pFile;
    bool isDownloading;
private:
    void deleteTempFile(CCHTTPRequest* request);
};

#endif
