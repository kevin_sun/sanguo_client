//
//  FileManager.cpp
//  casinogame
//
//  Created by 孙 伟 on 13-8-6.
//
//

#include "FileManager.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include "support/zip_support/unzip.h"
using namespace std;
using namespace cocos2d;
#define BUFFER_SIZE 8192
#define MAX_FILENAME   512
const char* FileManager::splicePath(const std::string& parentPath,const std::string& childPath)
{
    string tempPath1=parentPath;
    string tempPath2=childPath;
    if(tempPath1.size()>0&&tempPath1.at(tempPath1.size()-1)=='/')
    {
        tempPath1.erase(tempPath1.size()-1);
    }
    if(tempPath2.size()>0&&tempPath2.at(0)=='/')
    {
        tempPath2.erase(0);
    }
    tempPath1.append("/");
    tempPath1.append(tempPath2);
    return CCString::create(tempPath1)->getCString();
}

bool FileManager::fileExists(const std::string& filePath)
{
    return (0==access(filePath.c_str(), F_OK));
}

bool FileManager::moveFile(const std::string& oldFilePath,const std::string& newFilePath)
{
    return (0==rename(oldFilePath.c_str(), newFilePath.c_str()));
}

bool FileManager::removeFile(const std::string& filePath)
{
    return 0==remove(filePath.c_str());
}

bool FileManager::createDirectory(const std::string& directoryPath)
{
    vector<string> directoryPathArray;
    string fullPath=directoryPath;
    do {
        int pos=fullPath.find_last_of('/');
        string dirSubPath=fullPath.substr(0,pos);
        if(0!=access(dirSubPath.c_str(), F_OK))
        {
            directoryPathArray.insert(directoryPathArray.begin(), dirSubPath);
            fullPath=dirSubPath;
        }
        else
        {
            break;
        }
    } while (true);
    // 从父到子逐层创建目录
    bool result = true;
    for(vector<string>::iterator it=directoryPathArray.begin();it!=directoryPathArray.end();it++)
    {
        if(!doCreateDir(*it))
        {
            result=false;
            break;
        }
    }
    return result;
}

bool FileManager::doCreateDir(const std::string& dirPath)
{
    mode_t processMask=umask(0);
    int ret=mkdir(dirPath.c_str(), S_IRWXU | S_IRWXG | S_IRWXO);
    umask(processMask);
    if(ret!=0&&(errno != EEXIST))
    {
         return false;
    }
    return true;
}


bool FileManager::uncompressZipFile(const std::string &zipFilePath, const std::string &directory)
{
    unzFile zipfile=unzOpen(zipFilePath.c_str());
    if(!zipfile)
    {
        CCLOG("can't open downloaded zip file %s",zipFilePath.c_str());
    }
    //get Info from zip file
    unz_global_info globalinfo;
    if(unzGetGlobalInfo(zipfile, &globalinfo)!=UNZ_OK)
    {
        CCLOG("can't get zip global zip info %s",zipFilePath.c_str());
        unzClose(zipfile);
    }
    char readBuffer[BUFFER_SIZE];
    CCLOG("start uncompressing %s,to:%s",zipFilePath.c_str(),directory.c_str());
    for(uLong i=0;i<globalinfo.number_entry;i++)
    {
        unz_file_info fileInfo;
        char fileName[MAX_FILENAME];
        if(unzGetCurrentFileInfo(zipfile, &fileInfo, fileName, MAX_FILENAME, NULL, 0, NULL, 0)!=UNZ_OK)
        {
            CCLOG("can not read file info");
            unzClose(zipfile);
            return false;
        }
        string saveToDirectory = directory.c_str();
        if(saveToDirectory.at(saveToDirectory.size()-1)!='/')
        {
            saveToDirectory.append("/");
        }
        string fullPath=saveToDirectory+fileName;
        const size_t fileNameLength=strlen(fileName);
        if(fileName[fileNameLength-1]=='/')
        {
            //entry is a directory
            if(!createDirectory(fullPath.c_str()))
            {
                CCLOG("can not create directory %s", fullPath.c_str());
                unzClose(zipfile);
                return false;
            }
        }
        else
        {
            // Entry is a file, so extract it.
            if(unzOpenCurrentFile(zipfile)!=UNZ_OK)
            {
                CCLOG("can not open file %s", fileName);
                unzClose(zipfile);
                return false;
            }
            FILE* out=fopen(fullPath.c_str(), "wb");
            if(!out)
            {
                CCLOG("can not open destination file %s", fullPath.c_str());
                unzCloseCurrentFile(zipfile);
                unzClose(zipfile);
                return false;
            }
            int error = UNZ_OK;
            do {
                error=unzReadCurrentFile(zipfile, readBuffer, BUFFER_SIZE);
                if(error<0)
                {
                    CCLOG("can not read zip file %s, error code is %d", fileName, error);
                    unzCloseCurrentFile(zipfile);
                    unzClose(zipfile);
                    return false;
                }
                if (error > 0)
                {
                    //error 就是从文件中实际读取的字节数量
                    fwrite(readBuffer, error, 1, out);
                }
            } while (error>0);
            fclose(out);
        }
        unzCloseCurrentFile(zipfile);
        if((i+1)<globalinfo.number_entry)
        {
            if(unzGoToNextFile(zipfile)!=UNZ_OK)
            {
                CCLOG("can not read next file");
                unzClose(zipfile);
                return false;
            }
        }
    }
    CCLOG("end uncompressing");
    return true;
}

bool FileManager::writeStringToFile(const std::string &content, const std::string &filePath,bool isAppend)
{
    const char *mode = isAppend ? "ab" : "wb";
    FILE* out=fopen(filePath.c_str(), mode);
    if(!out)
    {
        CCLOG("创建文件错误，errno=%d", errno);
        return false;
    }
    size_t written = fwrite(content.c_str(), content.length(), 1, out);
    fclose(out);
    return written==content.length();
}

