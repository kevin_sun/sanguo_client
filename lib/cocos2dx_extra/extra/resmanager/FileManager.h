//
//  FileManager.h
//  casinogame
//
//  Created by 孙 伟 on 13-8-6.
//
//

#ifndef _FILE_MANAGER_H_
#define _FILE_MANAGER_H_
#include <iostream>
#include "cocos2d.h"

class FileManager {
    
public:
    //判断此路径是否存在文件/或文件夹
    static bool fileExists(const std::string& filePath);
    //拼接路径
    static const char* splicePath(const std::string& parentPath,const std::string& childPath);
    /*
     * 文件移动到其它目录，但是要移动的目录不存在，则会失败
     * 可以用来重命名文件，如果文件已经存在，则覆盖
     */
    static bool moveFile(const std::string& oldFilePath,const std::string& newFilePath);
    //删除文件
    static bool removeFile(const std::string& filePath);
    //保存文件
    static bool writeStringToFile(const std::string& content,const std::string& filePath,bool isAppend=false);
    //创建目录
    static bool createDirectory(const std::string& directoryPath);
    //解压zip文件到指定目录
    static bool uncompressZipFile(const std::string& zipFile,const std::string& directory);
private:
    static bool doCreateDir(const std::string& directoryPath);
};


#endif
