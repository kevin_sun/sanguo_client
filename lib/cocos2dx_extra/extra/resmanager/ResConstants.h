//
//  ResConstants.h
//  quickcocos2dx
//
//  Created by 孙 伟 on 13-8-23.
//  Copyright (c) 2013年 qeeplay.com. All rights reserved.
//

#ifndef _RES_CONSTANTS_H_
#define _RES_CONSTANTS_H_

namespace ResConstant
{
    //当version.txt下载后与本地版本做完版本对比后回调事件
    const string onVersionCheckCompleted="onVersionCheckCompleted";
    //当全局资源和模块更新列表更新完成后回调事件
    const string onVersionUpdated="onVersionUpdated";
    //当有下载回错，和resource里执行相应方法出错的回调事件
    const string onErrorOccured="onErrorOccured";
    //当有模块中的一个文件更新完成后回调事件
    const string onSingleFileUpdated="onSingleFileUpdated";
    //当模块更新完成后触发
    const string onModuleUpdated="onModuleUpdated";
    //定义错误码
    const int ok=0;
    //创建文件出错
    const int createTmpFileError = 1;
    //url出错
    const int downloadUrlError = 2;
    //下载文件出错
    const int downloadFileError=3;
    //解压模块文件列表出错
    const int uncompressUpdateListError=4;
    //解压全局资源文件出错
    const int uncompressUpdateError=5;
    //模块配置文件不存在
    const int moduleConfigNoexistError=6;
    //下载单个模块文件出错
    const int singleModuleDownloadError=7;
}

#endif
