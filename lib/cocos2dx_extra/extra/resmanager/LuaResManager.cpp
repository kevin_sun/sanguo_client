/*
** Lua binding: LuaResManager
** Generated automatically by tolua++-1.0.92 on Tue Aug 13 11:57:15 2013.
*/

#include "LuaResManager.h"
#include "CCLuaEngine.h"

using namespace cocos2d;




#include "LuaResManager.h"
#include "ResourceManager.h"

/* function to release collected object via destructor */
#ifdef __cplusplus

static int tolua_collect_LUA_FUNCTION (lua_State* tolua_S)
{
 LUA_FUNCTION* self = (LUA_FUNCTION*) tolua_tousertype(tolua_S,1,0);
    Mtolua_delete(self);
    return 0;
}
#endif


/* function to register type */
static void tolua_reg_types (lua_State* tolua_S)
{
 tolua_usertype(tolua_S,"ResourceManager");
 tolua_usertype(tolua_S,"ResourceUpdateDelegate");
 
 tolua_usertype(tolua_S,"CCObject");
}

/* method: onVersionCheckCompleted of class  ResourceUpdateDelegate */
#ifndef TOLUA_DISABLE_tolua_LuaResManager_ResourceUpdateDelegate_onVersionCheckCompleted00
static int tolua_LuaResManager_ResourceUpdateDelegate_onVersionCheckCompleted00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"ResourceUpdateDelegate",0,&tolua_err) ||
     !tolua_isboolean(tolua_S,2,0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  ResourceUpdateDelegate* self = (ResourceUpdateDelegate*)  tolua_tousertype(tolua_S,1,0);
  bool latest = ((bool)  tolua_toboolean(tolua_S,2,0));
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'onVersionCheckCompleted'", NULL);
#endif
  {
   self->onVersionCheckCompleted(latest);
  }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'onVersionCheckCompleted'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: onVersionUpdated of class  ResourceUpdateDelegate */
#ifndef TOLUA_DISABLE_tolua_LuaResManager_ResourceUpdateDelegate_onVersionUpdated00
static int tolua_LuaResManager_ResourceUpdateDelegate_onVersionUpdated00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"ResourceUpdateDelegate",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  ResourceUpdateDelegate* self = (ResourceUpdateDelegate*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'onVersionUpdated'", NULL);
#endif
  {
   self->onVersionUpdated();
  }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'onVersionUpdated'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: onModuleUpdated of class  ResourceUpdateDelegate */
#ifndef TOLUA_DISABLE_tolua_LuaResManager_ResourceUpdateDelegate_onModuleUpdated00
static int tolua_LuaResManager_ResourceUpdateDelegate_onModuleUpdated00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"ResourceUpdateDelegate",0,&tolua_err) ||
     !tolua_iscppstring(tolua_S,2,0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  ResourceUpdateDelegate* self = (ResourceUpdateDelegate*)  tolua_tousertype(tolua_S,1,0);
  const std::string moduleName = ((const std::string)  tolua_tocppstring(tolua_S,2,0));
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'onModuleUpdated'", NULL);
#endif
  {
   self->onModuleUpdated(moduleName);
   tolua_pushcppstring(tolua_S,(const char*)moduleName);
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'onModuleUpdated'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: onSingleFileUpdated of class  ResourceUpdateDelegate */
#ifndef TOLUA_DISABLE_tolua_LuaResManager_ResourceUpdateDelegate_onSingleFileUpdated00
static int tolua_LuaResManager_ResourceUpdateDelegate_onSingleFileUpdated00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"ResourceUpdateDelegate",0,&tolua_err) ||
     !tolua_iscppstring(tolua_S,2,0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  ResourceUpdateDelegate* self = (ResourceUpdateDelegate*)  tolua_tousertype(tolua_S,1,0);
  const std::string filePath = ((const std::string)  tolua_tocppstring(tolua_S,2,0));
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'onSingleFileUpdated'", NULL);
#endif
  {
   self->onSingleFileUpdated(filePath);
   tolua_pushcppstring(tolua_S,(const char*)filePath);
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'onSingleFileUpdated'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: onErrorOccured of class  ResourceUpdateDelegate */
#ifndef TOLUA_DISABLE_tolua_LuaResManager_ResourceUpdateDelegate_onErrorOccured00
static int tolua_LuaResManager_ResourceUpdateDelegate_onErrorOccured00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"ResourceUpdateDelegate",0,&tolua_err) ||
     !tolua_isstring(tolua_S,2,0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  ResourceUpdateDelegate* self = (ResourceUpdateDelegate*)  tolua_tousertype(tolua_S,1,0);
  const char* message = ((const char*)  tolua_tostring(tolua_S,2,0));
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'onErrorOccured'", NULL);
#endif
  {
   self->onErrorOccured(message);
  }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'onErrorOccured'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: sharedResourceManager of class  ResourceManager */
#ifndef TOLUA_DISABLE_tolua_LuaResManager_ResourceManager_sharedResourceManager00
static int tolua_LuaResManager_ResourceManager_sharedResourceManager00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertable(tolua_S,1,"ResourceManager",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  {
   ResourceManager* tolua_ret = (ResourceManager*)  ResourceManager::sharedResourceManager();
    tolua_pushusertype(tolua_S,(void*)tolua_ret,"ResourceManager");
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'sharedResourceManager'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: setDelegate of class  ResourceManager */
#ifndef TOLUA_DISABLE_tolua_LuaResManager_ResourceManager_setDelegate00
static int tolua_LuaResManager_ResourceManager_setDelegate00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"ResourceManager",0,&tolua_err) ||
     !tolua_isusertype(tolua_S,2,"ResourceUpdateDelegate",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  ResourceManager* self = (ResourceManager*)  tolua_tousertype(tolua_S,1,0);
  ResourceUpdateDelegate* delegate = ((ResourceUpdateDelegate*)  tolua_tousertype(tolua_S,2,0));
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'setDelegate'", NULL);
#endif
  {
   self->setDelegate(delegate);
  }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'setDelegate'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: getDelegate of class  ResourceManager */
#ifndef TOLUA_DISABLE_tolua_LuaResManager_ResourceManager_getDelegate00
static int tolua_LuaResManager_ResourceManager_getDelegate00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"ResourceManager",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  ResourceManager* self = (ResourceManager*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getDelegate'", NULL);
#endif
  {
   ResourceUpdateDelegate* tolua_ret = (ResourceUpdateDelegate*)  self->getDelegate();
    tolua_pushusertype(tolua_S,(void*)tolua_ret,"ResourceUpdateDelegate");
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getDelegate'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: setScriptHandler of class  ResourceManager */
#ifndef TOLUA_DISABLE_tolua_LuaResManager_ResourceManager_setScriptHandler00
static int tolua_LuaResManager_ResourceManager_setScriptHandler00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"ResourceManager",0,&tolua_err) ||
     (tolua_isvaluenil(tolua_S,2,&tolua_err) || !toluafix_isfunction(tolua_S,2,"LUA_FUNCTION",0,&tolua_err)) ||
     !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  ResourceManager* self = (ResourceManager*)  tolua_tousertype(tolua_S,1,0);
  LUA_FUNCTION scriptHandler = (  toluafix_ref_function(tolua_S,2,0));
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'setScriptHandler'", NULL);
#endif
  {
   self->setScriptHandler(scriptHandler);
  }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'setScriptHandler'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: getScriptHandler of class  ResourceManager */
#ifndef TOLUA_DISABLE_tolua_LuaResManager_ResourceManager_getScriptHandler00
static int tolua_LuaResManager_ResourceManager_getScriptHandler00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"ResourceManager",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  ResourceManager* self = (ResourceManager*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getScriptHandler'", NULL);
#endif
  {
   LUA_FUNCTION tolua_ret = (LUA_FUNCTION)  self->getScriptHandler();
   {
#ifdef __cplusplus
    void* tolua_obj = Mtolua_new((LUA_FUNCTION)(tolua_ret));
     tolua_pushusertype(tolua_S,tolua_obj,"LUA_FUNCTION");
    tolua_register_gc(tolua_S,lua_gettop(tolua_S));
#else
    void* tolua_obj = tolua_copy(tolua_S,(void*)&tolua_ret,sizeof(LUA_FUNCTION));
     tolua_pushusertype(tolua_S,tolua_obj,"LUA_FUNCTION");
    tolua_register_gc(tolua_S,lua_gettop(tolua_S));
#endif
   }
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getScriptHandler'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: setRootURL of class  ResourceManager */
#ifndef TOLUA_DISABLE_tolua_LuaResManager_ResourceManager_setRootURL00
static int tolua_LuaResManager_ResourceManager_setRootURL00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"ResourceManager",0,&tolua_err) ||
     !tolua_iscppstring(tolua_S,2,0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  ResourceManager* self = (ResourceManager*)  tolua_tousertype(tolua_S,1,0);
  const std::string rootUrl = ((const std::string)  tolua_tocppstring(tolua_S,2,0));
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'setRootURL'", NULL);
#endif
  {
   self->setRootURL(rootUrl);
   tolua_pushcppstring(tolua_S,(const char*)rootUrl);
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'setRootURL'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: getRootURL of class  ResourceManager */
#ifndef TOLUA_DISABLE_tolua_LuaResManager_ResourceManager_getRootURL00
static int tolua_LuaResManager_ResourceManager_getRootURL00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"ResourceManager",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  ResourceManager* self = (ResourceManager*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getRootURL'", NULL);
#endif
  {
   const char* tolua_ret = (const char*)  self->getRootURL();
   tolua_pushstring(tolua_S,(const char*)tolua_ret);
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getRootURL'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: isVersionLatest of class  ResourceManager */
#ifndef TOLUA_DISABLE_tolua_LuaResManager_ResourceManager_isVersionLatest00
static int tolua_LuaResManager_ResourceManager_isVersionLatest00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"ResourceManager",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  ResourceManager* self = (ResourceManager*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'isVersionLatest'", NULL);
#endif
  {
   bool tolua_ret = (bool)  self->isVersionLatest();
   tolua_pushboolean(tolua_S,(bool)tolua_ret);
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'isVersionLatest'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: getVersion of class  ResourceManager */
#ifndef TOLUA_DISABLE_tolua_LuaResManager_ResourceManager_getVersion00
static int tolua_LuaResManager_ResourceManager_getVersion00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"ResourceManager",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  ResourceManager* self = (ResourceManager*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getVersion'", NULL);
#endif
  {
   float tolua_ret = (float)  self->getVersion();
   tolua_pushnumber(tolua_S,(lua_Number)tolua_ret);
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getVersion'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: isDevelopMode of class  ResourceManager */
#ifndef TOLUA_DISABLE_tolua_LuaResManager_ResourceManager_isDevelopMode00
static int tolua_LuaResManager_ResourceManager_isDevelopMode00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"ResourceManager",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  ResourceManager* self = (ResourceManager*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'isDevelopMode'", NULL);
#endif
  {
   bool tolua_ret = (bool)  self->isDevelopMode();
   tolua_pushboolean(tolua_S,(bool)tolua_ret);
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'isDevelopMode'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: checkVersion of class  ResourceManager */
#ifndef TOLUA_DISABLE_tolua_LuaResManager_ResourceManager_checkVersion00
static int tolua_LuaResManager_ResourceManager_checkVersion00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"ResourceManager",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  ResourceManager* self = (ResourceManager*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'checkVersion'", NULL);
#endif
  {
   self->checkVersion();
  }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'checkVersion'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: updateVersion of class  ResourceManager */
#ifndef TOLUA_DISABLE_tolua_LuaResManager_ResourceManager_updateVersion00
static int tolua_LuaResManager_ResourceManager_updateVersion00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"ResourceManager",0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  ResourceManager* self = (ResourceManager*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'updateVersion'", NULL);
#endif
  {
   self->updateVersion();
  }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'updateVersion'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: checkModule of class  ResourceManager */
#ifndef TOLUA_DISABLE_tolua_LuaResManager_ResourceManager_checkModule00
static int tolua_LuaResManager_ResourceManager_checkModule00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"ResourceManager",0,&tolua_err) ||
     !tolua_iscppstring(tolua_S,2,0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  ResourceManager* self = (ResourceManager*)  tolua_tousertype(tolua_S,1,0);
  const std::string moduleName = ((const std::string)  tolua_tocppstring(tolua_S,2,0));
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'checkModule'", NULL);
#endif
  {
   int tolua_ret = (int)  self->checkModule(moduleName);
   tolua_pushnumber(tolua_S,(lua_Number)tolua_ret);
   tolua_pushcppstring(tolua_S,(const char*)moduleName);
  }
 }
 return 2;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'checkModule'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: updateModule of class  ResourceManager */
#ifndef TOLUA_DISABLE_tolua_LuaResManager_ResourceManager_updateModule00
static int tolua_LuaResManager_ResourceManager_updateModule00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"ResourceManager",0,&tolua_err) ||
     !tolua_iscppstring(tolua_S,2,0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  ResourceManager* self = (ResourceManager*)  tolua_tousertype(tolua_S,1,0);
  const std::string moduleName = ((const std::string)  tolua_tocppstring(tolua_S,2,0));
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'updateModule'", NULL);
#endif
  {
   self->updateModule(moduleName);
   tolua_pushcppstring(tolua_S,(const char*)moduleName);
  }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'updateModule'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* method: saveVersion of class  ResourceManager */
#ifndef TOLUA_DISABLE_tolua_LuaResManager_ResourceManager_saveVersion00
static int tolua_LuaResManager_ResourceManager_saveVersion00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
     !tolua_isusertype(tolua_S,1,"ResourceManager",0,&tolua_err) ||
     !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
     !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
  goto tolua_lerror;
 else
#endif
 {
  ResourceManager* self = (ResourceManager*)  tolua_tousertype(tolua_S,1,0);
  float version = ((float)  tolua_tonumber(tolua_S,2,0));
#ifndef TOLUA_RELEASE
  if (!self) tolua_error(tolua_S,"invalid 'self' in function 'saveVersion'", NULL);
#endif
  {
   self->saveVersion(version);
  }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'saveVersion'.",&tolua_err);
 return 0;
#endif
}
#endif //#ifndef TOLUA_DISABLE

/* Open function */
TOLUA_API int tolua_LuaResManager_open (lua_State* tolua_S)
{
 tolua_open(tolua_S);
 tolua_reg_types(tolua_S);
 tolua_module(tolua_S,NULL,0);
 tolua_beginmodule(tolua_S,NULL);
  tolua_cclass(tolua_S,"ResourceUpdateDelegate","ResourceUpdateDelegate","CCObject",NULL);
  tolua_beginmodule(tolua_S,"ResourceUpdateDelegate");
   tolua_function(tolua_S,"onVersionCheckCompleted",tolua_LuaResManager_ResourceUpdateDelegate_onVersionCheckCompleted00);
   tolua_function(tolua_S,"onVersionUpdated",tolua_LuaResManager_ResourceUpdateDelegate_onVersionUpdated00);
   tolua_function(tolua_S,"onModuleUpdated",tolua_LuaResManager_ResourceUpdateDelegate_onModuleUpdated00);
   tolua_function(tolua_S,"onSingleFileUpdated",tolua_LuaResManager_ResourceUpdateDelegate_onSingleFileUpdated00);
   tolua_function(tolua_S,"onErrorOccured",tolua_LuaResManager_ResourceUpdateDelegate_onErrorOccured00);
  tolua_endmodule(tolua_S);
  tolua_cclass(tolua_S,"ResourceManager","ResourceManager","CCObject",NULL);
  tolua_beginmodule(tolua_S,"ResourceManager");
   tolua_function(tolua_S,"sharedResourceManager",tolua_LuaResManager_ResourceManager_sharedResourceManager00);
   tolua_function(tolua_S,"setDelegate",tolua_LuaResManager_ResourceManager_setDelegate00);
   tolua_function(tolua_S,"getDelegate",tolua_LuaResManager_ResourceManager_getDelegate00);
   tolua_function(tolua_S,"setScriptHandler",tolua_LuaResManager_ResourceManager_setScriptHandler00);
   tolua_function(tolua_S,"getScriptHandler",tolua_LuaResManager_ResourceManager_getScriptHandler00);
   tolua_function(tolua_S,"setRootURL",tolua_LuaResManager_ResourceManager_setRootURL00);
   tolua_function(tolua_S,"getRootURL",tolua_LuaResManager_ResourceManager_getRootURL00);
   tolua_function(tolua_S,"isVersionLatest",tolua_LuaResManager_ResourceManager_isVersionLatest00);
   tolua_function(tolua_S,"getVersion",tolua_LuaResManager_ResourceManager_getVersion00);
   tolua_function(tolua_S,"isDevelopMode",tolua_LuaResManager_ResourceManager_isDevelopMode00);
   tolua_function(tolua_S,"checkVersion",tolua_LuaResManager_ResourceManager_checkVersion00);
   tolua_function(tolua_S,"updateVersion",tolua_LuaResManager_ResourceManager_updateVersion00);
   tolua_function(tolua_S,"checkModule",tolua_LuaResManager_ResourceManager_checkModule00);
   tolua_function(tolua_S,"updateModule",tolua_LuaResManager_ResourceManager_updateModule00);
   tolua_function(tolua_S,"saveVersion",tolua_LuaResManager_ResourceManager_saveVersion00);
  tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 return 1;
}


#if defined(LUA_VERSION_NUM) && LUA_VERSION_NUM >= 501
 TOLUA_API int luaopen_LuaResManager (lua_State* tolua_S) {
 return tolua_LuaResManager_open(tolua_S);
};
#endif

