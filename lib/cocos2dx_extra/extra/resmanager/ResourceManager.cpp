//
//  ResourceManager.cpp
//  casinogame
//
//  Created by 孙 伟 on 13-8-8.
//
//

#include "ResourceManager.h"
#include "FileManager.h"
#include "json/JSONElement.h"
#include "CCLuaEngine.h"

#define RMVersionFile                     "version.txt"                 //版本描述文件
#define RMUpdateZipFile                   "update.zip"                  //增量更新包 (只有全局资源，模块资源还是通过resource下载)
#define RMUpdateListZipFile               "updatelist.zip"              //模块更新列表包
#define RMModuleDirectory                 "module"                      //模块资源目录
#define RMModuleConfigDirectory           "module-config"               //模块配置目录
#define RMModuleConfigSuffix              ".txt"                        //模块资源配置文件后缀
#define RMServerUpdateDirectory           "Update"                      //增量更新包在资源服务器上的目录
#define RMServerResourceDirectory         "Resource"                    //最新资源包在资源服务器上的目录
#define RMLocalResourceDirectory          "Resource"                    //最新资源包在本地的目录
#define RMVersionPathFormat               "%0.2f/%0.2f"                 //更新包的版本目录名的格式
#define RMUpdateListFile                  "UpdateList.txt"              //更新列表文件
#define RMOldUpdateListFile               "UpdateList-old.txt"          //旧的更新列表文件（临时）

#define RMVersionKey                      "version"                     //本地的版本号在UserDefault中的键名，

#define BundleResourceVersion             0                             //如果程序包内无资源（微端模式），此处填0
#define RMDevelopMode                     false                          //开发模式标志

USING_NS_CC;
using namespace std;

static ResourceManager* s_ResourceManager=NULL;

ResourceManager::ResourceManager()
:rootUrl("")
,downloader(NULL)
,downloadFileType(kDownloadDefault)
,serverResourceVersion(0)
,versionLatest(false)
,currentModuleName("")
,downloadCache(NULL)
,scriptHandler(0)
,delegate(NULL)
{
    CCScriptEngineProtocol* pEngine = CCScriptEngineManager::sharedManager()->getScriptEngine();
    m_eScriptType = pEngine != NULL ? pEngine->getScriptType() : kScriptTypeNone;
    downloader=new HttpDownloader();
    //设置下载回调涵数
    downloader->setDownloadCallBack(this, (DownloadCallBack)&ResourceManager::onDownloaderResult);
    CC_SAFE_RETAIN(downloader);
    
    downloadCache=CCDictionary::create();
    CC_SAFE_RETAIN(downloadCache);
    //初始化本地资源
    localVersion=CCUserDefault::sharedUserDefault()->getFloatForKey(RMVersionKey);
}

ResourceManager* ResourceManager::sharedResourceManager()
{
    if(s_ResourceManager==NULL)
    {
        s_ResourceManager=new ResourceManager();
    }
    return s_ResourceManager;
}

ResourceManager::~ResourceManager()
{
    CC_SAFE_RELEASE_NULL(downloader);
    CC_SAFE_RELEASE_NULL(downloadCache);
    if(scriptHandler)
    {
        CCLuaEngine::defaultEngine()->removeScriptHandler(scriptHandler);
        scriptHandler=0;
    }
}

const char* ResourceManager::fullLocalResourcePath(const std::string &filePath)
{
    string llocalResourcePath=FileManager::splicePath(CCFileUtils::sharedFileUtils()->getWritablePath(), RMLocalResourceDirectory);
    return FileManager::splicePath(llocalResourcePath, filePath);
}

void ResourceManager::setRootURL(const string &rootUrl)
{
    this->rootUrl=rootUrl;
    if(this->rootUrl.at(this->rootUrl.size()-1)!='/')
    {
        this->rootUrl.append("/");
    }
}

const char* ResourceManager::getRootURL()
{
    return rootUrl.c_str();
}

bool ResourceManager::isVersionLatest()
{
    return versionLatest;
}

bool ResourceManager::isDevelopMode()
{
    return RMDevelopMode;
}

void ResourceManager::configSearchPath()
{
    //(1) 清除所有SearchPath设置
    std::vector<std::string> emptySearchPaths;
    CCFileUtils::sharedFileUtils()->setSearchPaths(emptySearchPaths);
    //(3) 设置CCFileUtils的SearchPath和lua的PackagePath
    std::vector<std::string> searchPaths;
    std::vector<std::string> packagePaths;
    std::string scriptPath = "script";
    
    //WritablePath
    if (!this->isDevelopMode()) {
        searchPaths.push_back(this->fullLocalResourcePath(""));
        packagePaths.push_back(this->fullLocalResourcePath(scriptPath));
    }
    //Bundle
    searchPaths.push_back("");
    packagePaths.push_back(CCFileUtils::sharedFileUtils()->fullPathForFilename(scriptPath.c_str()));
    CCFileUtils::sharedFileUtils()->setSearchPaths(searchPaths);
    //设置脚本的package path,后面来设置
    CCLuaEngine* pEngine = CCLuaEngine::defaultEngine();
    CCLuaStack* luaStack=pEngine->getLuaStack();
    for (std::vector<std::string>::const_iterator iter = packagePaths.begin(); iter != packagePaths.end(); ++iter)
    {
        luaStack->addSearchPath(iter->c_str());
    }
}

void ResourceManager::saveVersion(float version)
{
    localVersion=version;
    CCUserDefault::sharedUserDefault()->setFloatForKey(RMVersionKey, localVersion);
    CCUserDefault::sharedUserDefault()->flush();
}

void ResourceManager::checkVersion()
{
    if(isDevelopMode())
    {
        return;
    }
    if (localVersion < BundleResourceVersion)
    {
        //本地资源版本号小于内部绑定资源,所以这种情况一般是全局包已经安装ok，只要把模块更新文件列表下载下来就ok了，说明玩家是第一次安装好应用，第一次开始玩
        //则要先下载模块更新列表,以便玩家能下载对应的游戏
        downloadUpdateList(localVersion, BundleResourceVersion);
    }
    else
    {
        //本地资源版本号大于内部绑定资源,则说明有更新,与server version进行验证，是否服务器有更新
        downloadServerVersion();
    }
}


void ResourceManager::downloadUpdateList(float versionOld, float versionNew)
{
    string downloadUrl=FileManager::splicePath(rootUrl, RMServerUpdateDirectory);
    CCString* versionPath=CCString::createWithFormat(RMVersionPathFormat, versionNew,versionOld);
    downloadUrl=FileManager::splicePath(downloadUrl, versionPath->getCString());
    downloadUrl = FileManager::splicePath(downloadUrl, RMUpdateListZipFile);
    string savePath=fullLocalResourcePath(RMUpdateListZipFile);
    downloader->setUrl(downloadUrl.c_str());
    downloader->setFullPath(savePath.c_str());
    //设置下载回调类型
    downloadFileType=kDownloadFileTypeUpdateListPackage;
    downloader->download();
}

void ResourceManager::onDownloadUpdateList()
{
    std::string oldUpdateList=fullLocalResourcePath(RMOldUpdateListFile);
    std::string updateList = fullLocalResourcePath(RMUpdateListFile);
    if(FileManager::fileExists(updateList))
    {
        //存在需要更新文件的列表,move为old列表
        FileManager::moveFile(updateList, oldUpdateList);
    }
    std::string updateListZipFile = fullLocalResourcePath(RMUpdateListZipFile);
    bool unzipStatus=FileManager::uncompressZipFile(updateListZipFile, fullLocalResourcePath(""));
    if(unzipStatus)
    {
        //解压成功，删除zip文件
        FileManager::removeFile(updateListZipFile);
    }
    else
    {
        reportError("uncompress updatelist.zip error",ResConstant::uncompressUpdateListError);
        return;
    }
    //合并原来的updatelist.txt
    if(FileManager::fileExists(oldUpdateList))
    {
        //合并更新列表
        JSONObject* newUpdateListJson=JSONObject::createWithFile(updateList);
        JSONObject* oldUpdateListJson=JSONObject::createWithFile(oldUpdateList);
        CCArray* keyArray=oldUpdateListJson->allKeys();
        if(keyArray)
        {
            for(int i=0;i<keyArray->count();i++)
            {
                string filePath=((CCString*)keyArray->objectAtIndex(i))->getCString();
                if(!newUpdateListJson->has(filePath))
                {
                    newUpdateListJson->putString(filePath, oldUpdateListJson->getString(filePath));
                }
            }
        }
        //新写入文，并不是append
        FileManager::writeStringToFile(newUpdateListJson->toPrettyString(),updateList,false);
        //删除旧的更新列表,写入
        FileManager::removeFile(oldUpdateList);
    }
    if(localVersion<BundleResourceVersion)
    {
        //更新版本
        saveVersion(BundleResourceVersion);
        //将本地版本与服务器版本做对比，是否要接着更新
        downloadServerVersion();
    }
    else
    {
        saveVersion(serverResourceVersion);
        versionLatest=true;
        this->configSearchPath();
        if (this->delegate)
        {
            this->delegate->onVersionUpdated();
        }
        if ( m_eScriptType != kScriptTypeNone)
        {
            if(scriptHandler)
            {
                executeScriptHandler(ResConstant::onVersionUpdated.c_str(),NULL);
            }
        }
    }
}

void ResourceManager::downloadServerVersion()
{
    string versionUrl=FileManager::splicePath(rootUrl,RMVersionFile);
    string fullPath=fullLocalResourcePath(RMVersionFile);
    downloader->setUrl(versionUrl.c_str());
    downloader->setFullPath(fullPath.c_str());
    downloadFileType=kDownloadFileTypeVersionFile;
    downloader->download();
}

void ResourceManager::onDownloadServerVersion()
{
    string versionFilePath=fullLocalResourcePath(RMVersionFile);
    CCString* version=CCString::createWithContentsOfFile(versionFilePath.c_str());
    serverResourceVersion=version->floatValue();
    versionLatest=(localVersion>=serverResourceVersion);
    if(delegate)
    {
        delegate->onVersionCheckCompleted(versionLatest);
    }
    if ( m_eScriptType != kScriptTypeNone&&scriptHandler)
    {
        //脚本调用
        executeScriptHandler(ResConstant::onVersionCheckCompleted.c_str(),ResConstant::ok, versionLatest ? "true":"false");
    }
}

void ResourceManager::updateVersion()
{
    if(isDevelopMode())
        return;
    if(localVersion<=serverResourceVersion)
    {
        //(1) 下载地址
        std::string downloadPath = FileManager::splicePath(this->rootUrl, RMServerUpdateDirectory);
        CCString* versionPath = CCString::createWithFormat(RMVersionPathFormat,serverResourceVersion,localVersion);
        downloadPath = FileManager::splicePath(downloadPath, versionPath->getCString());
        downloadPath = FileManager::splicePath(downloadPath, RMUpdateZipFile);
        downloader->setUrl(downloadPath.c_str());
        //(2) 保存路径
        std::string savePath = this->fullLocalResourcePath(RMUpdateZipFile);
        downloader->setFullPath(savePath.c_str());
        //(3) 下载
        this->downloadFileType = kDownloadFileTypeUpdatePackage;
        downloader->download();
    }
}

void ResourceManager::onUpdateVersion()
{
    //更新全局update.zip
    const std::string zipFilePath  =  this->fullLocalResourcePath(RMUpdateZipFile);
    if (FileManager::fileExists(zipFilePath))
    {
        bool status = FileManager::uncompressZipFile(zipFilePath, this->fullLocalResourcePath(""));
        if(status)
        {
            FileManager::removeFile(zipFilePath.c_str());
        }
        else
        {
            this->reportError("uncompress zip file error",ResConstant::uncompressUpdateError);
            return;
        }
    }
    //全局资源更新完成，则更新下载列表
    downloadUpdateList(localVersion, serverResourceVersion);
}

/*
 * 下载回调方法，因为downloader是启动新的线程去下载，所以这个是在线程下载完，回调的方法
 *
 */

void ResourceManager::onDownloaderResult(HttpDownloader *downloader, DownloadResult result)
{
    if (result==kDownloadSucceed)
    {
        if (downloadFileType==kDownloadFileTypeUpdateListPackage)
        {
            //模块更新列表回调
            onDownloadUpdateList();
        }
        else if (downloadFileType==kDownloadFileTypeVersionFile)
        {
            //版本验证后回调
            onDownloadServerVersion();
        }
        else if(downloadFileType==kDownloadFileTypeUpdatePackage)
        {
            //全局资源下载后更新
            onUpdateVersion();
        }
        else if(downloadFileType==kDownloadFileTypeModule)
        {
            //模块资源下载后触发
            onDownloadModuleFile();
        }
    }
    else if(result==kDownloadError)
    {
        //do nothing,下载出错        
        //如果是下载某个模块文件出错的话，则还接着下载下面的文件
        if(downloadFileType==kDownloadFileTypeModule)
        {
            reportError(downloader->getErrorMsg(),ResConstant::singleModuleDownloadError);
            //接着下载之后的文件
            onDownloadModuleFile();
        }
        else
        {
            reportError(downloader->getErrorMsg(), downloader->getErrorCode());
        }
    }
    else
    {
        //下载中，触发下载进度
        onProcess(downloader->getProcess());
    }
}



void ResourceManager::reportError(const std::string &errorMsg,const int errorCode)
{
    //解压出错，回调相关接口
    if(delegate)
    {
        delegate->onErrorOccured(errorMsg.c_str());
    }
    if (m_eScriptType != kScriptTypeNone&&scriptHandler)
    {
        executeScriptHandler(ResConstant::onErrorOccured.c_str(),errorCode, errorMsg.c_str());
    }
}

void ResourceManager::onProcess(float process)
{
    if (m_eScriptType != kScriptTypeNone&&scriptHandler)
    {
        executeScriptHandler("onProcess",ResConstant::ok, Convert2String(process).c_str());
    }
}



int ResourceManager::checkModule(const std::string &moduleName)
{
    if(isDevelopMode())
        return 0;
    string moduleConfigPath;
    moduleConfigPath.append(moduleName);
    moduleConfigPath.append(RMModuleConfigSuffix);
    moduleConfigPath = FileManager::splicePath(RMModuleConfigDirectory, moduleConfigPath);
    moduleConfigPath = FileManager::splicePath(RMModuleDirectory, moduleConfigPath);
    moduleConfigPath = fullLocalResourcePath(moduleConfigPath.c_str());
    if(!FileManager::fileExists(moduleConfigPath))
    {
        this->reportError("module not exist the full path is "+moduleConfigPath,ResConstant::moduleConfigNoexistError);
        return -1;
    }
    CCArray* downloadList = (CCArray*)downloadCache->objectForKey(moduleName);
    if(!downloadList)
    {
        downloadList=CCArray::create();
        JSONObject* moduleObj = JSONObject::createWithFile(moduleConfigPath);
        JSONObject* updateListObj = JSONObject::createWithFile(fullLocalResourcePath(RMUpdateListFile));
        CCArray* keyArray = moduleObj->allKeys();
        if (keyArray)
        {
            for (int i = 0; i < keyArray->count(); i++)
            {
                CCString* path = (CCString*)keyArray->objectAtIndex(i);
                if (updateListObj->has(path->getCString()))
                {
                    downloadList->addObject(path);
                }
            }
        }
        downloadCache->setObject(downloadList, moduleName);
    }
    if(downloadList->count()==0)
    {
        return 0;
    }
    return downloadList->count();
}


void ResourceManager::updateModule(const std::string &moduleName)
{
    if(isDevelopMode())
        return;
    currentModuleName=moduleName;
    //
    downloadModuleFile();
    
}

void ResourceManager::downloadModuleFile()
{
    CCArray* downloadList=(CCArray*)downloadCache->objectForKey(currentModuleName);
    if (downloadList == NULL||downloadList->count() == 0)
    {
        return;
    }
    //下载第一个文件
    CCString* filePath=(CCString*)downloadList->objectAtIndex(0);
    string downloadUrl=FileManager::splicePath(getRootURL(), RMServerResourceDirectory);
    downloadUrl=FileManager::splicePath(downloadUrl, filePath->getCString());
    downloader->setUrl(downloadUrl.c_str());
    //(2) 保存地址
    std::string savePath=this->fullLocalResourcePath(filePath->getCString());
    downloader->setFullPath(savePath.c_str());
    //(3) 下载模块文件单个
    this->downloadFileType = kDownloadFileTypeModule;
    downloader->download();
};

//模块文件更新后触发
void ResourceManager::onDownloadModuleFile()
{
    CCArray* downloadList = (CCArray*)downloadCache->objectForKey(currentModuleName);
    CCString* filePath = (CCString*)downloadList->objectAtIndex(0);
    //回调
    if (this->delegate)
    {
        this->delegate->onSingleFileUpdated(filePath->getCString());
    }
    if (m_eScriptType != kScriptTypeNone&&scriptHandler)
    {
        //回调脚本的方法
        executeScriptHandler(ResConstant::onSingleFileUpdated.c_str(),ResConstant::ok, filePath->getCString());
    }
    //从updateList中删除
    std::string updateListFile = this->fullLocalResourcePath(RMUpdateListFile);
    JSONObject* updateListObj=JSONObject::createWithFile(updateListFile);
    updateListObj->remove(filePath->getCString());
    //回写updateList
    FileManager::writeStringToFile(updateListObj->toPrettyString(), updateListFile);
    //从缓存列表中删除
    downloadList->removeObjectAtIndex(0);
    if (downloadList->count() == 0)
    {
        //模块文件更新完成
        if (this->delegate)
        {
            this->delegate->onModuleUpdated(this->currentModuleName);
        }
        if (m_eScriptType != kScriptTypeNone&&scriptHandler)
        {
            //触发脚本文件,模块更新完成
            executeScriptHandler(ResConstant::onModuleUpdated.c_str(),ResConstant::ok, this->currentModuleName.c_str());
        }
    }
    else
    {
        //接着下载其它的模块文件
        downloadModuleFile();
    }
}

int ResourceManager::executeScriptHandler(const char *type,const int errorCode, const char* param)
{
    if(scriptHandler)
    {
        int paramCount=0;
        CCLuaEngine* pEngine = CCLuaEngine::defaultEngine();
        CCLuaStack* pStack = pEngine->getLuaStack();
        pStack->pushString(type);
        paramCount++;
        pStack->pushInt(errorCode);
        paramCount++;
        if(param)
        {
            pStack->pushString(param);
            paramCount++;
        }
        int ret= pStack->executeFunctionByHandler(scriptHandler, paramCount);
        pStack->clean();
        return ret;
    }
    return 0;
}














