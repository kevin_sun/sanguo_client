//
//  HttpDownloader.cpp
//  casinogame
//
//  Created by 孙 伟 on 13-8-6.
//
//

#include <curl/curl.h>
#include <curl/easy.h>
#include <stdio.h>
#include "HttpDownloader.h"
#include "FileManager.h"
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <semaphore.h>
#include "cocos-ext.h"
using namespace std;
using namespace cocos2d;
USING_NS_CC_EXTRA;
// 临时文件后缀，下载完成后会通过rename方式去掉
#define FileTempSuffix ".tmp"

HttpDownloader::HttpDownloader()
:_fullPath("")
,_url("")
,_pTarget(NULL)
,_downloadCallBack(NULL)
,_totalSize(0)
,_process(0)
,_errorMsg("")
,_pFile(NULL)
,isDownloading(false)
,_errorCode(ResConstant::ok)
{
}

HttpDownloader::~HttpDownloader()
{
    _fullPath=NULL;
    _url=NULL;
    _downloadCallBack=NULL;
    _errorMsg=NULL;
    _pFile=NULL;
    if(_pTarget)
    {
        _pTarget->release();
    }
    CCDirector::sharedDirector()->getScheduler()->unscheduleAllForTarget(this);
}

void HttpDownloader::setUrl(const char *url)
{
    _url=url;
    isDownloading=false;
    _totalSize=0;
    _process=0;
}

const char* HttpDownloader::getUrl()
{
    return _url.getCString();
}
void HttpDownloader::setFullPath(const char *fullPath)
{
    _fullPath=fullPath;
}

void HttpDownloader::setDownloadCallBack(cocos2d::CCObject *pTarget, DownloadCallBack downloadCallBack)
{
    _pTarget=pTarget;
    _downloadCallBack=downloadCallBack;
    if(_pTarget)
    {
        _pTarget->retain();
    }
}

float HttpDownloader::getProcess()
{
    return _process;
}

const char* HttpDownloader::getErrorMsg()
{
    return _errorMsg.getCString();
}


bool HttpDownloader::download()
{
    if(!createFile())
    {
        doCallback(kDownloadError);
        return false;
    }
    if(_url.length()==0)
    {
        _errorCode=ResConstant::downloadUrlError;
        _errorMsg="要下载url为空";
        CCLOG("url not set");
        // 回调
        if(!_pFile)
        {
            fclose(_pFile);
        }
        doCallback(kDownloadError);
        return false;
    }
    isDownloading = true;
    //下载进度重置为0
    _process=0;
    CCHTTPRequest* request = CCHTTPRequest::createWithUrl(this, getUrl(), kCCHTTPRequestMethodGET);
    request->setDownloadFile(_pFile);
    request->start();
    return true;
}


bool HttpDownloader::createFile()
{
    if(_fullPath.length()==0)
    {
        _errorCode=ResConstant::createTmpFileError;
        _errorMsg="有设置要保存的文件路径";
        return false;
    }
    std::string tempFullPath=_fullPath.getCString();
    if(tempFullPath.at(tempFullPath.length()-1)=='/')
    {
        _errorCode=ResConstant::createTmpFileError;
        _errorMsg = "需要指定文件而不是目录";
        return false;
    }
    //创建目录，如果目录不存在的话
    if(!FileManager::createDirectory(tempFullPath))
    {
        _errorCode=ResConstant::createTmpFileError;
        _errorMsg.initWithFormat("创建目录错误, errno=%d", errno);
        return false;
    }
    //先写入到一个淩时文件
    tempFullPath.append(FileTempSuffix);
    _pFile=fopen(tempFullPath.c_str(), "wb");
    if (! _pFile)
    {
        CCLOG("errno=%d", errno);
        _errorCode=ResConstant::createTmpFileError;
        _errorMsg.initWithFormat("创建文件错误, errno=%d", errno);
        return false;
    }
    return true;
}

//这个方法就是在主线程里回调的，所以不需要再selector调用
void HttpDownloader::doCallback(DownloadResult result)
{
    isDownloading=false;
    _result=result;
    if(_pTarget&&_downloadCallBack)
    {
        (_pTarget->*_downloadCallBack)(this,_result);
    }
}


void HttpDownloader::requestFinished(CCHTTPRequest *request)
{
    //关闭文件
    fclose(_pFile);
    if(request->getResponseStatusCode()==200)
    {
        //文件下载成功，重命名
        std::string tempFile = _fullPath.getCString();
        tempFile.append(FileTempSuffix);
        std::string realFullPath=_fullPath.getCString();
        //文件移动，其实就是重命名
        if(!FileManager::moveFile(tempFile, realFullPath))
        {
            doCallback(kDownloadError);
            return;
        }
        CCLOG("succeed downloading package %s, to %s", _url.getCString(), _fullPath.getCString());
        // 回调
        _errorCode=ResConstant::ok;
        _errorMsg = "下载成功";
        //切换到callback
        doCallback(kDownloadSucceed);
    }
    else
    {
        deleteTempFile(request);
    }
}

void HttpDownloader::requestFailed(CCHTTPRequest *request)
{
    //关闭文件
    fclose(_pFile);
    deleteTempFile(request);
}

void HttpDownloader::deleteTempFile(CCHTTPRequest *request)
{
    //删除临时文件
    string tempFile = _fullPath.getCString();
    tempFile.append(FileTempSuffix);
    remove(tempFile.c_str());
    _errorCode=ResConstant::downloadFileError;
    _errorMsg.initWithFormat("下载失败，%s,当前链接是%s", request->getErrorMessage(),getUrl());
    doCallback(kDownloadError);
    return ;
}

