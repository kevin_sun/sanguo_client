
#ifndef __LUARESMANAGER_H_
#define __LUARESMANAGER_H_

extern "C" {
#include "lua.h"
#include "tolua++.h"
#include "tolua_fix.h"
}

TOLUA_API int luaopen_LuaResManager(lua_State* tolua_S);

#endif // __LUARESMANAGER_H_
