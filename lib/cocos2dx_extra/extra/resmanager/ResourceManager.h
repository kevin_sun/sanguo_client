//
//  ResourceManager.h
//  casinogame
//
//  Created by 孙 伟 on 13-8-8.
//
//

#ifndef _RESOURCE_MANAGER_H_
#define _RESOURCE_MANAGER_H_
#include "cocos2d.h"
#include "HttpDownloader.h"
#include "ResConstants.h"




USING_NS_CC;
using namespace std;

template<typename T>
string Convert2String(const T &value)
{
    stringstream ss;
    ss << value;
    return ss.str();
}
class ResourceUpdateDelegate :public cocos2d::CCObject{
    
public:
    
    virtual ~ResourceUpdateDelegate(){}
    
    /**
     * 资源版本检查回调
     * @param latest [true]客户端版本已经最新,不需要更新;[false]客户端版本已过时,需要更新
     */
    virtual void onVersionCheckCompleted(bool latest){};
    
    /**
     * 版本更新完毕
     */
    virtual void onVersionUpdated(){};
    
    /**
     * 模块更新完毕
     */
    virtual void onModuleUpdated(const std::string& moduleName){};
    
    /**
     * 单个文件更新的回调
     */
    virtual void onSingleFileUpdated(const std::string& filePath){};
    
    /**
     * 资源检查、更新过程中出错的回调
     * @param 错误消息
     */
    virtual void onErrorOccured(const char* message){};
    /**
     * 资源检查、更新过程中出错的回调
     * @param 错误消息
     */
};

class ResourceManager :public CCObject
{
public:
    static ResourceManager* sharedResourceManager();
    
    
    virtual ~ResourceManager();
    
    /**
     * 设置、获取资源更新事件委托
     */
    CC_SYNTHESIZE_RETAIN(ResourceUpdateDelegate*, delegate, Delegate);
    /**
     * 设置、获取资源更新事件委托（脚本）
     */
    CC_SYNTHESIZE(int, scriptHandler, ScriptHandler);
    /**
     * 设置资源根地址
     */
    void setRootURL(const string& rootUrl);
    /**
     * 获取资源根地址
     */
    const char* getRootURL();
    /**
     * 版本是否更新
     */
    bool isVersionLatest();
    /**
     * 是否是开发模式
     */
    bool isDevelopMode();
    /**
     * 更新SearchPath设置
     */
    void configSearchPath();
    /**
     * 检查版本号
     * 异步接口，检查完毕后会回调delegate的versionCheckCompleted接口
     */
    void checkVersion();
    /*
     * 更新版本
     */
    void updateVersion();
    /**
     * 检查模块资源版本
     * @return 需要更新的文件数量，如果是0表示模块不需要更新
     */
    int checkModule(const std::string& moduleName);
    
    /**
     * 更新模块资源
     */
    void updateModule(const std::string& moduleName);
    /**
     *  下载器下载结果回调
     */
    void onDownloaderResult(HttpDownloader* downloader,DownloadResult result);
    /**
     * 获取版本
     */
    CC_SYNTHESIZE_READONLY(float, localVersion, Version);
    
    //保存版本号
    void saveVersion(float version);
    
private:
    ResourceManager();
    enum DownloadFileType {
        kDownloadFileTypeVersionFile,               //version说明文件下载
        kDownloadFileTypeUpdatePackage,             //全局update包下载
        kDownloadFileTypeUpdateListPackage,         //更新列表包下载
        kDownloadFileTypeModule,                     //模块资源下载
        kDownloadDefault                            //权举默认值
    };
    /**
     * 注：与资源服务器生成的updateList保持一致，不可随意改动
     */
    enum FileOperateType{
        kFileOperateTypeCreate=0,                   //创建
        kFileOperateTypeUpdate=1,                   //更新
        kFileOperateTypeDelete=2                    //删除
    };
    std::string rootUrl;                            //资源根地址
    HttpDownloader* downloader;                     //下载器
    DownloadFileType downloadFileType;              //操作类型 (回调的绑断)
    float serverResourceVersion;                    //服务端资源版本
    bool versionLatest;                             //版本是否最新的标志
    std::string currentModuleName;                  //当前正在更新的模块名
    cocos2d::CCDictionary*  downloadCache;          //待更新模块资源缓存 k-模块名  v-CCArray
    ccScriptType m_eScriptType;
    //下载服务器版本
    void downloadServerVersion();
    void onDownloadServerVersion();
    //下载更新列表
    void downloadUpdateList(float versionOld,float versionNew);
    //在模块更新列表后，回调
    void onDownloadUpdateList();
    //下载模块文件
    void downloadModuleFile();
    //单个模块文件下载完成后的处理
    void onDownloadModuleFile();
    //获取文件的绝对路径(本地资源目录下)
    const char* fullLocalResourcePath(const std::string& filePath);
    //报告错误，对delegate和scriptDelegate进行回调
    void reportError(const std::string& errorMsg,const int errorCode);
    //全局更新文件下载完后触发
    void onUpdateVersion();
    void onProcess(float process);
    int executeScriptHandler(const char* type,const int errorCode=ResConstant::ok,const char* param=NULL);
};


#endif
