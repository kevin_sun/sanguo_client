local facade = require("comlib.mvc.Facade")
local playerCommand = require("module.player.PlayerCommand")
local mainCommand = require("module.main.MainCommand")
local taskCommand = require("module.task.TaskCommand")
local activityCommand = require("module.activity.ActivityCommand")
local AppFacade=class("AppFacade",facade)

function AppFacade:startup()
	--添加资源搜索目录
	self:addFramesCache()
	--注册command,也就是Controller
	self:registerCommand(playerCommand)
	self:registerCommand(mainCommand)
	self:registerCommand(taskCommand)
	self:registerCommand(activityCommand)
	--注册切换回调
	LuaEventHandler:createAppHandler(handler(self,self.onAppForegroundAndBackgroudEnter))
	--登录首页
	self:sendNotification(playerCommand.LoginIndex)
end

function AppFacade:enterScene(sceneName,...)
	local scene = require(sceneName).new(...)
	display.replaceScene(scene,"fade", 0.6, display.COLOR_WHITE)
end

function AppFacade:getRunningScene()
	return display.getRunningScene()
end

function AppFacade:addFramesCache()
	CCFileUtils:sharedFileUtils():addSearchPath("res/")
	display.addSpriteFramesWithFile(GAME_TEXTURE_DATA_FILENAME_LOGIN, GAME_TEXTURE_IMAGE_FILENAME_LOGIN)
	display.addSpriteFramesWithFile(GAME_TEXTURE_DATA_FILENAME_LOADING, GAME_TEXTURE_IMAGE_FILENAME_LOADING)
	display.addSpriteFramesWithFile(GAME_TEXTURE_DATA_FILENAME_NEWPLAYER, GAME_TEXTURE_IMAGE_FILENAME_NEWPLAYER)
	--
	display.addSpriteFramesWithFile(GAME_TEXTURE_DATA_FILENAME_CONTROL_1, GAME_TEXTURE_IMAGE_FILENAME_CONTROL_1)
	display.addSpriteFramesWithFile(GAME_TEXTURE_DATA_FILENAME_CONTROL_2, GAME_TEXTURE_IMAGE_FILENAME_CONTROL_2)
	--
	display.addSpriteFramesWithFile(GAME_TEXTURE_DATA_FILENAME_ICON, GAME_TEXTURE_IMAGE_FILENAME_ICON)
	display.addSpriteFramesWithFile(GAME_TEXTURE_DATA_FILENAME_WORD, GAME_TEXTURE_IMAGE_FILENAME_WORD)
	--
	display.addSpriteFramesWithFile(GAME_TEXTURE_DATA_FILENAME_HEROBOX, GAME_TEXTURE_IMAGE_FILENAME_HEROBOX)
end

--游戏数据
AppFacade.player = require("module.player.PlayerModel")

--app 前后台切换时调用
function AppFacade:onAppForegroundAndBackgroudEnter(e)
	if e == kLuaEventAppEnterBackground then
	elseif e == kLuaEventAppEnterForeground then
	end
end


return AppFacade