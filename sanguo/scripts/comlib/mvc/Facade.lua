local observer = require("comlib.mvc.Observer")
local model=require("comlib.mvc.Model")
local controller=require("comlib.mvc.Controller")
local Facade = class("Facade")

--发送通知
function Facade:sendNotification(name,body)
	observer:sendNotification({name=name,data=body})
end
--别名类
function Facade:notify(name,body)
	self:sendNotification(name,body)
end

--注册command相关
function Facade:registerCommand(name,command)
	controller:registerCommand(name,command)
end

--删除command
function Facade:removeCommand(name)
	controller:removeCommand(name)
end

function Facade:hasCommand(name)
	return controller:hasCommand(notifnameicationName)
end

function Facade:registerProxy(proxy)
	model:registerProxy(proxy)
end

function Facade:retrieveProxy(name)
	return model:retrieveProxy(name)
end

function Facade:hasProxy(name)
	return model:hasProxy(name)
end

function Facade:removeProxy(name)
	model:removeProxy(name)
end


return Facade