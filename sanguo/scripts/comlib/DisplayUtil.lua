local DisplayUtil = class("DisplayUtil")

--获取配置的信息
function DisplayUtil:getConfigPosition(fileName,isRelative)
	if fileName then
		if string.byte(fileName) == 35 then --start with #
			fileName = string.sub(fileName,2)
		end
		if GamePosInfo and GamePosInfo[fileName]~=nil then
			local pos = clone(GamePosInfo[fileName])
			pos.x = pos.x/contentScaleFactor
			pos.y = pos.y/contentScaleFactor
			if not isRelative then
				pos.x = display.cx + pos.x
				pos.y = display.cy + pos.y
			end
			return pos
		end
	end
	return nil
end

--从坐标配置文件创建sprite
function DisplayUtil:newSprite(fileName,x,y,isRelative)
	if x and y then
		return display.newSprite(fileName,x,y)
	else
		local posInfo=self:getConfigPosition(fileName,isRelative)
		return display.newSprite(fileName,posInfo.x,posInfo.y)
	end
end

--创建带动画的sprite
function DisplayUtil:newSpriteWithAni(fileName,pattern,begin,length,time,isForover,isReversed,isRelative)
	local frames = display.newFrames(pattern, begin, length,isReversed)
	local posInfo=self:getConfigPosition(fileName,isRelative)
	local sprite=nil
	if posInfo then
		sprite = display.newSprite(frames[1],posInfo.x,posInfo.y)
	else
		sprite = display.newSprite(frames[1])
	end
	local animation = display.newAnimation(frames, time)
	if isForover and isForover==true then
		transition.playAnimationForever(sprite, animation)
	else
		transition.playAnimationOnce(sprite,animation,true)
	end
	return sprite
end

--创建按饭
function DisplayUtil:newImageMenuItem(params,isRelative)
	local posInfo=self:getConfigPosition(params.image,isRelative)
	if posInfo then
		params.x = posInfo.x
		params.y = posInfo.y
	end
	return ui.newImageMenuItem(params)
end

--创建进度文本
function DisplayUtil:newTTFLabel(params,name,isRelative)
	local posInfo = self:getConfigPosition(name,isRelative)
	if posInfo then
		params.x = posInfo.x
		params.y = posInfo.y
	end
	return ui.newTTFLabel(params)
end


function DisplayUtil:newEditBox(params,isRelative)
	local posInfo = self:getConfigPosition(params.image,isRelative)
	if posInfo then
		params.x = posInfo.x
		params.y = posInfo.y
		params.size = CCSize(posInfo.width,posInfo.height)
	end
	return ui.newEditBox(params)
end


function DisplayUtil:newControlButton(params)
	local posInfo = self:getConfigPosition(params.image,isRelative)
	if posInfo then
		params.x = posInfo.x
		params.y = posInfo.y
	end
	local scale9Sprite = display.newScale9Sprite(params.image)
	CCLuaLog(scale9Sprite:getContentSize().height)
	local button = CCControlButton:create(scale9Sprite)
	if params.x and params.y then
		button:setPosition(CCPoint(params.x,params.y))
	end
	if params.listener then
		button:addHandleOfControlEvent(params.listener,CCControlEventTouchUpInside)
	end
	return button
end

return DisplayUtil