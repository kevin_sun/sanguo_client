local TabControl = class("TabControl",function()
	return display.newLayer()
end)

TabControl.selectEvent="onSelect"

function TabControl:ctor(tabinfos,marginLeft,tabSpace,contentSize)
	self.curIndex = 0
	self.tabinfos = tabinfos
	self.tabs = {}
	local menuItems = {}
	local unselectedImageSize = display.newSprite("#tab_unselected.png"):getContentSize()
	local selectedImageSize = display.newSprite("#tab_selected.png"):getContentSize()
	local tabX = marginLeft + unselectedImageSize.width / 2
	local tabY = unselectedImageSize.height / 2 + contentSize.height
	for i=1,#tabinfos do
		local tabinfo = tabinfos[i]
		if i == 1 then
			tabinfo.x = tabX
		else
			tabX = tabX + unselectedImageSize.width + tabSpace
			tabinfo.x = tabX
		end
		tabinfo.y = tabY
		local tab = 
			{	index = i,
				menuitem = ui.newImageMenuItem({image = "#tab_unselected.png",imageSelected = "#tab_selected.png",listener = handler(self,self.switchTab),tag = i,x = tabinfo.x,y = tabinfo.y}),
				menuitemLabel = ui.newTTFLabel({text = tabinfo.text,color = tabinfo.color,x = tabinfo.x ,y = tabinfo.y - 10,align = ui.TEXT_ALIGN_CENTER})
			}
		menuItems[i] = tab.menuitem
		self.tabs[i] = tab
	end
	--添加tab的button
	self.tabMenu = ui.newMenu(menuItems)
	self:addChild(self.tabMenu)
	--添加tab显示的文字标签
	for i=1,#self.tabs do 
		self:addChild(self.tabs[i].menuitemLabel)
	end
	--添加内容和tab按钮的分隔线
	--local lineSprite = display.newSprite("#line.png")
	--local lineSize = lineSprite:getContentSize:getContentSize()
	--local fullRect = CCRectMake(0,0, lineSize.width, lineSize.height)
	--local insetRect = CCRectMake(10,0,lineSize.width - 10*2,lineSize.height)
	--lineSprite:release()
	local lineSize = CCSizeMake(640,4)
	local lineSprite = display.newScale9Sprite("#line.png",contentSize.width/2,contentSize.height + lineSize.height / 2 - 1,lineSize)
	self:addChild(lineSprite)
	--添加view的容器
	self.container = CCLayerGradient:create(ccc4(130,22,0,255),ccc4(43,1,0,255),CCPoint(0,-1))
	self.container:setContentSize(contentSize)
	self.container.curView = nil
	self:addChild(self.container)
	--扩展事件
	require("framework.api.EventProtocol").extend(self)
	self:switchTab(1)
	
	
end

function TabControl:addTab(index,tabView)
end

function TabControl:switchTab(index)
	if index < 1 or index > #self.tabs then
		return
	else
		if self.curIndex == index then
			return
		end
		if self.tabs[index] then
			if self.curIndex > 0 then
				local tabinfo = self.tabinfos[self.curIndex]
				self.tabs[self.curIndex].menuitem:unselected()
				self:removeChild(self.tabs[self.curIndex].menuitemLabel)
				self.tabs[self.curIndex].menuitemLabel = ui.newTTFLabel({text = tabinfo.text,color = tabinfo.color,x = tabinfo.x ,y = tabinfo.y - 10,align = ui.TEXT_ALIGN_CENTER})
				self:addChild(self.tabs[self.curIndex].menuitemLabel)
				if self.container.curView then
					self.container:removeChild(self.container.curView)
					self.container.curView = nil
				end
			end
			self.tabs[index].menuitem:selected()
			self.curIndex = index
			self:removeChild(self.tabs[index].menuitemLabel)
			self.tabs[index].menuitemLabel = ui.newTTFLabelWithShadow({text = self.tabinfos[index].text,color = self.tabinfos[index].selectedColor,shadowColor = self.tabinfos[index].shadowColor,x = self.tabinfos[index].x,y = self.tabinfos[index].y - 10,align = ui.TEXT_ALIGN_CENTER})
			self:addChild(self.tabs[index].menuitemLabel)
			local tabinfo = self.tabinfos[index]
			if tabinfo.view then
				self.container.curView = require(tabinfo.view).new()
				self.container.curView:setPosition(CCPoint(tabinfo.viewX,tabinfo.viewY))
				self.container:addChild(self.container.curView)
			end
			--抛出事件
			self:dispatchEvent({name=self.selectEvent,data=index})
		end
	end
end

return TabControl