local eventProtocol = require("framework.api.EventProtocol")
local RadioButton = class("RadioButton",function () 
	return display.newNode()
	end
)

RadioButton.selectEvent="onSelect"

function RadioButton:ctor(...)
	CCLuaLog(select("#", ...))
	self.selectedIndex=0
	self.items = {}
	local index = 1
	local argNum = select("#", ...)
	for i=1,argNum do
		local v = select(i,...)
		if v.bgName or v.clippingName then
			--添加背景
			if v.bgName then
				local bgSprite = displayUtil:newSprite(v.bgName,nil,nil,true)
				self:addChild(bgSprite)
			end
			if v.clippingName then
				self.clippingName = v.clippingName
			end
		else
			v.tag = index
			v.listener = handler(self,self.onSelect)
			local menuItem = displayUtil:newImageMenuItem(v,true)
			self.items[index] = menuItem
			if self.selectedIndex == 0 and v.default then
					self.selectedIndex = index
					menuItem:selected()
			end
			index = index + 1 
		end		
	end
	eventProtocol.extend(self)
	--
	self:addChild(ui.newMenu(self.items))
end

function RadioButton:onSelect(itemTag,target)
	if self.items[itemTag] then
		if self.selectedIndex > 0 then
			self.items[self.selectedIndex]:unselected()
			if self.clippingSprite then
				self:removeChild(self.clippingSprite)
				self.clippingSprite = nil
			end
		end
		self.items[itemTag]:selected()
		if self.clippingName then
			self.clippingSprite = display.newSprite(self.clippingName)
			self.clippingSprite:setPosition(self.items[itemTag]:getPosition())
		end
		self.selectedIndex = itemTag
		--抛出事件
		self:dispatchEvent({name=self.selectEvent,data=itemTag})
	end
end

function RadioButton:cancelSelected()
	if self.selectedIndex > 0 then
		if self.clippingSprite then
			self:removeChild(self.clippingSprite,true)
		end
		self.items[self.selectedIndex]:unselected()
		self.selectedIndex = 0
	end
end

return RadioButton