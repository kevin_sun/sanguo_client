local ScrollView =class("ScrollView",function (rect)
	if not rect then 
		rect=CCRectMake(0,0,0,0)
	end
	local node=display.newClippingRegionNode(rect)
	node:setNodeEventEnabled(true)
	require("framework.api.EventProtocol").extend(node)
	return node
end)

ScrollView.DIRECTION_VERTICAL = 1
ScrollView.DIRECTION_HORIZONTAL = 2
ScrollView.SCROLL_TO_VALID_RANGE_SPEED=400
ScrollView.SWIPING_CACEL_TIME=0.034
ScrollView.SWIPING_SPEED_MAX=2200
ScrollView.priority = -129

ScrollView.cellClickEvent="onCellClick"

function ScrollView:ctor(rect,saleFactor,direction,magrin)
	assert(direction==ScrollView.DIRECTION_VERTICAL or direction==ScrollView.DIRECTION_HORIZONTAL,
"ScrollView:ctor() ---invalid direction")
	if magrin then
		self.magrin =magrin
	else
		self.magrin = 0
	end
	self.frameTime=0
	self.dragThreshold = 10
    self.bouncThreshold = 140
    self.defaultAnimateTime = 0.4
    self.defaultAnimateEasing = "backOut"
    self.direction=direction
    self.touchRect=rect
    if direction==ScrollView.DIRECTION_HORIZONTAL then
    	--水平滑动的scrollview
    	self.offsetX = 0
   		self.offsetY = 0
    else
    	self.offsetX = 0 
    	self.offsetY = rect.size.height
    end
   	self.cells={}
   	self.currentIndex=0
   	self.view=display.newLayer()
   	if direction == ScrollView.DIRECTION_VERTICAL then
   		self.view:setPosition(CCPoint(0,rect.size.height))
   	end
   	self:addChild(self.view)
   	self.view:addTouchEventListener(handler(self,self.onTouch),false,self.priority,true)
    self.view:setTouchEnabled(true)
    self:scheduleUpdate(function(delt) self:update(delt) end)
end


function ScrollView:onExit()
	self:removeAllEventListeners()
end

function ScrollView:getCurrentCell()
	if self.currentIndex >0 then
		return self.cells[self.currentIndex]
	else
		return nil
	end
end

function ScrollView:getCurrentIndex()
	return self.currentIndex
end

function ScrollView:setCurrentIndex(index)
    self:scrollToCell(index)
end

function ScrollView:addCell(cell)
	self.view:addChild(cell)
	self.cells[#self.cells + 1] = cell
	self:reorderAllCells()
end

function ScrollView:reorderAllCells()
	local count=#self.cells
	local x,y=0,0
	local maxWidth, maxHeight = 0, 0
	local contentWidth,contentHeight =0,0
	for i=1,count do
		local cell=self.cells[i]
		if self.direction==ScrollView.DIRECTION_HORIZONTAL then
			if i == 1 then
				x = cell:getItemWidth()/2
				y = cell:getItemHeight()/2
			end
			cell:setPosition(CCPoint(x,y))
			local cellWidth=cell:getItemWidth()
			x=x+cellWidth
			maxWidth=math.max(cellWidth,maxWidth)
			contentWidth = contentWidth + cell:getItemWidth() + self.magrin
		else
			if i == 1 then
				x = cell:getItemWidth()/2
				y = -cell:getItemHeight()/2 - self.magrin
			end
			cell:setPosition(CCPoint(x,y))
			local cellHeight=cell:getItemHeight()
			y=y-cellHeight - self.magrin
			maxHeight=math.max(cellHeight,maxHeight)
			contentHeight = contentHeight + cell:getItemHeight() + self.magrin
		end
	end
	if count>0 then
		if self.currentIndex < 1 then
			self.currentIndex=1
		elseif self.currentIndex>count then
			self.currentIndex=count
		end
	else
		self.currentIndex=0
	end
	--
	local containerSize=nil
	if self.direction==ScrollView.DIRECTION_HORIZONTAL then
		containerSize=CCSize(contentWidth,maxHeight)
	else
		containerSize=CCSize(maxWidth,contentHeight)
	end
	self.view:setContentSize(containerSize)
end


function ScrollView:setContentOffset(offset,animated,time,easing)
	local ox,oy=self.offsetX,self.offsetY
	local x,y=ox,oy
	if self.direction==ScrollView.DIRECTION_HORIZONTAL then
		self.offsetX=offset
		x=offset
		local maxOffsetX=self:getMaxOffset()
		local minOffsetX=self:getMinOffset()
		x=math.min(maxOffsetX,x)
		x=math.max(minOffsetX,x)
	else
		self.offsetY=offset
		y=offset
		local maxY=self:getMaxOffset()
		local minY=self:getMinOffset()
		y=math.min(maxY,y)
		y=math.max(minY,y)
	end
	if animated then 
		transition.stopTarget(self.view)
		transition.moveTo(self.view, {
            x = x,
            y = y,
            time = time or self.defaultAnimateTime,
            easing = easing or self.defaultAnimateEasing,
        })
	else
		self.view:setPosition(CCPoint(x,y))
	end
end

function ScrollView:getMaxOffset()
	if self.direction==ScrollView.DIRECTION_HORIZONTAL then
		return self.bouncThreshold
	else
		return self.view:getContentSize().height+self.bouncThreshold
	end
end

function ScrollView:getMinOffset()
	if self.direction==ScrollView.DIRECTION_HORIZONTAL then
		return -self.view:getContentSize().width-self.bouncThreshold+self.touchRect.size.width
	else
		return self.touchRect.size.height-self.bouncThreshold
	end
end

function ScrollView:scrollToCell(index, animated, time, easing)
	local count=#self.cells
	if count < 1 then
		self.currentIndex=0
		return
	end
	index=math.max(1,index)
	index=math.min(index,count)
	self.currentIndex=index
	local offset=0
	for i=2,index do
		local cell = self.cells[i - 1]
		local size = cell:getContentSize()
		if self.direction == ScrollView.DIRECTION_HORIZONTAL then
			offset=offset-size.width
		else
			offset=offset+size.height
		end
	end
	self:setContentOffset(offset, animated, time, easing)
end



function ScrollView:onTouch(event,x,y)
	if event==CCTOUCHBEGAN then
		return self:onBegan(event, x,y)
	elseif event==CCTOUCHMOVED then
		return self:onTouchMoved(event, x,y)
	elseif event==CCTOUCHENDED then
		return self:onTouchEnded(event, x,y)
	else
		return self:onTouchCanceled(event, x,y)
	end
end

function ScrollView:onBegan(event,x,y)
	local curPoint=self:convertToNodeSpace(CCPoint(x,y))
	--验证是否在可以划动的区哉中
	if not self.touchRect:containsPoint(curPoint) then
		return false
	end
	self.touch=
	{
		offsetX=self.offsetX,
		offsetY=self.offsetY,
		startX=x,
		startY=y,
		isMaybeTap=(self.swiping==nil),
		lastX=x,
		lastY=y,
		isMaybeSwiping=false,
		swipingBeganTime=0,
		swipingBeganX=0,
		swipingBeganY=0,
		lastTouchTime=self.frameTime
	}
	self.swiping=nil --一旦触动就停掉自动转动
	return true
end

function ScrollView:onTouchMoved(event,x,y)
	local curPoint=self:convertToNodeSpace(CCPoint(x,y))
	--验证是否在可以划动的区哉中
	if not self.touchRect:containsPoint(curPoint) then
		return false
	end
	if self.direction==ScrollView.DIRECTION_HORIZONTAL and self.touch.lastX==x then
		self.touch.isMaybeSwiping=false
	elseif self.direction==ScrollView.DIRECTION_VERTICAL and self.touch.lastY==y then
		self.touch.isMaybeSwiping=false
	else
		if self.touch.isMaybeSwiping then
			if self.frameTime-self.touch.lastTouchTime>ScrollView.SWIPING_CACEL_TIME then
				--中间有停留，则滑动状态取消
				self.touch.isMaybeSwiping=false
				self.touch.swipingBeganTime=0
				self.touch.swipingBeganX=0
				self.touch.swipingBeganY=0
			end
		else
			self.touch.isMaybeSwiping=true
			self.touch.swipingBeganTime=self.frameTime
			self.touch.swipingBeganX=x
			self.touch.swipingBeganY=y
		end
	end
	if self.touch.isMaybeTap then
		if self.direction==ScrollView.DIRECTION_HORIZONTAL then
			if math.abs(x-self.touch.startX) >=self.dragThreshold then
				self.touch.isMaybeTap=false
			end
		else
			if math.abs(y-self.touch.startY) >=self.dragThreshold then
				self.touch.isMaybeTap=false
			end
		end
	end
	local lastPoint=CCPoint(self.touch.lastX,self.touch.lastY)
	local newPoint=CCPoint(x,y)
	local moveDistance=ccpSub(newPoint,lastPoint)
	local newoffset=nil
	if self.direction==ScrollView.DIRECTION_HORIZONTAL then
		newoffset=self.view:getPositionX()+moveDistance.x
	else
		newoffset=self.view:getPositionY()+moveDistance.y
	end
	self:setContentOffset(newoffset)
	self.touch.lastX=x
	self.touch.lastY=y
	--滑动的最后时间
	self.touch.lastTouchTime=self.frameTime
end

function ScrollView:onTouchEnded(event,x,y)
	if self.touch.isMaybeSwiping then
		if self.frameTime-self.touch.lastTouchTime > ScrollView.SWIPING_CACEL_TIME then
			--停顿了一定时间后，滑动状态取消
			self.touch.isMaybeSwiping=false
		end 
	end
	if self.touch.isMaybeTap then
		self:onCellTap(event,x,y)
	else
		if self:isRedressView() then
			self:redressView()
		elseif self.touch.isMaybeSwiping then
			local time=self.frameTime-self.touch.swipingBeganTime
			if time>0 then
				if self.direction==ScrollView.DIRECTION_HORIZONTAL then
					local offset=x-self.touch.swipingBeganX
					local speed=offset/time
					self.swiping={speed = speed}
				else
					local offset=y-self.touch.swipingBeganY
					local speed=offset/time
					self.swiping={speed = speed}
				end
				if math.abs(self.swiping.speed)>ScrollView.SWIPING_SPEED_MAX then
					if self.swiping.speed >0 then
						self.swiping.speed=ScrollView.SWIPING_SPEED_MAX
					else
						self.swiping.speed=0-ScrollView.SWIPING_SPEED_MAX
					end
				end
				CCLuaLog("self.swiping.speed "..tostring(self.swiping.speed))
			end
		end
	end
end

function ScrollView:redressView()
		--矫正位置，如果拉的offset超过限定值
		if self.direction==ScrollView.DIRECTION_HORIZONTAL then
			if self.view:getPositionX() > (self:getMaxOffset()-self.bouncThreshold) then
				self:setContentOffset(self:getMaxOffset()-self.bouncThreshold,true)
			elseif self.view:getPositionX() < (self:getMinOffset()+self.bouncThreshold) then
				self:setContentOffset(self:getMinOffset()+self.bouncThreshold,true)
			end
		else
			if self.view:getPositionY() < (self:getMinOffset()+self.bouncThreshold) then
				self:setContentOffset(self:getMinOffset()+self.bouncThreshold,true)
			elseif self.view:getPositionY() > (self:getMaxOffset()-self.bouncThreshold) then
				self:setContentOffset(self:getMaxOffset()-self.bouncThreshold,true)
			end	
		end	
end

--是否需要矫正位置
function ScrollView:isRedressView()
	if self.direction==ScrollView.DIRECTION_HORIZONTAL then
		return self.view:getPositionX() > (self:getMaxOffset()-self.bouncThreshold) or self.view:getPositionX() < (self:getMinOffset()+self.bouncThreshold)
	else
		return self.view:getPositionY() < (self:getMinOffset()+self.bouncThreshold) or self.view:getPositionY() > (self:getMaxOffset()-self.bouncThreshold)
	end
end

function ScrollView:update(delt)
	self.frameTime=self.frameTime+delt
	if not self.swiping then
		return
	end
	local newoffset=0
	local moving=self.swiping.speed*delt
	if self.direction==ScrollView.DIRECTION_HORIZONTAL then
		newoffset=self.view:getPositionX()+moving
	else
		newoffset=self.view:getPositionY()+moving
	end
	self:setContentOffset(newoffset)
	if math.abs(moving)<1 then
		self.swiping=nil
		self:redressView()
	else
		self.swiping.speed=self.swiping.speed*0.9
	end
end

function ScrollView:onCellTap(event,x,y)
	--根据坐标判断哪上scrollview被点中
	CCLuaLog(x.." "..y)
end


function ScrollView:onTouchCanceled(event,x,y)
end

return ScrollView