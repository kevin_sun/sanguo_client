local ScrollCell=class("ScrollCell",function (filename)
	local sprite=display.newSprite(filename)
	eventProtocol.extend(sprite)
	return sprite
end)

function ScrollCell:getItemWidth()
	return self:getContentSize().width
end

function ScrollCell:getItemHeight()
	return self:getContentSize().height
end

function ScrollCell:setId(id)
	self.cellId = id
end

function ScrollCell:getId()
	return self.cellId
end

function ScrollCell.extend(target)
	eventProtocol.extend(target)
	function target:getItemWidth()
		return self:getContentSize().width
	end
	
	function target:getItemHeight()
		return self:getContentSize().height
	end
	
	function target:setId(id)
		self.cellId = id
	end
	
	function target:getId()
		return self.cellId
	end
end

return ScrollCell



