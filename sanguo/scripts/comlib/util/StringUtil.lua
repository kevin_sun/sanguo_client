local StringUtil = {}
--查找最后一个字符的索引
function StringUtil:findLast(str1,str2)
	local pos = 0
	local lastPos = nil
	while(true) do
		pos,_ = string.find(str1,str2,pos+1)
		if pos then
			lastPos = pos
		else
			break
		end
	end
	return lastPos
end

return StringUtil