--加载quick-cocos2dx的配置
require("config.Config")

--加载quick-cocos2dx
require("framework.init")

--加载所有ui的全局坐标信息
require("config.GamePosInfo")

--util类，用于处理sprite坐标相关
displayUtil=require("comlib.DisplayUtil")

--全局内容缩放因子
contentScaleFactor =  CCDirector:sharedDirector():getContentScaleFactor()

--事件扩展
eventProtocol = require("framework.api.EventProtocol")

--加载程序的入口
appFacade = require("AppFacade")

httpConnector = require("net.HttpConnector")

stringUtil = require("comlib.util.StringUtil")