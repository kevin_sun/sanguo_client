local LayerManager = class("LayerManager")
LayerManager.LAYER_TYPE_BG = "backgroud"
LayerManager.LAYER_TYPE_PLAYER = "player"
LayerManager.LAYER_TYPE_MAINMENU = "mainmenu"
LayerManager.LAYER_TYPE_GAME = "gamelayer"
LayerManager.allLayers = {}

function LayerManager:createMultiLayer(curScene)
	self.curScene = curScene
	--默认层
	self.allLayers = {
		bgLayerOne = display.newColorLayer(ccc4(2,33,64,255)),bgLayerTwo = CCLayerGradient:create(ccc4(130,22,0,255),ccc4(43,1,0,255),CCPoint(0,-1)),
		mainMenuLayer = require("module.main.view.MainMenuLayer").new(),playerInfoLayer = require("module.player.view.PlayerInfoLayer").new(),
		simplePlayerInfoLayer = require("module.player.view.PlayerInfoLayer").new(true)
	}
	for k, v in pairs(self.allLayers) do
		v:retain()
	end
	--层的初始位置
	self.curBackGroudLayer = {name = nil,layer = nil,zOrder = 1}
	self.curMainMenuLayer = {name = nil,layer = nil,zOrder = 10}
	self.curPlayerInfoLayer = {name = nil,layer = nil,zOrder =10}
	self.curGameLayer = {name = nil,layer = nil,zOrder = 2}
	self.popLayer = {name = nil,layer = nil ,zOrder = 21}
	self.popMaskLayer = {name = nil,layer = nil,zOrder = 20}
end

function LayerManager:popup(popWin,x,y)
	if not x or not y then
		x = display.cx
		y = display.cy
	end
	if popWin == nil and self.popLayer.name ~=nil then
		self.curScene:removeChild(self.popLayer.layer)
		self.curScene:removeChild(self.popMaskLayer.layer)
		self.popLayer.name = nil
		self.popLayer.layer = nil
		self.popMaskLayer.name = nil
		self.popMaskLayer.layer = nil
	end
	if self.popLayer.name ~= popWin then
		if self.popLayer.name ~= nil then
			self.curScene:removeChild(self.popLayer.layer)
			self.popLayer.name = nil
			self.popLayer.layer = nil
		end
		if self.popMaskLayer.name == nil then
			self.popMaskLayer.name = "popMaskLayer"
			self.popMaskLayer.layer = display.newColorLayer(ccc4(73,73,73,200))
			self.curScene:addChild(self.popMaskLayer.layer,self.popMaskLayer.zOrder)
		end
		self.popLayer["name"] = popWin
		self.popLayer["layer"] = require(popWin).new()
		self.popLayer.layer:setPosition(CCPoint(x,y))
		self.curScene:addChild(self.popLayer["layer"],self.popLayer["zOrder"])
	end
end

function LayerManager:switchPlayerBar(name)
	self:_switch(self.LAYER_TYPE_PLAYER,name)
end

function LayerManager:switchMainMenu(name)
	self:_switch(self.LAYER_TYPE_MAINMENU,name)
end

function LayerManager:switchBg(name)
	self:_switch(self.LAYER_TYPE_BG,name)
end


function LayerManager:_switchGameLayer(gameLayer)
	if gameLayer == nil and self.curGameLayer["name"] ~= nil then
		self.curScene:removeChild(self.curGameLayer["layer"])
		self.curGameLayer["name"] = nil
		self.curGameLayer["layer"] = nil
	end 
	if self.curGameLayer["name"] ~= gameLayer then
		if self.curGameLayer["name"] ~= nil then
			self.curScene:removeChild(self.curGameLayer["layer"])
			self.curGameLayer["name"] = nil
			self.curGameLayer["layer"] = nil
		end
		self.curGameLayer["name"] = gameLayer
		self.curGameLayer["layer"] = require(gameLayer).new()
		self.curScene:addChild(self.curGameLayer["layer"],self.curGameLayer["zOrder"])
	end
end

function LayerManager:_switch(type,name)
	local curScene = self.curScene
	local curLayer = nil
	if type == self.LAYER_TYPE_BG then
		curLayer = self.curBackGroudLayer
	elseif type == self.LAYER_TYPE_PLAYER then
		curLayer = self.curPlayerInfoLayer
	else 
		curLayer = self.curMainMenuLayer
	end
	if name == nil then
		if curLayer["name"] ~= nil then
			curScene:removeChild(curLayer["layer"])
			curLayer["name"] = nil
			curLayer["layer"] = nil
		end
	else
		if curLayer["name"] == nil then
			curLayer["name"] = name
			curLayer["layer"] = self.allLayers[name]
			curScene:addChild(curLayer["layer"],curLayer["zOrder"])
		else
			if curLayer["name"] ~= name then
				curScene:removeChild(curLayer["layer"])
				curLayer["name"] = name
				curLayer["layer"] = self.allLayers[name]
				curScene:addChild(curLayer["layer"],curLayer["zOrder"])
			end
		end
	end
end

return LayerManager