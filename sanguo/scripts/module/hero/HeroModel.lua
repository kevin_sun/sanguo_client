local HeroModel = class("HeroModel")

function HeroModel:ctor(heroid,name,level,attack,defend,zhi,blood)
	self.heroid = heroid
	self.name = name
	self.level = level
	self.attack = attack
	self.defend = defend
	self.zhi = zhi
	self.blood = blood
	--从1 到 6 依次是武器，防具，坐骑，兵法1，兵法2，兵法3
	self.equipment = {}
end

return HeroModel