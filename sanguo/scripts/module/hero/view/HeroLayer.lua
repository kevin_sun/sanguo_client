local HeroLayer = class("HeroLayer",function () 
	local layer = display.newLayer()
	return layer
end)

function HeroLayer:ctor()
	--英雄列表
	self.heroScrollView = require("module.hero.view.HeroListCL").new({{id=310011},{id=310012},{id=310013},{id=310014},{id=310015},{id=310016},{id=310017},{id=310018},{id=330021}})
	local posInfo = displayUtil:getConfigPosition("group_hero_list")
	self.heroScrollView:setPosition(CCPoint(posInfo.x,posInfo.y))
	self:addChild(self.heroScrollView)
	--英雄明细
	self.heroDetail = require("module.hero.view.HeroDetailCL").new()
	posInfo = displayUtil:getConfigPosition("group_hero_detail")
	self.heroDetail:setPosition(CCPoint(posInfo.x,posInfo.y))
	self:addChild(self.heroDetail)
end


return HeroLayer