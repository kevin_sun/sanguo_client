local scrollView = require("comlib.ui.ScrollView")
local HeroListCL = class("HeroListCL.lua",function () 
	local proxy = LuaProxy:create()
	local node = proxy:readCCBFromFile("ccb/HScrollListView.ccbi")
	node = tolua.cast(node,"CCSprite")
	--向左button
	node.leftSelect = tolua.cast(proxy:getNode("selectLeft"),"CCMenuItem")
	--向右button
	node.rightSelect = tolua.cast(proxy:getNode("selectRight"),"CCMenuItem")
	--scrollview 容器
	node.heroListContainer = tolua.cast(proxy:getNode("scrollContainer"),"CCNode")
	return node
end)

function HeroListCL:ctor(heros)
	--注册事件
	self.leftSelect:registerScriptTapHandler(handler(self,self.onLeftRightClick))
	self.rightSelect:registerScriptTapHandler(handler(self,self.onLeftRightClick))
	--创建scrollview结点
	local containerContentSize = self.heroListContainer:getContentSize()
	self.scrollView = scrollView.new(CCRectMake(0,0,containerContentSize.width,containerContentSize.height),contentScaleFactor,scrollView.DIRECTION_HORIZONTAL)
	self.heroListContainer:addChild(self.scrollView)
	--添加scrollcell
	for i=1,#heros do
		local heroCell = require("module.hero.view.HeroScrollCellCL").new(heros[i])
		self.scrollView:addCell(heroCell)
	end
end

function HeroListCL:onLeftRightClick(index)
	if index == 1 then
		CCLuaLog("向左选")
	else
		CCLuaLog("向右选")
	end
end

function HeroListCL:moveRight()
end

return HeroListCL