local scrollCell = require("comlib.ui.ScrollCell")
local HeroScrollCellCL = class("HeroScrollCellCL",function () 
	local node = display.newSprite("#head_box_blue.png")
	return node
end)

--创建对象
function HeroScrollCellCL:ctor(hero)
	scrollCell.extend(self)
	local heroImage = "hero/"..hero.id..".png"
	local heroSprite = display.newSprite(heroImage)
	local centerPoint = CCPoint(self:getItemWidth()/2,self:getItemHeight()/2)
	heroSprite:setTextureRect(CCRectMake(0,44,96,96))
	heroSprite:setPosition(centerPoint)
	self:addChild(heroSprite)
	self:setId(hero.id)
end

return HeroScrollCellCL