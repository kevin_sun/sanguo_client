local HeroDetailCL  = class("HeroDetailCL",function ()
	local proxy = LuaProxy:create()
	local detailLayer = proxy:readCCBFromFile("ccb/HeroDetailView.ccbi")
	detailLayer = tolua.cast(detailLayer,"CCLayer")
	detailLayer.heroFlag = tolua.cast(proxy:getNode("heroFlag"),"CCSprite")
	detailLayer.heroName = tolua.cast(proxy:getNode("heroName"),"CCLabelTTF")
	detailLayer.heroAttackLabel = tolua.cast(proxy:getNode("attackLabel"),"CCLabelTTF")
	detailLayer.heroDefendLabel = tolua.cast(proxy:getNode("defendLabel"),"CCLabelTTF")
	detailLayer.heroIntegenceLabel = tolua.cast(proxy:getNode("integenceLabel"),"CCLabelTTF")
	detailLayer.heroBloodLabel = tolua.cast(proxy:getNode("bloodLabel"),"CCLabelTTF")
	detailLayer.heroBiFaItemList = {}
	detailLayer.heroBiFaItemList[1] = tolua.cast(proxy:getNode("binfaItem1"),"CCSprite")
	detailLayer.heroBiFaItemList[2] = tolua.cast(proxy:getNode("binfaitem2"),"CCSprite")
	detailLayer.heroBiFaItemList[3] = tolua.cast(proxy:getNode("binfaitem3"),"CCSprite")
	detailLayer.heroWeaponItemList = {}
	detailLayer.heroWeaponItemList[1] = tolua.cast(proxy:getNode("weaponItem"),"CCSprite")
	detailLayer.heroWeaponItemList[2] = tolua.cast(proxy:getNode("defendItem"),"CCSprite")
	detailLayer.heroWeaponItemList[3] = tolua.cast(proxy:getNode("hourseItem"),"CCSprite")
	detailLayer.heroSprite = tolua.cast(proxy:getNode("heroSprite"),"CCSprite")
	--添加事件
	detailLayer.confirmButton = tolua.cast(proxy:getNode("buzhenButton"),"CCMenuItem")
	--
	CCLayerExtend.extend(detailLayer)
	return detailLayer
end)

function HeroDetailCL:ctor()
	self:setNodeEventEnabled(true)
	
end

function HeroDetailCL:onEnter()
	self.confirmButton:registerScriptTapHandler(handler(self,self.confirmHandler))
end

function HeroDetailCL:onExit()
	self.confirmButton:unregisterScriptTapHandler()
end

function HeroDetailCL:confirmHandler()
	
end

return HeroDetailCL