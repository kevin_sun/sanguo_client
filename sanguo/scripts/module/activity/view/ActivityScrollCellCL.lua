local ActivityScrollCellCL = class("ActivityScrollCellCL",function () 
	local node = display.newSprite("#head_box_red.png")
	return node
end)

function ActivityScrollCellCL:ctor(activity)
	require("comlib.ui.ScrollCell").extend(self)
	local activityIcon = "active/activeicon/huodong_"..activity["name"]..".png"
	local activitySprite = display.newSprite(activityIcon)
	local centerPoint = CCPoint(self:getItemWidth()/2,self:getItemHeight()/2)
	activitySprite:setPosition(centerPoint)
	activitySprite:setTextureRect(CCRectMake(0,44,96,96))
	self:addChild(activitySprite)
	self:setId(activity.id)
end


return ActivityScrollCellCL