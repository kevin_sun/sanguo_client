local scrollView = require("comlib.ui.ScrollView")

local ActivityScrollCL = class("ActivityScrollCL",function () 
	local proxy = LuaProxy:create()
	local node = proxy:readCCBFromFile("ccb/HScrollListView.ccbi")
	node = tolua.cast(node,"CCSprite")
	--向左button
	node.leftSelect = tolua.cast(proxy:getNode("selectLeft"),"CCMenuItem")
	--向右button
	node.rightSelect = tolua.cast(proxy:getNode("selectRight"),"CCMenuItem")
	--scrollview 容器
	node.activityListContainer = tolua.cast(proxy:getNode("scrollContainer"),"CCNode")
	return node
end)

function ActivityScrollCL:ctor(activities)
	--注册事件
	self.leftSelect:registerScriptTapHandler(handler(self,self.onLeftRightClick))
	self.rightSelect:registerScriptTapHandler(handler(self,self.onLeftRightClick))
	--创建scrollview结点
	local containerContentSize = self.activityListContainer:getContentSize()
	self.scrollView = scrollView.new(CCRectMake(0,0,containerContentSize.width,containerContentSize.height),contentScaleFactor,scrollView.DIRECTION_HORIZONTAL)
	self.activityListContainer:addChild(self.scrollView)
	--添加scrollcell
	for i=1,#activities do
		local activityCell = require("module.activity.view.ActivityScrollCellCL").new(activities[i])
		self.scrollView:addCell(activityCell)
	end
end

function ActivityScrollCL:onLeftRightClick(index)
	if index == 1 then
		CCLuaLog("向左选")
	else
		CCLuaLog("向右选")
	end
end

function ActivityScrollCL:onLeftRightClick()
end


return ActivityScrollCL