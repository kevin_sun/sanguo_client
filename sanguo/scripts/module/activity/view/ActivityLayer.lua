local ActivityLayer = class("ActivityLayer",function() 
	return display.newLayer()
end)

function ActivityLayer:ctor()
	self.activityList = require("module.activity.view.ActivityScrollCL").new(
	{	
		{id=310011,name = "chongzhi"},{id=310012,name = "denglu"},
		{id=310013,name = "duihuan"},{id=310014,name = "duobao"},
		{id=310015,name = "invite"},{id=310016,name = "junling"},
		{id = 310017,name = "leitai"},{id = 310018,name = "shaoxiang"}
	})
	local posInfo = displayUtil:getConfigPosition("activiy_list_cl")
	self.activityList:setPosition(CCPoint(posInfo.x,posInfo.y))
	self:addChild(self.activityList)
end 

return ActivityLayer