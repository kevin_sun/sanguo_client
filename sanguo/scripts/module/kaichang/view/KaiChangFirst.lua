local playerCommand = require("module.player.PlayerCommand")
local KaiChangFirst = class("KaiChangFirst",function ()
	local scene = display.newScene("KaiChangFirst")
	local proxy = LuaProxy:create()
	local bgLayer = tolua.cast(proxy:readCCBFromFile("ccb/KaiChangOne.ccbi"),"CCLayer")
	scene:addChild(bgLayer)
	scene.textSprite = tolua.cast(proxy:getNode("textSprite"),"CCSprite")
	return scene
end)

function KaiChangFirst:ctor()
	
end

function KaiChangFirst:onEnterTransitionFinish()
	transition.fadeIn(self.textSprite, {time = 4})
	--
	require("framework.scheduler").performWithDelayGlobal(function () appFacade:sendNotification(playerCommand.KaiChangFabang) end,5)
end

return KaiChangFirst