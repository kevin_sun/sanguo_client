local playerCommand = require("module.player.PlayerCommand")
local KaiChangZhuiJi = class("KaiChangZhuiJi",function ()
	local scene = display.newScene("KaiChangZhuiJi")
	local proxy = LuaProxy:create()
	scene.bgSprite = tolua.cast(proxy:readCCBFromFile("ccb/KaiChangZhuiJi.ccbi"),"CCSprite")
	scene.bgSprite:setScale(2.0)
	scene.bgSprite:setPosition(CCPoint(display.cx,display.cy))
	scene:addChild(scene.bgSprite)
	return scene
end)

function KaiChangZhuiJi:ctor()
end

function KaiChangZhuiJi:onEnterTransitionFinish()
	transition.scaleTo(self.bgSprite, {scale = 1.0,time = 4})
	--
	require("framework.scheduler").performWithDelayGlobal(function () appFacade:sendNotification(playerCommand.CreatePlayer) end,6)
end

return KaiChangZhuiJi