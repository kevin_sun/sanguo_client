local playerCommand = require("module.player.PlayerCommand")
local KaiChangSecond = class("KaiChangSecond",function ()
	local scene = display.newScene("KaiChangSecond")
	local proxy = LuaProxy:create()
	local bgLayer = tolua.cast(proxy:readCCBFromFile("ccb/KaiChangOne.ccbi"),"CCLayer")
	scene:addChild(bgLayer)
	scene.textSprite = tolua.cast(proxy:getNode("textSprite"),"CCSprite")
	local point = CCPoint(scene.textSprite:getPositionX(),scene.textSprite:getPositionY())
	bgLayer:removeChild(scene.textSprite)
	scene.textSprite = display.newSprite("#txt4.png",point.x,point.y)
	bgLayer:addChild(scene.textSprite)
	return scene
end)

function KaiChangSecond:ctor()
	
end

function KaiChangSecond:onEnterTransitionFinish()
	transition.fadeIn(self.textSprite, {time = 4})
	--
	require("framework.scheduler").performWithDelayGlobal(function () appFacade:sendNotification(playerCommand.KaiChangZhuiJi) end,5)
end

return KaiChangSecond