local playerCommand = require("module.player.PlayerCommand")
local KaiChangFabang = class("KaiChangFabang",function ()
	local scene = display.newScene("KaiChangFabang")
	local proxy = LuaProxy:create()
	scene.bgSprite = tolua.cast(proxy:readCCBFromFile("ccb/KaiChangFaBang.ccbi"),"CCSprite")
	scene.bgSprite:setScale(1.8)
	scene.bgSprite:setPosition(CCPoint(display.cx,display.cy))
	scene:addChild(scene.bgSprite)
	return scene
end)

function KaiChangFabang:ctor()
end

function KaiChangFabang:onEnterTransitionFinish()
	transition.scaleTo(self.bgSprite, {scale = 1.0,time = 3})
	require("framework.scheduler").performWithDelayGlobal(function () appFacade:sendNotification(playerCommand.KaiChangSecond) end,4)
end

return KaiChangFabang