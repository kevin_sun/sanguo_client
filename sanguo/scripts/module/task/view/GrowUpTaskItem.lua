local GrowUpTaskItem = class("GrowUpTaskItem",function ()
	local proxy = LuaProxy:create()
	local node = tolua.cast(proxy:readCCBFromFile("ccb/GrowUpTaskItem.ccbi"),"CCSprite")
	node.taskItemName = tolua.cast(proxy:getNode("taskItemName"),"CCLabelTTF")
	node.taskItemDes = tolua.cast(proxy:getNode("taskItemDes"),"CCLabelTTF")
	node.taskItemStatus = tolua.cast(proxy:getNode("taskItemStatus"),"CCSprite")
	node.taskItemProcess = tolua.cast(proxy:getNode("taskItemProcess"),"CCLabelTTF")
	node.buttonContainer = tolua.cast(proxy:getNode("buttonContainer"),"CCNode")
	return node
end)

function GrowUpTaskItem:ctor(taskId,taskName,taskDes,taskAward,taskStatus,taskProcess)
	require("comlib.ui.ScrollCell").extend(self)
end

function GrowUpTaskItem:onGetAward()

end

return GrowUpTaskItem