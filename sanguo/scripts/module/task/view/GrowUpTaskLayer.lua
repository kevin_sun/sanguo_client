local GrowUpTaskLayer = class("GrowUpTaskLayer",function () 
	return display.newLayer()
end)

function GrowUpTaskLayer:ctor()
	self.groupTask = require("module.task.view.GrowUpTask").new()
	local posInfo = displayUtil:getConfigPosition("growup_task_list")
	self.groupTask:setPosition(CCPoint(posInfo.x,posInfo.y))
	self:addChild(self.groupTask)
end

return GrowUpTaskLayer