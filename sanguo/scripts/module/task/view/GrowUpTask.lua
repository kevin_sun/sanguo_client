local scrollView = require("comlib.ui.ScrollView")
local GrowUpTask = class("GrowUpTask",function () 
	local proxy = LuaProxy:create()
	local node = tolua.cast(proxy:readCCBFromFile("ccb/GrowupTaskList.ccbi"),"CCSprite")
	node.scrollContainer = tolua.cast(proxy:getNode("scrollContainer"),"CCNode")
	return node	
end)

function GrowUpTask:ctor()
	local containerContentSize = self.scrollContainer:getContentSize()
	self.scrollView = scrollView.new(CCRectMake(0,0,containerContentSize.width,containerContentSize.height),contentScaleFactor,scrollView.DIRECTION_VERTICAL)
	self.scrollContainer:addChild(self.scrollView)
	for i=1,10 do
		local taskViewCell = require("module.task.view.GrowUpTaskItem").new()
		self.scrollView:addCell(taskViewCell)
	end
end

return GrowUpTask