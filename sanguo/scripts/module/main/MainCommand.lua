local MainCommand = class("MainCommand")

MainCommand.Index = "MAIN_INDEX"
MainCommand.Group = "MAIN_GROUP"
MainCommand.Battle = "MAIN_BATTLE"
MainCommand.PK = "MAIN_PK"
MainCommand.Fight = "MAIN_FIGHT"
MainCommand.Shop = "MAIN_SHOP"
MainCommand.Activity = "MAIN_ACTIVITY"
MainCommand.GrowTask = "GROW_TASK"

function MainCommand:execute(data)
	if data.name == self.Index then
		layerManager:switchMainMenu("mainMenuLayer")
		layerManager:switchBg("bgLayerTwo")
		layerManager:switchPlayerBar("playerInfoLayer")
		layerManager:_switchGameLayer("module.city.view.CityBuildingLayer")
	elseif data.name == self.GrowTask then
		layerManager:switchPlayerBar("simplePlayerInfoLayer")
		layerManager:switchBg("bgLayerTwo")
		layerManager:_switchGameLayer("module.task.view.GrowUpTaskLayer")
	elseif data.name == self.Group then
		layerManager:switchBg("bgLayerTwo")
		layerManager:switchPlayerBar()
		layerManager:_switchGameLayer("module.hero.view.HeroLayer")
	elseif data.name == self.Battle then
		layerManager:switchBg("bgLayerOne")
		layerManager:switchPlayerBar("playerInfoLayer")
		layerManager:_switchGameLayer()
	elseif data.name == self.PK then
	elseif data.name == self.Fight then
	elseif data.name == self.Shop then
		layerManager:switchBg("bgLayerOne")
		layerManager:switchPlayerBar("simplePlayerInfoLayer")
		layerManager:_switchGameLayer("module.shop.view.ShopLayer")		
	elseif data.name == self.Activity then
		layerManager:switchPlayerBar()
		layerManager:switchBg("bgLayerTwo")
		layerManager:_switchGameLayer("module.activity.view.ActivityLayer")
	end
end

function MainCommand:listCommands()
	return { self.Index,self.Group,self.Battle,self.PK,self.Fight,self.Shop,self.GrowTask,self.Activity
	}
end



return MainCommand