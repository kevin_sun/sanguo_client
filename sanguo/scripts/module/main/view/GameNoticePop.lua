local scrollView = require("comlib.ui.ScrollView")
local GameNoticePop = class("GameNoticePop",function ()
	local proxy = LuaProxy:create()
	local node = tolua.cast(proxy:readCCBFromFile("ccb/GameNoticesView.ccbi"),"CCSprite")
	node.scrollContainer = tolua.cast(proxy:getNode("scrollContainer"),"CCNode")
	node.enterGame = tolua.cast(proxy:getNode("enterGame"),"CCMenuItem")
	return node
end)

function GameNoticePop:ctor()
	local containerContentSize = self.scrollContainer:getContentSize()
	self.scrollView = scrollView.new(CCRectMake(0,0,containerContentSize.width,containerContentSize.height),contentScaleFactor,scrollView.DIRECTION_HORIZONTAL)
	self.scrollContainer:addChild(self.scrollView)
	self.enterGame:registerScriptTapHandler(handler(self,self.onEnterGame))
end

function GameNoticePop:onEnterGame()

end

return GameNoticePop