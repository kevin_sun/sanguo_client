local MainIndexView = class("MainIndexView",function() 
	local proxy = LuaProxy:create()
	local node = proxy:readCCBFromFile("ccb/MainIndexView.ccbi")
	node = tolua.cast(node,"CCLayer")
	return node
end)