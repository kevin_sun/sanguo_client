local MainMenuLayer = class("MainMenuLayer",function ()
	local layer = display.newLayer()
	return layer
end)

function MainMenuLayer:ctor()
	local posInfo = displayUtil:getConfigPosition("menu_bar")
	self.mainMenu = require("module.main.view.MainMenuCL").new()
	self.mainMenu:setPosition(CCPoint(posInfo.x,posInfo.y))
	self:addChild(self.mainMenu)
	--添加notice msg bar
	self.noticeBg = displayUtil:newSprite("#notice.png")
	self:addChild(self.noticeBg)
end


return MainMenuLayer