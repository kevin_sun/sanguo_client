local MainMenuCL = class("MainMenuCL",function ()
	local proxy = LuaProxy:create()
	local node = proxy:readCCBFromFile("ccb/MainMenu.ccbi")
	node = tolua.cast(node,"CCSprite")
	--建筑可点击
	node.menuIndex = tolua.cast(proxy:getNode("menuIndex"),"CCMenuItem")
	node.menuStrategy = tolua.cast(proxy:getNode("menuStrategy"),"CCMenuItem")
	node.menuBattle = tolua.cast(proxy:getNode("menuBattle"),"CCMenuItem")
	node.menuPk = tolua.cast(proxy:getNode("menuPk"),"CCMenuItem")
	node.menuFight = tolua.cast(proxy:getNode("menuFight"),"CCMenuItem")
	node.menuShop = tolua.cast(proxy:getNode("menuShop"),"CCMenuItem")
	node.menuItems = {node.menuIndex,node.menuStrategy,node.menuBattle,node.menuPk,node.menuFight,node.menuShop}
	--选中图片放置位置
	node.selectedClipNode = tolua.cast(proxy:getNode("selectedClipNode"),"CCNode")
	return node
end)

function MainMenuCL:ctor()
	--事件通知扩展
	eventProtocol.extend(self)
	--按钮没有选中时的状态
	self.curIndex = 0
	for i=1,#self.menuItems do
		 self.menuItems[i]:registerScriptTapHandler(handler(self,self.onMenuClick))
	end
end

--主菜单点击触发
function MainMenuCL:onMenuClick(index)
	local selectedItem = self.menuItems[index]
	local x = selectedItem:getPositionX()
	local y = selectedItem:getPositionY()
	--将原来选中的状态取消
	if self.selectedCliping then
		self.selectedClipNode:removeChild(self.selectedCliping,true)
	end
	--设定新的选中
	self.selectedCliping = display.newSprite("#footer_selected.png")
	self.selectedCliping:setPosition(CCPoint(x,y))
	self.selectedClipNode:addChild(self.selectedCliping)
	self.curIndex = index
	if self.curIndex == 1 then
		appFacade:notify(require("module.main.MainCommand").Index)
	elseif self.curIndex == 2 then
		appFacade:notify(require("module.main.MainCommand").Group)
	elseif self.curIndex == 3 then
		appFacade:notify(require("module.main.MainCommand").Battle)
	elseif self.curIndex == 6 then
		appFacade:notify(require("module.main.MainCommand").Shop)
	end
	CCLuaLog(index.."MenuIndex")
end

--取消选择
function MainMenuCL:cancelSelected()
	if self.curIndex >0 then
		self.curIndex = 0
		if self.selectedCliping then
			self:removeChild(self.selectedCliping)
		end
	end
end


return MainMenuCL