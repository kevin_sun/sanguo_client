local ShopItemCL = class("ShopItemCL",function () 
	local proxy = LuaProxy:create()
	local node = tolua.cast(proxy:readCCBFromFile("ccb/ShopItem.ccbi"),"CCNode")
	node.itemBg = tolua.cast(proxy:getNode("itemBg"),"CCSprite")
	node.titleLabel = tolua.cast(proxy:getNode("titleLabel"),"CCLabelTTF")
	node.desLabel = tolua.cast(proxy:getNode("desLabel"),"CCLabelTTF")
	node.priceLabel = tolua.cast(proxy:getNode("priceLabel"),"CCLabelTTF")
	node.numLabel = tolua.cast(proxy:getNode("numLabel"),"CCLabelTTF")
	node.buyButton = tolua.cast(proxy:getNode("buyButton"),"CCControlButton")
	return node
end)

function ShopItemCL:ctor()
	require("comlib.ui.ScrollCell").extend(self)
end

return ShopItemCL