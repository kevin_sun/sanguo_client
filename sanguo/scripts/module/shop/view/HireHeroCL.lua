local HireHeroCL = class("HireHeroCL",function ()
	local proxy = LuaProxy:create()
	local layer = proxy:readCCBFromFile("ccb/HireHero.ccbi")
	layer = tolua.cast(layer,"CCLayer")
	layer.oneFreeTimes = tolua.cast(proxy:getNode("oneFreeTimes"),"CCLabelTTF")
	layer.twoFreeTimes = tolua.cast(proxy:getNode("twoFreeTimes"),"CCLabelTTF")
	layer.threeFreeTimes = tolua.cast(proxy:getNode("threeFreeTimes"),"CCLabelTTF")
	return layer
end)

function HireHeroCL:ctor()
end

return HireHeroCL
