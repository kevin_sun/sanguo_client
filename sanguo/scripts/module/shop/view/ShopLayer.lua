local ShopLayer = class("ShopLayer",function ()
	return display.newLayer()
end)

function ShopLayer:ctor()
	local tabinfos = 
		{
			{text = "招将",color = ccc3(78,26,0),selectedColor = ccc3(253,251,100),shadowColor = display.COLOR_BLACK,size = 30,view = "module.shop.view.HireHeroCL",viewX = 0,viewY = 0},
			{text = "道具",color = ccc3(78,26,0),selectedColor = ccc3(253,251,100),shadowColor = display.COLOR_BLACK,size = 30,view = "module.shop.view.ShopItemLayer",viewX = 0,viewY = 0},
			{text = "礼品",color = ccc3(78,26,0),selectedColor = ccc3(253,251,100),shadowColor = display.COLOR_BLACK,size = 30},
		}
	local posInfo =displayUtil:getConfigPosition("shop_tab_control") 
	self.shopTabControl = require("comlib.ui.TabControl").new(tabinfos,20,10,CCSize(640,680))
	self.shopTabControl:setPosition(CCPoint(posInfo.x,posInfo.y))
	self:addChild(self.shopTabControl)
end

return ShopLayer