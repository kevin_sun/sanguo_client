local scrollView = require("comlib.ui.ScrollView")
local ShopItemLayer = class("ShopItemLayer",function ()
	return display.newLayer()
end)

function ShopItemLayer:ctor()
	local containerContentSize = CCSizeMake(620,680)
	self.scrollView = scrollView.new(CCRectMake(0,0,containerContentSize.width,containerContentSize.height),contentScaleFactor,scrollView.DIRECTION_VERTICAL,10)
	self.scrollView:setPosition(CCPoint(10,0))
	for i=1,50 do
		local shopItemCell = require("module.shop.view.ShopItemCL").new()
		self.scrollView:addCell(shopItemCell)
	end
	self:addChild(self.scrollView)
end

return ShopItemLayer