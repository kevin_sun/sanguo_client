local CityCommand = class("CityCommand")

--这个command可以接受的命令
CityCommand.EnterCity="EnterCity"
CityCommand.NeiWuFu="NeiWuFu"
CityCommand.CangKu="CangKu"
CityCommand.KeJiGuan="KeJiGuan"
CityCommand.BingQiPu="BingQiPu"
CityCommand.JiangJuFu="JiangJuFu"
CityCommand.TaiXueYuan="TaiXueYuan"
CityCommand.TongQueTai="TongQueTai"

function CityCommand:execute(data)
	if data.name == self.EnterCity then
		
	elseif data.name == self.NeiWuFu then
	end
end

function CityCommand:listCommands()
	return {self.EnterCity,self.NeiWuFu,self.CangKu,self.KeJiGuan,self.BingQiPu,self.JiangJuFu,self.TaiXueYuan,self.TongQueTai}
end

return CityCommand