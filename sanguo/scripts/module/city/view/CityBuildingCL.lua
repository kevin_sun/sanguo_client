local mainCommand = require("module.main.MainCommand")
local CityBuildingCL = class("CityBuildingCL",function ()
	local proxy = LuaProxy:create()
	local node = proxy:readCCBFromFile("ccb/CityBuildingView.ccbi")
	node = tolua.cast(node,"CCSprite")
	--建筑可点击
	node.functionHouse = tolua.cast(proxy:getNode("functionHouse"),"CCMenuItem")
	node.bagHouse = tolua.cast(proxy:getNode("bagHouse"),"CCMenuItem")
	node.placeHouse = tolua.cast(proxy:getNode("placeHouse"),"CCMenuItem")
	node.heroHouse = tolua.cast(proxy:getNode("heroHouse"),"CCMenuItem")
	node.techHouse = tolua.cast(proxy:getNode("techHouse"),"CCMenuItem")
	node.strategyHouse = tolua.cast(proxy:getNode("strategyHouse"),"CCMenuItem")
	node.weaponHouse = tolua.cast(proxy:getNode("weaponHouse"),"CCMenuItem")
	node.houseItems = {node.functionHouse,node.bagHouse,node.placeHouse,node.heroHouse,node.techHouse,node.strategyHouse,node.weaponHouse}
	node.growupTask = tolua.cast(proxy:getNode("growupTask"),"CCSprite")
	node.bbs = tolua.cast(proxy:getNode("bbs"),"CCSprite")
	node.activity = tolua.cast(proxy:getNode("activity"),"CCSprite")
	CCSpriteExtend.extend(node)
	return node
end)

--添加事件
function CityBuildingCL:ctor()
	self:setNodeEventEnabled(true)
end

function CityBuildingCL:onEnter()
	--调整坐标
	for i=1,#self.houseItems do
		 self.houseItems[i]:registerScriptTapHandler(handler(self,self.onHouseClick))
	end
	self.bbs:registerScriptTouchHandler(handler(self,self.onBBS),false)
	self.bbs:setTouchEnabled(true)
	self.growupTask:registerScriptTouchHandler(handler(self,self.onGrowupTask),false)
	self.growupTask:setTouchEnabled(true)
	self.activity:registerScriptTouchHandler(handler(self,self.onActivity),false)
	self.activity:setTouchEnabled(true)
end

function CityBuildingCL:onExit()
	for i=1,#self.houseItems do
		 self.houseItems[i]:unregisterScriptTapHandler()
	end
	self.growupTask:unregisterScriptHandler()
	self.growupTask:setTouchEnabled(false)
	self.bbs:unregisterScriptHandler()
	self.bbs:setTouchEnabled(false)
	self.activity:unregisterScriptHandler()
	self.activity:setTouchEnabled(false)
end

function CityBuildingCL:onHouseClick(tag)
	echo(tag)
end

function CityBuildingCL:onMainMenu(tag)
	echo(tag)
end

function CityBuildingCL:onGrowupTask(event,x,y)
	if event == CCTOUCHBEGAN then
		return true
	elseif event == CCTOUCHENDED then
		appFacade:notify(require("module.main.MainCommand").GrowTask)
	end
end	

function CityBuildingCL:onBBS(event,x,y)
	
end

function CityBuildingCL:onActivity(event,x,y)
	if event == CCTOUCHBEGAN then
		return true
	end
	if event == CCTOUCHENDED then
		appFacade:notify(require("module.main.MainCommand").Activity)
	end
	
end



return CityBuildingCL