local CityBuildingLayer = class("CityBuildingLayer",function () 
	local layer = display.newLayer()
	return layer
end)

function CityBuildingLayer:ctor()
	--添加
	self.cityBuildingView = require("module.city.view.CityBuildingCL").new()
	local posInfo = displayUtil:getConfigPosition("city_building_view")
	self.cityBuildingView:setPosition(CCPoint(posInfo.x,posInfo.y))
	self:addChild(self.cityBuildingView)
	--英雄
	self.heroScrollView = require("module.hero.view.HeroListCL").new({{id=310011},{id=310012},{id=310013},{id=310014},{id=310015},{id=310016},{id=310017},{id=310018},{id=330021}})
	posInfo = displayUtil:getConfigPosition("main_hero_scroll")
	self.heroScrollView:setPosition(CCPoint(posInfo.x,posInfo.y))
	self:addChild(self.heroScrollView)
end

return CityBuildingLayer