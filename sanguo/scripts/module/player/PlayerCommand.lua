local PlayerCommand = class("PlayerCommand")
PlayerCommand.CreatePlayer = "CREATEPLAYER" 
PlayerCommand.LoginIndex = "LOGININDEX"
PlayerCommand.EnterGame = "ENTERGAME"
PlayerCommand.ShowCurGameArea = "SHOWCURGAMEAREA"
PlayerCommand.ListGameArea = "LISTGAMEAREA"
PlayerCommand.SelectedGameArea = "SELECTEDGAMEAREA"
PlayerCommand.KaiChangZhuiJi = "KAICHANGZHUIJI"
PlayerCommand.KaiChangFabang = "KAICHANGFABANG"
PlayerCommand.KaiChangSecond = "KAICHANGSECOND"
PlayerCommand.PopPlayerInfo = "POPPLAYERINFO"

function PlayerCommand:execute(data)
	if data.name == PlayerCommand.LoginIndex then
		--app facade 是全局变量
		appFacade:enterScene("module.player.view.LoginScene")
	    --
	elseif data.name == PlayerCommand.EnterGame then
		--进入游戏
		--appFacade:enterScene("module.kaichang.view.KaiChangFirst")
		appFacade:enterScene("module.player.view.NewPlayerScene")
	elseif data.name == PlayerCommand.ShowCurGameArea then
		local runningScene = appFacade:getRunningScene()
		runningScene:ShowCurGameArea()
	elseif data.name == PlayerCommand.ListGameArea then
	
	elseif data.name == PlayerCommand.KaiChangZhuiJi then
		appFacade:enterScene("module.kaichang.view.KaiChangZhuiJi")
	elseif data.name == PlayerCommand.KaiChangFabang then
		appFacade:enterScene("module.kaichang.view.KaiChangFabang")
	elseif data.name == PlayerCommand.KaiChangSecond then
		appFacade:enterScene("module.kaichang.view.KaiChangSecond")
	elseif data.name == PlayerCommand.CreatePlayer then
		appFacade:enterScene("module.player.view.NewPlayerScene")
	elseif data.name == PlayerCommand.PopPlayerInfo then
		layerManager:popup("module.player.view.PlayerInfoPop")
	end
end

function PlayerCommand:listCommands()
	return {self.LoginIndex,self.EnterGame,self.ListGameArea,self.SelectedGameArea,self.ShowCurGameArea,self.KaiChangZhuiJi,
	self.KaiChangFabang,self.KaiChangSecond,self.CreatePlayer,self.PopPlayerInfo}
end


return PlayerCommand