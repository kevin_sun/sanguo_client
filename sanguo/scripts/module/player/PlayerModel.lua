local PlayerModel = {}
eventProtocol.extend(PlayerModel)

PlayerModel.playerId = 0
PlayerModel.name = nil
PlayerModel.area = 0
PlayerModel.gold = 0
PlayerModel.coins = 0
PlayerModel.command = 0
PlayerModel.books = 0
PlayerModel.level = 0
--阵法
PlayerModel.battleArray = {}
--道具
PlayerModel.items = {}
--英雄列表
PlayerModel.heros = {}
--科技
PlayerModel.techs = {}


PlayerModel.updateEvent = "UPDATE_EVENT"


return PlayerModel