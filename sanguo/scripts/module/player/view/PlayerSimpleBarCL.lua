local PlayerSmallBarCL = class("PlayerSmallBarCL",function () 
	local proxy = LuaProxy:create()
	local node = proxy:readCCBFromFile("ccb/PlayerSimpleBar.ccbi")
	node = tolua.cast(node,"CCSprite")
	node.playerLevel = tolua.cast(proxy:getNode("playerLevel"),"CCLabelBMFont")
	node.playerName = tolua.cast(proxy:getNode("playerName"),"CCLabelTTF")
	node.moneyLabel = tolua.cast(proxy:getNode("moneyLabel"),"CCLabelTTF")
	node.goldLabel = tolua.cast(proxy:getNode("goldLabel"),"CCLabelTTF")
	return node
end)

--构造方法，初始化位置
function PlayerSmallBarCL:ctor()
	
end


return PlayerSmallBarCL