local scrollView = require("comlib.ui.ScrollView")
local GameAreaList = class("GameAreaList",function ()
	local proxy = LuaProxy:create()
	local node = tolua.cast(proxy:readCCBFromFile("ccb/GameAreaListView.ccbi","CCSprite"))
	node.lastScrollContainer = tolua.cast(proxy:getNode("lastScrollContainer"),"CCNode")
	node.allScrollContainer = tolua.cast(proxy:getNode("allScrollContainer"),"CCNode")
	return node
end)

function GameAreaList:ctor()
	local containerContentSize = self.lastScrollContainer:getContentSize()
	self.lastScrollView = scrollView.new(CCRectMake(0,0,containerContentSize.width,containerContentSize.height),contentScaleFactor,scrollView.DIRECTION_HORIZONTAL)
	containerContentSize = self.allScrollContainer:getContentSize()
	self.allScrollView = scrollView.new(CCRectMake(0,0,containerContentSize.width,containerContentSize.height),contentScaleFactor,scrollView.DIRECTION_HORIZONTAL)
	self.lastScrollContainer:addChild(self.lastScrollView)
	self.allScrollContainer:addChild(self.allScrollView)
end

return GameAreaList