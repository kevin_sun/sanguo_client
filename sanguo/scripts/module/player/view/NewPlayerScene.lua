local radioButton = require("comlib.ui.RadioButton")
local NewPlayerScene = class("NewPlayerScene", function ()
	local proxy = LuaProxy:create()
	local node = proxy:readCCBFromFile("ccb/NewPlayer.ccbi")
	node = tolua.cast(node,"CCLayer")
	local scene = display.newScene("NewPlayerScene")
	scene:addChild(node)
	--
	scene.LuaProxy = proxy
	scene.selectHeroContainer = tolua.cast(proxy:getNode("selectHeroContainer"),"CCNode")
	scene.randomButton = tolua.cast(proxy:getNode("randomButton"),"CCMenuItem")
	scene.confirmButton  = tolua.cast(proxy:getNode("confirmButton"),"CCControlButton")
	scene.textContainer = tolua.cast(proxy:getNode("textContainer"),"CCNode")
	proxy:retain()
	return scene
end
)

function NewPlayerScene:ctor()
	local selectHeroMenu = radioButton.new({image="#ssx_2.png",imageSelected="#ssx_1.png"},
		{image="#xhy_2.png",imageSelected="#xhy_1.png"},{image="#xs_2.png",imageSelected="#xs_1.png"})
	self.selectHeroMenu = selectHeroMenu
	self.selectHeroContainer:addChild(selectHeroMenu)
	--添加英雄选择界面
	self.selectHeroMenu:addEventListener(radioButton.selectEvent,handler(self,self.onSelectHero))
	--添加事件响应
	self.LuaProxy:handleButtonEvent(self.confirmButton, handler(self,self.onConfirm), CCControlEventTouchUpInside)
	--添加随机名称事件
	self.randomButton:registerScriptTapHandler(handler(self,self.onRandomName))
	--创建输入框
	self.nameEditor = displayUtil:newEditBox({image="#input.png",size = CCSizeMake(320,74),listener = handler(self,self.onNameEdit)},true)
	self.nameEditor:setMaxLength(10)
	self.textContainer:addChild(self.nameEditor)
end

function NewPlayerScene:onSelectHero(event)
	CCLuaLog(event.data)
end

function NewPlayerScene:onNameEdit(event,target)
	CCLuaLog(event)
end

function NewPlayerScene:onRandomName()
end

function NewPlayerScene:onCleanup()
	
end

function NewPlayerScene:onExit()
	--
	CCLuaLog("on exit release proxy")
	self.LuaProxy:release()
end


function NewPlayerScene:onConfirm(button,event)
	appFacade:enterScene("module.main.view.MainScene")
	appFacade:notify(require("module.main.MainCommand").Index)
end

return NewPlayerScene