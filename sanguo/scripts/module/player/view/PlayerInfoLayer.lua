local PlayerInfoLayer = class("PlayerInfoLayer",function ()
	local layer = display.newLayer()
	return layer
end)

function PlayerInfoLayer:ctor(isSimple)
	if not isSimple then
		--设置简单的用户信息bar
		self.playerBar = require("module.player.view.PlayerBarCL").new()
		local posInfo = displayUtil:getConfigPosition("player_bar")
		self.playerBar:setPosition(CCPoint(posInfo.x,posInfo.y))
		self:addChild(self.playerBar)
	else
		--用户信息bar
		self.playerBar = require("module.player.view.PlayerSimpleBarCL").new()
		local posInfo = displayUtil:getConfigPosition("player_simple_bar")
		self.playerBar:setPosition(CCPoint(posInfo.x,posInfo.y))
		self:addChild(self.playerBar)
	end	
end

return PlayerInfoLayer