local playerCommand = require("module.player.PlayerCommand")
local mainCommand = require("module.main.MainCommand")
local LoginScene = class("LoginScene",function () 
	return display.newScene("LoginScene") end
)
--登录场景不同的状态
LoginScene.status={
	statusLoadingRes=0,statusGoGame=1,statusSelectArea=2,statusGoCity=3
}

function LoginScene:ctor(...)
	--资源加载进度
	self.loadProcess = 0
	self.curStatus = self.status["statusLoadingRes"]
	self.bgSprite = displayUtil:newSprite("#loginLoading2.png")
	self:addChild(self.bgSprite)	
	--增加加载画面
	self.loadingSprite = displayUtil:newSpriteWithAni("Loading_01.png","Loading_%02d.png", 1, 48,1/60,true)
	self:addChild(self.loadingSprite)	
	--增加提示文字
	self.processLabel = displayUtil:newTTFLabel({text="正在初始化",align=ui.TEXT_ALIGN_CENTER},"Loading_process001")
	self:addChild(self.processLabel)
end	

function LoginScene:ShowCurGameArea()
	self.curStatus = self.status["statusGoGame"]
	self:cleanChild()
	local curAreaPos = displayUtil:getConfigPosition("#login_recent.png")
	self.curAreaBg = require("module.player.view.CurGameArea").new()
	self.curAreaBg:setPosition(CCPoint(curAreaPos.x,curAreaPos.y))
	self:addChild(self.curAreaBg)
	local enterGameItems = displayUtil:newImageMenuItem({image = "#login_enter1.png",imageSelected = "#login_enter2.png",listener = handler(self,self.enterGame)})
	self.enterGameMenu = ui.newMenu({enterGameItems})
	self:addChild(self.enterGameMenu)
end

function LoginScene:showGameAreaList()
	self.curStatus = self.status["statusSelectArea"]
	self:cleanChild()
end

function LoginScene:showGoCity()
	self.curStatus = self.status["statusGoCity"]
	self:cleanChild()
end

function LoginScene:enterGame()
	appFacade:sendNotification(playerCommand.EnterGame)
end

function LoginScene:cleanChild()
	if self.loadingSprite then
		self:removeChild(self.loadingSprite,true)
		self.loadingSprite = nil
	end
	if self.processLabel then
		self:removeChild(self.processLabel,true)
		self.processLabel = nil
	end
	if self.curAreaBg then
		self:removeChild(self.curAreaBg,true)
		self.curAreaBg = nil
	end
	if self.curAreaBg then
		self:removeChild(self.curAreaBg,true)
		self.curAreaBg = nil
	end
end

function LoginScene:onData(event)
	local player = event.data
end

function LoginScene:onEnter()
	--增加进度提示
	self:scheduleUpdate(handler(self,self.update))
end

function LoginScene:onCleanup()
	CCLuaLog("on clean login scene")
	self:unscheduleUpdate()
end

function LoginScene:update(delt)
	if self.curStatus == self.status.statusLoadingRes then
		self.loadProcess = self.loadProcess + delt/1
		self.processLabel:setString("正在初始化"..tostring(math.ceil(math.min(self.loadProcess*100,100))).."%")
		if self.loadProcess >= 1 then
			appFacade:sendNotification(playerCommand.ShowCurGameArea)
		end
	end
end

return LoginScene