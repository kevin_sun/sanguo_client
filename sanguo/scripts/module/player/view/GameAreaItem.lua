local GameAreaItem = class("GameAreaItem",function ()
	local proxy = LuaProxy:create()
	local node = tolua.cast(proxy:readCCBFromFile("ccb/GameAreaItem.ccbi"),"CCSprite")
	node.gameAreaId = tolua.cast(proxy:getNode("gameAreaId"),"CCLabelTTF")
	node.gameAreaName = tolua.cast(proxy:getNode("gameAreaName"),"CCLabelTTF")
	return node
end)

function GameAreaItem:ctor()
end



return GameAreaItem