local PlayerBarCL = class("PlayerBarCL",function () 
	local proxy = LuaProxy:create()
	local node = proxy:readCCBFromFile("ccb/PlayerBar.ccbi")
	node = tolua.cast(node,"CCSprite")
	--设定级别
	local levelLabel = tolua.cast(proxy:getNode("playerLevelLabel"),"CCLabelBMFont")
	node.levelLabel = levelLabel
	--
	node.goldLabel = tolua.cast(proxy:getNode("goldLabel"),"CCLabelTTF")
	--
	node.skillBookLabel = tolua.cast(proxy:getNode("skillBookLabel"),"CCLabelTTF")
	node.playerNameLabel = tolua.cast(proxy:getNode("playerNameLabel"),"CCLabelTTF")
	--
	CCNodeExtend.extend(node)
	node:setNodeEventEnabled(true)
	return node
end)

function PlayerBarCL:ctor()
	self.modelUpdateHandler = appFacade.player:addEventListener(appFacade.player.updateEvent,handler(self,self.onData))
end

function PlayerBarCL:onData(event)
	local player = event.data
	if self.levelLabel:getString() ~= player.level then
		self.levelLabel:setString(player.level)
	end
	--
	if self.goldLabel:getString() ~= player.gold then
		self.goldLabel:setString(player.gold)
	end
	--
	if self.skillBookLabel:getString() ~= player.books then
		self.skillBookLabel:setString(player.books)
	end
	--
	if self.playerNameLabel ~=player.name then
		self.playerNameLabel:setString(player.name)
	end
end

function PlayerBarCL:onCleanup()
	if self.modelUpdateHandler then
		appFacade.player:removeEventListener(appFacade.player.updateEvent,self.modelUpdateHandler)
	end
end


function PlayerBarCL:onEnter()
	print("playerbarcl enter")
	self:registerScriptTouchHandler(function () appFacade:notify(require("module.player.PlayerCommand").PopPlayerInfo) end,false)
	self:setTouchEnabled(true)
end



function PlayerBarCL:onExit()
	self:unregisterScriptHandler()
end

return PlayerBarCL