local CurGameArea = class("CurGameArea",function ()
	local proxy = LuaProxy:create()
	local node = tolua.cast(proxy:readCCBFromFile("ccb/GameAreaView.ccbi"),"CCSprite")
	node.gameAreaId = tolua.cast(proxy:getNode("gameAreaId"),"CCLabelTTF")
	node.gameAreaName = tolua.cast(proxy:getNode("gameAreaName"),"CCLabelTTF")
	node.gameAreaStatus = tolua.cast(proxy:getNode("gameAreaStatus","CCSprite"))
	node.moreGameAreaMenu = tolua.cast(proxy:getNode("moreGameAreaMenu"),"CCMenu")
	return node
end)

function CurGameArea:ctor()
	local itemParames = {text = "点击选区>>",size = 30,x = 0,y = 0,listener = handler(self,self.onMoreGameArea)}
	local labelMenuItem = ui.newTTFLabelMenuItem(itemParames)
	self.moreGameAreaMenu:addChild(labelMenuItem, 0, labelMenuItem:getTag())
end

function CurGameArea:onMoreGameArea()

end

return CurGameArea