local PlayerInfoPop = class("PlayerInfoPop",function () 
	local proxy = LuaProxy:create()
	local node = proxy:readCCBFromFile("ccb/PlayerInfoPop.ccbi")
	node = tolua.cast(node,"CCLayer")
	node.closeButton = tolua.cast(proxy:getNode("close"),"CCMenuItem")
	--
	CCNodeExtend.extend(node)
	node:setNodeEventEnabled(true)
	return node
end)

function PlayerInfoPop:ctor()
end

function PlayerInfoPop:onEnter()
	self.closeButton:registerScriptTapHandler(function() layerManager:popup() end)
end

function PlayerInfoPop:onExit()
	self.closeButton:unregisterScriptTapHandler()
end


return PlayerInfoPop