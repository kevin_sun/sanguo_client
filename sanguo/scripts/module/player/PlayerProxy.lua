local HttpConnector=require("net.HttpConnector")
local PlayerProxy=class("PlayerProxy")
local Facade=require("puremvc.patterns.facade.Facade")
--proxy name
PlayerProxy.name="model.PlayerProxy"
--proxy callback notification
PlayerProxy.RegisterPlayer="RegisterPlayerS2C"
PlayerProxy.GameConfig="GameConfigS2C"
PlayerProxy.GetPlayerInfo="GetPlayerInfoS2C"

local HttpConnector=require("net.HttpConnector")
local PlayerProxy=class("PlayerProxy")
local Facade=require("puremvc.patterns.facade.Facade")
--proxy name
PlayerProxy.name="model.PlayerProxy"
--proxy callback notification
PlayerProxy.RegisterPlayer="RegisterPlayerS2C"
PlayerProxy.GameConfig="GameConfigS2C"
PlayerProxy.GetPlayerInfo="GetPlayerInfoS2C"

--构造方法
function PlayerProxy:ctor()
	
end

function PlayerProxy:registPlayer(firstName,lastName,email,facebookId,sex,brithday,language,adSource,thirdPathId)
	HttpConnector:addEventListener(self.RegisterPlayer,handler(self,self.onData))
	local data={firstName=firstName,lastName=lastName,email=email,facebookId=facebookId,
		sex=sex,brithday=brithday,language=language,adSource=adSource,thirdPathId=thirdPathId}
	HttpConnector:sendRequest("registerPlayer",data)
end

function PlayerProxy:getGameAreaInfo()
	
end




function PlayerProxy:gameConfig()
	HttpConnector:addEventListener(self.GameConfigS2C,handler(self,self.onData))
	HttpConnector:sendRequest("gameConfig")
end

function PlayerProxy:getPlayerInfo()
	HttpConnector:addEventListener(self.GetPlayerInfoS2C,handler(self,self.onData))
	HttpConnector:sendRequest("getPlayerInfo")
end

function PlayerProxy:onData(event)
	HttpConnector:removeAllEventListenersForEvent(event.name)
	Facade:sendNotification()
end

return PlayerProxy

return PlayerProxy