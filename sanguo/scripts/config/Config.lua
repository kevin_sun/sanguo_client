
-- 0 - disable debug info, 1 - less debug info, 2 - verbose debug info
DEBUG = 2
DEBUG_FPS = true

-- design resolution
CONFIG_SCREEN_WIDTH  = 640
CONFIG_SCREEN_HEIGHT = 960

-- auto scale mode
CONFIG_SCREEN_AUTOSCALE = "FIXED_WIDTH"

--image resource bundle
GAME_TEXTURE_DATA_FILENAME_LOGIN  = "loading/Login.plist"
GAME_TEXTURE_IMAGE_FILENAME_LOGIN = "loading/Login.png"
--
GAME_TEXTURE_DATA_FILENAME_LOADING  = "loading/Loading.plist"
GAME_TEXTURE_IMAGE_FILENAME_LOADING = "loading/Loading.png"

GAME_TEXTURE_DATA_FILENAME_NEWPLAYER  = "newplayer/createplayer.plist"
GAME_TEXTURE_IMAGE_FILENAME_NEWPLAYER= "newplayer/createplayer.png"

GAME_TEXTURE_DATA_FILENAME_CONTROL_1  = "control/control_one.plist"
GAME_TEXTURE_IMAGE_FILENAME_CONTROL_1= "control/control_one.png"

GAME_TEXTURE_DATA_FILENAME_CONTROL_2  = "control/control_two.plist"
GAME_TEXTURE_IMAGE_FILENAME_CONTROL_2= "control/control_two.png"

GAME_TEXTURE_DATA_FILENAME_ICON  = "icon/icons.plist"
GAME_TEXTURE_IMAGE_FILENAME_ICON= "icon/icons.png"

GAME_TEXTURE_DATA_FILENAME_WORD  = "word/word.plist"
GAME_TEXTURE_IMAGE_FILENAME_WORD= "word/word.png"

GAME_TEXTURE_DATA_FILENAME_HEROBOX  = "hero/herobox.plist"
GAME_TEXTURE_IMAGE_FILENAME_HEROBOX= "hero/herobox.png"

-- sounds
GAME_SFX = {
	tapButton  = "sfx/TapButtonSound.mp3",
	backButton = "sfx/BackButtonSound.mp3",
}

CCTOUCHBEGAN="began"
CCTOUCHMOVED="moved"
CCTOUCHENDED="ended"



