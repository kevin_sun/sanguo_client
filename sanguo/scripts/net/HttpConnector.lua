local cjson = require("cjson")
local EventProtocol=require("framework.api.EventProtocol")
local HttpConnector=class("HttpConnector")
--增加事件机制
EventProtocol.extend(HttpConnector)
HttpConnector.errorEvent="errorEvent"
HttpConnector.serverUrl=''
HttpConnector.logFun=nil

--设置请求处理的url,日志程序
function HttpConnector:init(url,logFun)
	self.url=url
	self.logFun=logFun
end

--发送http 请求 data是lua的table,以json的格式发送
function HttpConnector:sendRequest(reqName,data)
	local requestUrl=self.url.."?rpc="..reqName.."&ra=" .. math.random()
	local request=CCHTTPRequest:createWithUrlLua(handler(self,self.onRes),requestUrl,kCCHTTPRequestMethodPOST)
	request.repName=reqName
	request:addRequestHeader("Content-Type:text/json");
	if data then
		request:setPOSTData(cjson.encode(data))
	end
	CCLuaLog("HttpConnector:sendRequest..."..reqName.." send data ..."..to)
	request:start()
end

--处理CCHTTPRequest 回调
function HttpConnector:onRes(resp)
	local reqName=resp["name"]
	local request=resp["request"]
	if reqName=="completed" then
		self:onCompleted(request)
	elseif reqName=="cancelled" then
		self:onCancelled(request)
	elseif reqName=="failed" then
		self:onFailed(request)
	end
end

function HttpConnector:onCompleted(request)
	CCLuaLog("HttpConnector:onRes get response code..."..tostring(request:getResponseStatusCode()))
	if request:getResponseStatusCode()==200 then
		local data=cjson.decode(request:getResponseDataLua())
		self:dispatchEvent({name=request.repName,data=data})
	else
		self:dispatchEvent({name=self.errorEvent,errorMsg=request:getErrorMessage(),errorCode=request:getErrorCode()})
	end
end


function HttpConnector:onCancelled(request)
end

function HttpConnector:onFailed(request)
	self:dispatchEvent({name=self.errorEvent,errorMsg=request:getErrorMessage(),errorCode=request:getErrorCode()})
end

return HttpConnector