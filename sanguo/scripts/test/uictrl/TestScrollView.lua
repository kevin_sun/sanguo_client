local ScrollView=require("uictrl.ScrollView")
local ScrollCell=require("uictrl.ScrollCell")

local TestScrollView=class("TestScrollView",function () 
	return display.newScene("TestScrollView")
end)

function TestScrollView:ctor()
	local rect=CCRectMake(0,0,480,48)
	local scrollView=ScrollView.new(rect,ScrollView.DIRECTION_HORIZONTAL)
	for i=100001,100023 do
		local avatarFile="avatar/96/"..i..".png"
		local scrollCell=ScrollCell.new(avatarFile,0,0,ScrollView.DIRECTION_HORIZONTAL)
		scrollView:addCell(scrollCell)
	end
	self:setPosition(0,480)
	self:addChild(scrollView)
	
end

return TestScrollView